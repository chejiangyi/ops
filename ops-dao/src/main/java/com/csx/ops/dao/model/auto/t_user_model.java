package com.csx.ops.dao.model.auto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * t_user 表自动实体映射,不要手工修改
 *
 * @author 车江毅
 * @since 2020-12-09 13:14:58
 * 自动生成:https://gitee.com/makejava/EasyCode/wikis/
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class t_user_model implements Serializable {

    public Integer id;
    /**
     * 姓名
     */
    public String name;
    /**
     * 密码
     */
    public String pswd;
    /**
     * 角色:0=管理员,1=普通用户
     */
    public Integer role;
    /**
     * app权限
     */
    public String appAuth;
    /**
     * db权限
     */
    public String dbAuth;
    /**
     * 服务器权限
     */
    public String serverAuth;
    /**
     * 更新时间
     */
    public Date updateTime;

    public Date createTime;
}