package com.csx.ops.dao;

import com.yh.csx.bsf.core.util.ContextUtils;

import javax.naming.Context;
import javax.sql.DataSource;

public class DbConfig {
    public static DataSource getDbSource(){
        return ContextUtils.getBean(DataSource.class,true);
    }
}
