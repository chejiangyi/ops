package com.csx.ops.dao.model.auto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * t_publish 表自动实体映射,不要手工修改
 *
 * @author 车江毅
 * @since 2020-12-09 13:14:56
 * 自动生成:https://gitee.com/makejava/EasyCode/wikis/
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class t_publish_model implements Serializable {

    public Integer id;
    /**
     * 发布名称
     */
    public String name;
    /**
     * 应用名
     */
    public String app;
    /**
     * 版本号
     */
    public Double version;
    /**
     * 正常流程序列化
     */
    public String commonFlow;
    /**
     * 异常路程序列化
     */
    public String rollFlow;
    /**
     * 创建时间
     */
    public Date createTime;
    /**
     * 创建人
     */
    public String createUser;
    /**
     * 更新时间
     */
    public Date updateTime;
    /**
     * 更新人
     */
    public String updateUser;

    /**
     * 创建人ID
     */
    public Integer createUserId;

    /**
     * 创建人ID
     */
    public Integer updateUserId;
}