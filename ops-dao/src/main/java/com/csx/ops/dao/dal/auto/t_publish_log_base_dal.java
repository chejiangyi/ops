package com.csx.ops.dao.dal.auto;


import com.csx.ops.dao.model.auto.t_publish_log_model;
import com.yh.csx.bsf.core.db.DbConn;
import com.yh.csx.bsf.core.util.ConvertUtils;
import lombok.val;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * t_publish_log 表自动dal映射,不要手工修改
 *
 * @author 车江毅
 * @since 2020-12-09 13:14:57
 * 自动生成: https://gitee.com/makejava/EasyCode/wikis/
 */
public class t_publish_log_base_dal {

    public boolean add(DbConn conn, t_publish_log_model model) {
        val par = new Object[]{
                /**发布id*/
                model.publishId,
                /**发布日志名称*/
                model.name,
                /**发布内容*/
                model.content,
                /**创建人*/
                model.createUser,
                /**创建时间*/
                model.createTime,
                /**本次执行耗时*/
                model.timeWatch
        };
        int rev = conn.executeSql("insert into t_publish_log(publishId,name,content,createUser,createTime,timeWatch)" +
                "values(?,?,?,?,?,?)", par);
        return rev == 1;
    }

    public boolean edit(DbConn conn, t_publish_log_model model) {
        val par = new Object[]{
                /**发布id*/
                model.publishId,
                /**发布日志名称*/
                model.name,
                /**发布内容*/
                model.content,
                /**创建人*/
                model.createUser,
                /**创建时间*/
                model.createTime,
                /**本次执行耗时*/
                model.timeWatch,
                model.id
        };
        int rev = conn.executeSql("update t_publish_log set publishId=?,name=?,content=?,createUser=?,createTime=?,timeWatch=? where id=?", par);
        return rev == 1;

    }

    public boolean delete(DbConn conn, Integer id) {
        val par = new Object[]{id};
        String Sql = "delete from t_publish_log where id=?";
        int rev = conn.executeSql(Sql, par);
        return rev == 1;
    }

    public t_publish_log_model get(DbConn conn, Integer id) {
        val par = new Object[]{id};
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_publish_log s where s.id=?");
        val ds = conn.executeList(stringSql.toString(), par);
        if (ds != null && ds.size() > 0) {
            return createModel(ds.get(0));
        }
        return null;
    }

    public ArrayList<t_publish_log_model> list(DbConn conn) {
        val rs = new ArrayList<t_publish_log_model>();
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_publish_log s ");
        val ds = conn.executeList(stringSql.toString(), new Object[]{});
        if (ds != null && ds.size() > 0) {
            for (Map<String, Object> dr : ds) {
                rs.add(createModel(dr));
            }
        }
        return rs;
    }

    public ArrayList<t_publish_log_model> getListByPublishId(DbConn conn,Integer id) {
        val rs = new ArrayList<t_publish_log_model>();
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_publish_log s  where  s.publishId=? order by createTime desc  limit 30");
        val ds = conn.executeList(stringSql.toString(), new Object[]{id});
        if (ds != null && ds.size() > 0) {
            for (Map<String, Object> dr : ds) {
                rs.add(createModel(dr));
            }
        }
        return rs;
    }

    public t_publish_log_model createModel(Map<String, Object> dr) {
        val o = new t_publish_log_model();
        /***/
        if (dr.containsKey("id")) {
            o.id = ConvertUtils.convert(dr.get("id"), Integer.class);
        }
        /**发布id*/
        if (dr.containsKey("publishId")) {
            o.publishId = ConvertUtils.convert(dr.get("publishId"), Integer.class);
        }
        /**发布日志名称*/
        if (dr.containsKey("name")) {
            o.name = ConvertUtils.convert(dr.get("name"), String.class);
        }
        /**发布内容*/
        if (dr.containsKey("content")) {
            o.content = ConvertUtils.convert(dr.get("content"), String.class);
        }
        /**创建人*/
        if (dr.containsKey("createUser")) {
            o.createUser = ConvertUtils.convert(dr.get("createUser"), String.class);
        }
        /**创建时间*/
        if (dr.containsKey("createTime")) {
            o.createTime = ConvertUtils.convert(dr.get("createTime"), Date.class);
        }
        /**本次执行耗时*/
        if (dr.containsKey("timeWatch")) {
            o.timeWatch = ConvertUtils.convert(dr.get("timeWatch"), Long.class);
        }
        return o;
    }
}