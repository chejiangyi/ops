package com.csx.ops.dao.model.auto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * t_config 表自动实体映射,不要手工修改
 *
 * @author 车江毅
 * @since 2020-12-09 13:14:52
 * 自动生成:https://gitee.com/makejava/EasyCode/wikis/
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class t_config_model implements Serializable {

    public Integer id;

    /**
     * 配置项名称
     */
    public String configName;

    /**
     * 配置key
     */
    public String key;
    /**
     * 配置值
     */
    public String value;
    /**
     * 创建时间
     */
    public Date createTime;
    /**
     * 更新时间
     */
    public Date updateTime;

    /**
     * 创建人
     */
    public String createUser;
    /**
     * 更新人
     */
    public String updateUser;

    /**
     * 创建人ID
     */
    public Integer createUserId;

    /**
     * 创建人ID
     */
    public Integer updateUserId;
}