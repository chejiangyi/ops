package com.csx.ops.dao;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.yh.csx.bsf.core.db.DbHelper;
import lombok.var;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ConfigParse {
    public List<String> getAppList(Integer userId){
        List<String> rs= Lists.newArrayList();
        var value = DbHelper.get(DbConfig.getDbSource(),(c)->{
            var m = new t_config_dal().get(c,"app",userId);
            if(m == null)
                return "";
            return com.yh.csx.bsf.core.util.StringUtils.nullToEmpty(m.value);
        });
        return Arrays.asList(value.split(";"));
    }

    public Map<String, Map<String,String>> getDatabaseList(Integer userId){
        var value = DbHelper.get(DbConfig.getDbSource(),(c)->{
            var m = new t_config_dal().get(c,"database",userId);
            if(m == null)
                return "";
            return com.yh.csx.bsf.core.util.StringUtils.nullToEmpty(m.value);
        });
        //dbconfig
        //ops-localhost-db->ip=jdbc:mysql://localhost:3306/ops?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC,user=root,password=123456;        Map<String, Map<String,String>> resultMap=Maps.newHashMap();
        Map<String,String>  databaseMap=null;
        Map<String, Map<String,String>> resultMap=Maps.newHashMap();
        String[] dbRecords=value.split(";");
        if(dbRecords.length>0){
            for(String dbRecord:dbRecords){
               String[]  dbArray= dbRecord.split("\\->");
               if(dbArray.length>=2){
                   String dbName=dbArray[0];
                   dbName=dbName.replaceAll("\r|\n", "");
                   String dbServerMessage=dbArray[1];
                   String[] databaseServer=dbServerMessage.split("\\,");
                   if(databaseServer.length==3){
                      String ip=databaseServer[0];
                      String user=databaseServer[1];
                      String password=databaseServer[2];
                      databaseMap=Maps.newHashMap();
                      String[] array=ip.split("ip=");
                      if(array.length==2){
                          databaseMap.put("ip",array[1]);
                      }
                       array=user.split("user=");
                       if(array.length==2){
                           databaseMap.put("user",array[1]);
                       }
                       array=password.split("password=");
                       if(array.length==2){
                           databaseMap.put("password",array[1]);
                       }
                       resultMap.put(dbName,databaseMap);
                   }
               }
            }
        }
        return resultMap;
    }

    public Map<String, Map<String,String>> getServerList(Integer userId){
        var value = DbHelper.get(DbConfig.getDbSource(),(c)->{
            var m = new t_config_dal().get(c,"server",userId);
            if(m == null)
                return "";
            return com.yh.csx.bsf.core.util.StringUtils.nullToEmpty(m.value);
        });
        //dbconfig
        //test-cool-app-server:ip=127.0.0.1,user=root,password=123456;
        //music-app-server:ip=127.0.0.1,user=root,password=123456;
        Map<String, Map<String,String>> resultMap=Maps.newHashMap();
        Map<String,String>  databaseMap=null;
        String[] dbRecords=value.split(";");
        if(dbRecords.length>0){
            for(String dbRecord:dbRecords){
                String[]  dbArray= dbRecord.split("\\:");
                if(dbArray.length>=2){
                    String dbName=dbArray[0];
                    String dbServerMessage=dbArray[1];
                    String[] databaseServer=dbServerMessage.split("\\,");
                    if(databaseServer.length>=3){
                        String ip=databaseServer[0];
                        String user=databaseServer[1];
                        String password=databaseServer[2];
                        databaseMap=Maps.newHashMap();

                        String[] array=ip.split("ip=");
                        if(array.length==2){
                            databaseMap.put("ip",array[1]);
                        }
                        array=user.split("user=");
                        if(array.length==2){
                            databaseMap.put("user",array[1]);
                        }
                        array=password.split("password=");
                        if(array.length==2){
                            databaseMap.put("password",array[1]);
                        }
                        resultMap.put(dbName,databaseMap);
                    }
                }
            }
        }
        return resultMap;
    }
}
