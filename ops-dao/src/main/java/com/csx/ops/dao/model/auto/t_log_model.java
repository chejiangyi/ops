package com.csx.ops.dao.model.auto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * t_log 表自动实体映射,不要手工修改
 *
 * @author 车江毅
 * @since 2020-12-09 13:14:54
 * 自动生成:https://gitee.com/makejava/EasyCode/wikis/
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class t_log_model implements Serializable {

    public Integer id;
    /**
     * 内容
     */
    public String content;
    /**
     * 类型:0=正常,1=错误
     */
    public Integer type;
    /**
     * 创建时间
     */
    public Date createTime;

    public Integer createUserId;

    public String createUserName;
}