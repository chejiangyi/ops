package com.csx.ops.dao.dal.auto;


import com.csx.ops.dao.model.auto.t_user_model;
import com.google.common.collect.Maps;
import com.yh.csx.bsf.core.base.Ref;
import com.yh.csx.bsf.core.db.DbConn;
import com.yh.csx.bsf.core.db.DbHelper;
import com.yh.csx.bsf.core.util.ConvertUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * t_user 表自动dal映射,不要手工修改
 *
 * @author 车江毅
 * @since 2020-12-09 13:14:58
 * 自动生成: https://gitee.com/makejava/EasyCode/wikis/
 */
@Slf4j
@Repository
public class t_user_base_dal {

    @Autowired
    private DataSource dataSource;

    public boolean add(DbConn conn, t_user_model model) {
        val par = new Object[]{
                /**姓名*/
                model.name,
                /**密码*/
                model.pswd,
                /**角色:0=管理员,1=普通用户*/
                model.role,
                /**app权限*/
                model.appAuth,
                /**db权限*/
                model.dbAuth,
                /**服务器权限*/
                model.serverAuth,
                /**更新时间*/
                model.updateTime,
                /***/
                model.createTime
        };
        int rev = conn.executeSql("insert into t_user(name,pswd,role,appAuth,dbAuth,serverAuth,updateTime,createTime)" +
                "values(?,?,?,?,?,?,?,?)", par);
        return rev == 1;
    }

    public boolean edit(DbConn conn, t_user_model model) {
        val par = new Object[]{
                /**姓名*/
                model.name,
                /**密码*/
                model.pswd,
                /**角色:0=管理员,1=普通用户*/
                model.role,
                /**app权限*/
                model.appAuth,
                /**db权限*/
                model.dbAuth,
                /**服务器权限*/
                model.serverAuth,
                /**更新时间*/
                model.updateTime,
                model.id
        };
        int rev = conn.executeSql("update t_user set name=?,pswd=?,role=?,appAuth=?,dbAuth=?,serverAuth=?,updateTime=? where id=?", par);
        return rev == 1;

    }

    public boolean delete(DbConn conn, Integer id) {
        val par = new Object[]{id};
        String Sql = "delete from t_user where id=?";
        int rev = conn.executeSql(Sql, par);
        return rev == 1;
    }

    public t_user_model get(DbConn conn, Integer id) {
        val par = new Object[]{id};
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_user s where s.id=?");
        val ds = conn.executeList(stringSql.toString(), par);
        if (ds != null && ds.size() > 0) {
            return createModel(ds.get(0));
        }
        return null;
    }

    public ArrayList<t_user_model> list(DbConn conn) {
        val rs = new ArrayList<t_user_model>();
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_user s ");
        val ds = conn.executeList(stringSql.toString(), new Object[]{});
        if (ds != null && ds.size() > 0) {
            for (Map<String, Object> dr : ds) {
                rs.add(createModel(dr));
            }
        }
        return rs;
    }

    public t_user_model createModel(Map<String, Object> dr) {
        val o = new t_user_model();
        /***/
        if (dr.containsKey("id")) {
            o.id = ConvertUtils.convert(dr.get("id"), Integer.class);
        }
        /**姓名*/
        if (dr.containsKey("name")) {
            o.name = ConvertUtils.convert(dr.get("name"), String.class);
        }
        /**密码*/
        if (dr.containsKey("pswd")) {
            o.pswd = ConvertUtils.convert(dr.get("pswd"), String.class);
        }
        /**角色:0=管理员,1=普通用户*/
        if (dr.containsKey("role")) {
            o.role = ConvertUtils.convert(dr.get("role"), Integer.class);
        }
        /**app权限*/
        if (dr.containsKey("appAuth")) {
            o.appAuth = ConvertUtils.convert(dr.get("appAuth"), String.class);
        }
        /**db权限*/
        if (dr.containsKey("dbAuth")) {
            o.dbAuth = ConvertUtils.convert(dr.get("dbAuth"), String.class);
        }
        /**服务器权限*/
        if (dr.containsKey("serverAuth")) {
            o.serverAuth = ConvertUtils.convert(dr.get("serverAuth"), String.class);
        }
        /**更新时间*/
        if (dr.containsKey("updateTime")) {
            o.updateTime = ConvertUtils.convert(dr.get("updateTime"), Date.class);
        }
        /***/
        if (dr.containsKey("createTime")) {
            o.createTime = ConvertUtils.convert(dr.get("createTime"), Date.class);
        }
        return o;
    }


    /**
     * 根据用户名查询统计
     * @author yls
     * @return
     */
    public t_user_model  findUserByUsername(String name) {
        t_user_model  user=null;
        String sql="select id,name,pswd,role from t_user where name=? ";
        var rs = DbHelper.get(dataSource,(c)->{
           return c.executeList(sql,new Object[]{name});
        });
        if(rs.size()==0){
            return user;
        }
        Map<String,Object> reuslt=rs.get(0);
        user=new t_user_model();
        user.setId(Integer.parseInt(reuslt.get("id").toString()));
        user.setName(reuslt.get("name").toString());
        user.setPswd(reuslt.get("pswd").toString());
        user.setRole(Integer.parseInt(reuslt.get("role").toString()));
        return user;
    }




    /**
     * 获取用户分页信息
     * @author yls
     * @return
     */
    public Map<String,Object>  getPage(DbConn db, String name, int pageIndex, int pageSize, Ref<Integer> totalSize) {
        Map<String,Object>  data= Maps.newHashMap();
        val par = new ArrayList<>();
        val rs = new ArrayList<t_user_model>();

        StringBuilder sb = new StringBuilder(" from t_user where 1=1 ");
        if (!StringUtils.isEmpty(name)) {
            par.add(name);
            sb.append(" and name like concat('%', ?, '%')");
        }
        String sql = "select * " + sb.toString() + " order by id desc " + String.format(" limit %s,%s", (pageIndex - 1) * pageSize, pageSize);
        String countSql = "select count(0) " + sb.toString();
        val ds = db.executeList(sql, par.toArray());
        if (ds != null && ds.size() > 0) {
            for (Map<String, Object> dr : ds) {
                rs.add(createModel(dr));
            }
        }
       data.put("data",rs);
       data.put("count",ConvertUtils.convert(db.executeScalar(countSql, par.toArray()),int.class));
       return data;
    }


}