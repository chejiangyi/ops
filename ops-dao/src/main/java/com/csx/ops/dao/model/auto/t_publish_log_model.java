package com.csx.ops.dao.model.auto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * t_publish_log 表自动实体映射,不要手工修改
 *
 * @author 车江毅
 * @since 2020-12-09 13:14:57
 * 自动生成:https://gitee.com/makejava/EasyCode/wikis/
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class t_publish_log_model implements Serializable {

    public Integer id;
    /**
     * 发布id
     */
    public Integer publishId;
    /**
     * 发布日志名称
     */
    public String name;
    /**
     * 发布内容
     */
    public String content;
    /**
     * 创建人
     */
    public String createUser;
    /**
     * 创建时间
     */
    public Date createTime;
    /**
     * 本次执行耗时
     */
    public Long timeWatch;
}