package com.csx.ops.dao;

import com.csx.ops.dao.dal.auto.t_config_base_dal;
import com.csx.ops.dao.model.auto.t_config_model;
import com.yh.csx.bsf.core.db.DbConn;
import lombok.val;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;

public class t_config_dal extends t_config_base_dal {
    public t_config_model get(DbConn conn, String key) {
        val par = new Object[]{key};
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_config s where s.key=?");
        val ds = conn.executeList(stringSql.toString(), par);
        if (ds != null && ds.size() > 0) {
            return createModel(ds.get(0));
        }
        return null;
    }

    public t_config_model get(DbConn conn, String key,Integer userId) {
        val par = new ArrayList<>();
        val stringSql = new StringBuilder();

        stringSql.append("select s.* from t_config s  where 1=1 ");
        if (!StringUtils.isEmpty(key)) {
            par.add(key);
            stringSql.append(" and s.key=?");
        }
        if (userId!=null) {
            par.add(userId);
            stringSql.append(" and s.createUserId=?");
        }

        val ds = conn.executeList(stringSql.toString(), par.toArray());
        if (ds != null && ds.size() > 0) {
            return createModel(ds.get(0));
        }
        return null;
    }


    public t_config_model getList(DbConn conn, String key,Integer userId) {
        val par = new ArrayList<>();
        val stringSql = new StringBuilder();

        stringSql.append("select s.* from t_config s  where 1=1 ");
        if (!StringUtils.isEmpty(key)) {
            par.add(key);
            stringSql.append(" and s.key=?");
        }
        if (userId!=null) {
            par.add(userId);
            stringSql.append(" and s.createUserId=?");
        }

        val ds = conn.executeList(stringSql.toString(), par.toArray());
        if (ds != null && ds.size() > 0) {
            return createModel(ds.get(0));
        }
        return null;
    }
}
