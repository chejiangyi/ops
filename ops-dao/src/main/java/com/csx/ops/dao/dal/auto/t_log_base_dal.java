package com.csx.ops.dao.dal.auto;


import com.csx.ops.dao.model.auto.t_log_model;
import com.csx.ops.dao.model.auto.t_user_model;
import com.google.common.collect.Maps;
import com.yh.csx.bsf.core.base.Ref;
import com.yh.csx.bsf.core.db.DbConn;
import com.yh.csx.bsf.core.util.ConvertUtils;
import lombok.val;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * t_log 表自动dal映射,不要手工修改
 *
 * @author 车江毅
 * @since 2020-12-09 13:14:55
 * 自动生成: https://gitee.com/makejava/EasyCode/wikis/
 */
public class t_log_base_dal {

    public boolean add(DbConn conn, t_log_model model) {
        val par = new Object[]{
                /**内容*/
                model.content,
                /**类型:0=正常,1=错误*/
                model.type,
                /**创建时间*/
                model.createTime
        };
        int rev = conn.executeSql("insert into t_log(content,type,createTime)" +
                "values(?,?,?)", par);
        return rev == 1;
    }

    public boolean edit(DbConn conn, t_log_model model) {
        val par = new Object[]{
                /**内容*/
                model.content,
                /**类型:0=正常,1=错误*/
                model.type,
                /**创建时间*/
                model.createTime,
                model.id
        };
        int rev = conn.executeSql("update t_log set content=?,type=?,createTime=? where id=?", par);
        return rev == 1;

    }

    public boolean delete(DbConn conn, Integer id) {
        val par = new Object[]{id};
        String Sql = "delete from t_log where id=?";
        int rev = conn.executeSql(Sql, par);
        return rev == 1;
    }

    public boolean deleteAll(DbConn conn) {
        String Sql = "delete from t_log ";
        int rev = conn.executeSql(Sql,null);
        return rev == 1;
    }


    public t_log_model get(DbConn conn, Integer id) {
        val par = new Object[]{id};
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_log s where s.id=?");
        val ds = conn.executeList(stringSql.toString(), par);
        if (ds != null && ds.size() > 0) {
            return createModel(ds.get(0));
        }
        return null;
    }

    public ArrayList<t_log_model> list(DbConn conn) {
        val rs = new ArrayList<t_log_model>();
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_log s ");
        val ds = conn.executeList(stringSql.toString(), new Object[]{});
        if (ds != null && ds.size() > 0) {
            for (Map<String, Object> dr : ds) {
                rs.add(createModel(dr));
            }
        }
        return rs;
    }

    public t_log_model createModel(Map<String, Object> dr) {
        val o = new t_log_model();
        /***/
        if (dr.containsKey("id")) {
            o.id = ConvertUtils.convert(dr.get("id"), Integer.class);
        }
        /**内容*/
        if (dr.containsKey("content")) {
            o.content = ConvertUtils.convert(dr.get("content"), String.class);
        }
        /**类型:0=正常,1=错误*/
        if (dr.containsKey("type")) {
            o.type = ConvertUtils.convert(dr.get("type"), Integer.class);
        }
        /**创建时间*/
        if (dr.containsKey("createTime")) {
            o.createTime = ConvertUtils.convert(dr.get("createTime"), Date.class);
        }
        /**创建用户Id*/
        if (dr.containsKey("createUserId")) {
            o.createUserId = ConvertUtils.convert(dr.get("createUserId"), Integer.class);
        }
        /**创建时间*/
        if (dr.containsKey("createUserName")) {
            o.createUserName = ConvertUtils.convert(dr.get("createUserName"), String.class);
        }
        return o;
    }


    /**
     * 获取用户分页信息
     * @author yls
     * @return
     */
    public Map<String,Object>  getPage(DbConn db, String content, String timeType, String errType, int pageIndex, int pageSize, Ref<Integer> totalSize) {
        Map<String,Object>  data= Maps.newHashMap();
        val par = new ArrayList<>();
        val rs = new ArrayList<t_log_model>();

        StringBuilder sb = new StringBuilder(" from t_log where 1=1 ");

        //操作内容
        if (!StringUtils.isEmpty(content)) {
            par.add(content);
            sb.append(" and content like concat('%', ?, '%')");
        }

        //查询类型
        if (!StringUtils.isEmpty(timeType)) {
            if("0".equals(timeType)){
                 //一周
                sb.append(" and DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(createTime)");
            }else if("1".equals(timeType)){
                //二周
                sb.append(" and DATE_SUB(CURDATE(), INTERVAL 14 DAY) <= date(createTime)");

            }else if("2".equals(timeType)){
                //一个月
                sb.append(" and DATE_SUB(CURDATE(), INTERVAL 30 DAY) <= date(createTime)");
            }
        }

        //错误类型
        if (!StringUtils.isEmpty(errType)) {
            if(!"2".equals(errType)) {
                //不是所有的情况
                par.add(errType);
                sb.append(" and type=?");
            }
        }

        String sql = "select * " + sb.toString() + " order by id desc " + String.format(" limit %s,%s", (pageIndex - 1) * pageSize, pageSize);
        String countSql = "select count(0) " + sb.toString();
        val ds = db.executeList(sql, par.toArray());
        if (ds != null && ds.size() > 0) {
            for (Map<String, Object> dr : ds) {
                rs.add(createModel(dr));
            }
        }
        data.put("data",rs);
        data.put("count",ConvertUtils.convert(db.executeScalar(countSql, par.toArray()),int.class));
        return data;
    }
}