package com.csx.ops.dao.dal.auto;


import com.csx.ops.dao.model.auto.t_publish_model;
import com.csx.ops.dao.model.auto.t_publish_model;
import com.google.common.collect.Maps;
import com.yh.csx.bsf.core.base.Ref;
import com.yh.csx.bsf.core.db.DbConn;
import com.yh.csx.bsf.core.util.ConvertUtils;
import lombok.val;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * t_publish 表自动dal映射,不要手工修改
 *
 * @author 车江毅
 * @since 2020-12-09 13:14:56
 * 自动生成: https://gitee.com/makejava/EasyCode/wikis/
 */
public class t_publish_base_dal {

    public boolean add(DbConn conn, t_publish_model model) {
        val par = new Object[]{
                /**发布名称*/
                model.name,
                /**应用名*/
                model.app,
                /**版本号*/
                model.version,
                /**正常流程序列化*/
                model.commonFlow,
                /**异常路程序列化*/
                model.rollFlow,
                /**创建时间*/
                model.createTime,
                /**创建人*/
                model.createUser,
                /**更新时间*/
                model.updateTime,
                /**更新人*/
                model.updateUser,
                model.createUserId,
                model.updateUserId
        };
        int rev = conn.executeSql("insert into t_publish(name,app,version,commonFlow,rollFlow,createTime,createUser,updateTime,updateUser,createUserId,updateUserId)" +
                "values(?,?,?,?,?,?,?,?,?,?,?)", par);
        return rev == 1;
    }

    public boolean edit(DbConn conn, t_publish_model model) {
        val par = new Object[]{
                /**发布名称*/
                model.name,
                /**应用名*/
                model.app,
                /**版本号*/
                model.version,
                /**正常流程序列化*/
                model.commonFlow,
                /**异常路程序列化*/
                model.rollFlow,
                /**更新时间*/
                model.updateTime,
                /**更新人*/
                model.updateUser,
                model.updateUserId,
                model.id
        };
        int rev = conn.executeSql("update t_publish set name=?,app=?,version=?,commonFlow=?,rollFlow=?,updateTime=?,updateUser=?,updateUserId=? where id=?", par);
        return rev == 1;

    }

    public boolean delete(DbConn conn, Integer id) {
        val par = new Object[]{id};
        String Sql = "delete from t_publish where id=?";
        int rev = conn.executeSql(Sql, par);
        return rev == 1;
    }

    public t_publish_model get(DbConn conn, Integer id) {
        val par = new Object[]{id};
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_publish s where s.id=?");
        val ds = conn.executeList(stringSql.toString(), par);
        if (ds != null && ds.size() > 0) {
            return createModel(ds.get(0));
        }
        return null;
    }

    public ArrayList<t_publish_model> list(DbConn conn) {
        val rs = new ArrayList<t_publish_model>();
        val stringSql = new StringBuilder();
        stringSql.append("select s.* from t_publish s ");
        val ds = conn.executeList(stringSql.toString(), new Object[]{});
        if (ds != null && ds.size() > 0) {
            for (Map<String, Object> dr : ds) {
                rs.add(createModel(dr));
            }
        }
        return rs;
    }

    public t_publish_model createModel(Map<String, Object> dr) {
        val o = new t_publish_model();
        /***/
        if (dr.containsKey("id")) {
            o.id = ConvertUtils.convert(dr.get("id"), Integer.class);
        }
        /**发布名称*/
        if (dr.containsKey("name")) {
            o.name = ConvertUtils.convert(dr.get("name"), String.class);
        }
        /**应用名*/
        if (dr.containsKey("app")) {
            o.app = ConvertUtils.convert(dr.get("app"), String.class);
        }
        /**版本号*/
        if (dr.containsKey("version")) {
            o.version = ConvertUtils.convert(dr.get("version"), Double.class);
        }
        /**正常流程序列化*/
        if (dr.containsKey("commonFlow")) {
            o.commonFlow = ConvertUtils.convert(dr.get("commonFlow"), String.class);
        }
        /**异常路程序列化*/
        if (dr.containsKey("rollFlow")) {
            o.rollFlow = ConvertUtils.convert(dr.get("rollFlow"), String.class);
        }
        /**创建时间*/
        if (dr.containsKey("createTime")) {
            o.createTime = ConvertUtils.convert(dr.get("createTime"), Date.class);
        }
        /**创建人*/
        if (dr.containsKey("createUser")) {
            o.createUser = ConvertUtils.convert(dr.get("createUser"), String.class);
        }
        /**更新时间*/
        if (dr.containsKey("updateTime")) {
            o.updateTime = ConvertUtils.convert(dr.get("updateTime"), Date.class);
        }
        /**更新人*/
        if (dr.containsKey("updateUser")) {
            o.updateUser = ConvertUtils.convert(dr.get("updateUser"), String.class);
        }
        /**更新人*/
        if (dr.containsKey("createUserId")) {
            o.createUserId = ConvertUtils.convert(dr.get("createUserId"), Integer.class);
        }
        /**更新人*/
        if (dr.containsKey("updateUserId")) {
            o.updateUserId = ConvertUtils.convert(dr.get("updateUserId"), Integer.class);
        }
        return o;
    }



    /**
     * 获取用户分页信息
     * @author yls
     * @return
     */
    public Map<String,Object>  getPage(DbConn db, String name, int pageIndex, int pageSize, Ref<Integer> totalSize,Integer userId) {
        Map<String,Object>  data= Maps.newHashMap();
        val par = new ArrayList<>();
        val rs = new ArrayList<t_publish_model>();

        StringBuilder sb = new StringBuilder(" from t_publish where 1=1 ");
        if (!StringUtils.isEmpty(name)) {
            par.add(name);
            sb.append(" and name like concat('%', ?, '%')");
        }
        if(userId!=null){
            par.add(userId);
            sb.append(" and createUserId =?");
        }
        String sql = "select * " + sb.toString() + " order by id desc " + String.format(" limit %s,%s", (pageIndex - 1) * pageSize, pageSize);
        String countSql = "select count(0) " + sb.toString();
        val ds = db.executeList(sql, par.toArray());
        if (ds != null && ds.size() > 0) {
            for (Map<String, Object> dr : ds) {
                rs.add(createModel(dr));
            }
        }
        data.put("data",rs);
        data.put("count",ConvertUtils.convert(db.executeScalar(countSql, par.toArray()),int.class));
        return data;
    }
}