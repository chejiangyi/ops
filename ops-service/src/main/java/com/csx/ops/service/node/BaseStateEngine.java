package com.csx.ops.service.node;

import com.csx.ops.core.XStreamFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.yh.csx.bsf.core.http.DynamicObj;
import com.yh.csx.bsf.core.util.JsonUtils;
import lombok.var;

import java.util.*;

public class BaseStateEngine {
    public StateEngine fromXml(String xml){
        XStream xStream = new XStream(new DomDriver("utf-8"));
        xStream.processAnnotations(this.getClass());
        for(var kv:getMapClass()){
            xStream.processAnnotations(kv);
        }
        return (StateEngine) xStream.fromXML(xml);
    }

    private List<Class> getMapClass(){
        var mapClass = new ArrayList<Class>();
        mapClass.add(StartNode.class);
        mapClass.add(ApolloNode.class);
        mapClass.add(JenkinsNode.class);
        mapClass.add(NotifyNode.class);
        mapClass.add(ShellNode.class);
        mapClass.add(SqlNode.class);
        mapClass.add(EndNode.class);
        return mapClass;
    }
    public String toXml(){
        XStream xStream = XStreamFactory.getXStream(new XmlFriendlyNameCoder("_-","_"));
        xStream.processAnnotations(this.getClass());
        for(var kv:getMapClass()){
            xStream.processAnnotations(kv);
        }
        var xml = xStream.toXML(this);
        return xml;
    }

    public StateEngine fromJsonWithOperate(String json,String operateType){
        var obj = (HashMap<String,Object>) JsonUtils.deserialize(json, new HashMap<String,Object>().getClass());
        if(obj ==null)
            return null;
        var clss = getMapClass();
        StateEngine r = new StateEngine();
        r.setNodes(new ArrayList<>());
        for(var o:(Collection<HashMap<String,Object>>)obj.get("nodes")){
            var cls = clss.stream().filter(c->c.getSimpleName().toLowerCase().equals(o.get("type").toString()+"node")).findFirst();
            if(cls.isPresent()){
                var node =  (BaseNode)JsonUtils.deserialize(JsonUtils.serialize(o), cls.get());
                node.setOperateType(operateType);
                node.setEngine(r);
                r.getNodes().add(node);
            }
        }
        return r;
    }


    /**
     * 获取当前节点Node
     * @author yls
     * @param json
     * @return
     */
    public StateEngine getNodefromJson(String json){
        var obj = (HashMap<String,Object>) JsonUtils.deserialize(json, new HashMap<String,Object>().getClass());
        if(obj ==null)
            return null;
        var clss = getMapClass();
        StateEngine r = new StateEngine();
        var cls = clss.stream().filter(c->c.getSimpleName().toLowerCase().equals(obj.get("type").toString()+"node")).findFirst();
        if(cls.isPresent()){
            var node =  (BaseNode)JsonUtils.deserialize(JsonUtils.serialize(obj), cls.get());
            node.setEngine(r);
            r.setCurrentNode(node);
        }
        return r;
    }


    public String toJson(){
        return JsonUtils.serialize(this);
    }
    
}
