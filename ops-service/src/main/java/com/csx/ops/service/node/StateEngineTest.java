package com.csx.ops.service.node;

import lombok.var;

import java.util.ArrayList;

public class StateEngineTest {
    public static void main(String[] args) {
       StateEngine engine = new StateEngine();
       engine.setNodes(new ArrayList<>());
       var start = new StartNode();start.setText("开始");start.setNext("sql");start.setLocation("0 0");
       engine.getNodes().add(start);
        var sql = new SqlNode();sql.setText("sql");sql.setNext("结束");sql.setLocation("100 200");
        sql.setDb("wms");sql.setSql("select * from aaa");
        engine.getNodes().add(sql);
        var end = new EndNode();end.setText("结束");end.setNext("");end.setLocation("300 300");
        engine.getNodes().add(end);
        var a =engine.toXml();
        System.out.println(a);
       var o = engine.fromXml(a);
       var b=1;
       var json = engine.toJson();
        System.out.println(json);
       o=new StateEngine().fromJsonWithOperate(json,"normal");
       var c=1;
    }
}
