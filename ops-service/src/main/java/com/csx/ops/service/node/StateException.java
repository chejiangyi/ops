package com.csx.ops.service.node;

import com.yh.csx.bsf.core.base.BsfException;

public class StateException extends BsfException {
    public StateException(String message){
        super(message);
    }

    public StateException(String message,Throwable exp){
        super(message,exp);
    }
}
