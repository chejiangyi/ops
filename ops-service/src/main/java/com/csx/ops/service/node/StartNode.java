package com.csx.ops.service.node;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.AllArgsConstructor;

import java.util.List;

@XStreamAlias("StartNode")
public class StartNode extends BaseNode {
    @Override
    public void exec(){
        writeLog("发布开始...");
    }

    //从子节点设置对应的状态机
    public void setEngine(StateEngine engine){
        super.setEngine(engine);
    }
}
