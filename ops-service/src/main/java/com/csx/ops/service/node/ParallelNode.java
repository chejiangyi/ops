package com.csx.ops.service.node;

import com.yh.csx.bsf.core.thread.ThreadPool;
import com.yh.csx.bsf.core.util.ThreadUtils;
import lombok.AllArgsConstructor;

/**
 * 并行发布暂不支持
 */
@AllArgsConstructor
public class ParallelNode extends BaseNode {
    int flowCount=0;
    @Override
    public void flow() {
       flowCount++;
       /*>上级节点数量*/
       if(flowCount>=this.getLastNodes().size()){
            this.goNextNode();
       }

    }

    @Override
    public void goNextNode() {
//        ThreadUtils.parallelFor("并行节点",this.getNext().size(),this.getNext(),(c)->{
//            this.goNextNode(c);
//        });
    }
}
