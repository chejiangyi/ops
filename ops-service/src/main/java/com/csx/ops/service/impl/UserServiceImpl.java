package com.csx.ops.service.impl;


import cn.hutool.http.server.HttpServerRequest;
import cn.hutool.http.server.HttpServerResponse;
import com.csx.ops.core.common.ServerResponse;
import com.csx.ops.dao.DbConfig;
import com.csx.ops.dao.dal.auto.t_user_base_dal;
import com.csx.ops.dao.model.auto.t_user_model;
import com.csx.ops.dao.t_config_dal;
import com.csx.ops.service.UserService;
import com.yh.csx.bsf.core.db.DbHelper;
import com.yh.csx.bsf.core.util.JsonUtils;
import com.yh.csx.bsf.redis.RedisProvider;
import lombok.var;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.UUID;

/**
 * 用户服务业务实现层
 */
@Service
public class UserServiceImpl implements UserService{

    /**
     * 用户dao层
     */
    @Autowired
    private t_user_base_dal userDao;

    @Override
    public ServerResponse<t_user_model> doLogin(String username,String password){
        if(StringUtils.isBlank(username)){
            return ServerResponse.createByErrorMessage("用户名不能为空");
        }
        if(StringUtils.isBlank(password)){
            return ServerResponse.createByErrorMessage("密码不能为空");
        }
        DbHelper.get(DbConfig.getDbSource(),(c)->{
            var m = new t_config_dal().get(c,"app");
            if(m == null)
                return "";
            return com.yh.csx.bsf.core.util.StringUtils.nullToEmpty(m.value);
        });
        t_user_model user=userDao.findUserByUsername(username);
        if(user==null){
            return ServerResponse.createByErrorMessage("用户名不存在");
        }

        //用户输入密码md5处理
        String md5Password=DigestUtils.md5DigestAsHex(password.getBytes());
        //比对密码
        if (!(md5Password.equals(user.getPswd()))) {
            return ServerResponse.createByErrorMessage("用户名或密码错误");
        }
        user.setPswd(StringUtils.EMPTY);
        return ServerResponse.createBySuccess(user,"登录成功");
    }
}
