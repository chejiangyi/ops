package com.csx.ops.service.node;

import com.csx.ops.core.XStreamFactory;
import com.csx.ops.core.XmlUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.yh.csx.bsf.core.util.*;
import lombok.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 状态机
 */
@Data
@XStreamAlias("StateXml")
public class StateEngine extends BaseStateEngine {

    @XStreamAlias("Nodes")
    private List<BaseNode> nodes;

    //日志文件的路径
    @XStreamOmitField
    @JsonIgnore
    private String logFile= PropertyUtils.getPropertyCache("flow.logFilePath","");

    //日志文件名称
    @Getter
    @Setter
    private String logFileName;

    //当前节点
    private BaseNode currentNode;

    public void flow(){
        try {
            var start = nodes.stream().filter(c -> c.getText().equals("开始")).findFirst();
            if (!start.isPresent())
                throw new StateException("开始节点不存在");
            start.get().flow();
        }catch (StateException e){
            writeLog("【流程错误】"+ ExceptionUtils.getDetailMessage(e));
            throw e;
        }catch (Exception e){
            writeLog("【系统错误】"+ExceptionUtils.getDetailMessage(e));
            throw e;
        }
    }

    //当前节点向下执行
    public void currentNodeflow(){
        try {
            if(nodes.size()==0){
                throw new StateException("没有节点可执行");
            }
            //取出第一个节点向下执行
            var start =nodes.get(0);
            if(start.getText().equals("结束"))
                throw new StateException("结束节点不支持发布");
            start.flow();
        }catch (StateException e){
            writeLog("【流程错误】"+ ExceptionUtils.getDetailMessage(e));
            throw e;
        }catch (Exception e){
            writeLog("【系统错误】"+ExceptionUtils.getDetailMessage(e));
            throw e;
        }
    }



    public void createFolder() throws IOException{
        if(StringUtils.isEmpty(logFile))
            return;
        var path = logPath();
        File file =new File(path);
        File fileParent = file.getParentFile();//返回的是File类型,可以调用exsit()等方法
        if (!fileParent.exists()) {
            fileParent.mkdirs();// 能创建多级目录
        }
        if (!file.exists())
            file.createNewFile();
    }


    public void writeLog(String content){
        if(StringUtils.isEmpty(logFile))
            return;
        var path = logPath();
        FileUtils.createDirectory(path);
        FileUtils.appendAllText(path,content);
    }



    public String logPath(){
        if(StringUtils.isEmpty(logFile))
            return "";
        return logFile+"/"+logFileName;
    }

    public String getAbsouteFilePath(String fileName){
        return logFile+"/"+fileName;
    }
}
