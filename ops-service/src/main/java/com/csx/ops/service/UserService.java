package com.csx.ops.service;


import cn.hutool.http.server.HttpServerRequest;
import cn.hutool.http.server.HttpServerResponse;
import com.csx.ops.core.common.ServerResponse;
import com.csx.ops.dao.model.auto.t_user_model; /**
 * 用户服务业务层
 */
public interface UserService {


    /**
     * 用户登录
     * @param username  用户名
     * @param password  密码
     */
    ServerResponse<t_user_model> doLogin(String username,String password) ;
}
