package com.csx.ops.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.csx.ops.core.common.ServerResponse;
import com.csx.ops.dao.DbConfig;
import com.csx.ops.dao.dal.auto.t_publish_log_base_dal;
import com.csx.ops.dao.model.auto.t_publish_log_model;
import com.csx.ops.dao.model.auto.t_publish_model;
import com.csx.ops.service.PublishLogService;
import com.yh.csx.bsf.core.db.DbHelper;

import java.util.Date;

public class PublishLogServiceImpl implements PublishLogService{


    public String buildFileName(Integer publishId,String operateType,String nodeType,String fileSuffix){
        StringBuilder publishLogFileNameSb=new StringBuilder();
        publishLogFileNameSb.append(publishId)
                .append("-")
                .append(operateType)
                .append("-")
                .append(nodeType)
                .append("-nodeExecute")
                .append(DateUtil.format(new DateTime(), "yyyy-MM-dd-HH-mm-sss"));
        String logFileNname=publishLogFileNameSb.toString()+fileSuffix;
        return logFileNname;
    }


    public String buildPublishContent(t_publish_model publishModel,String nodeText,String publishTypeDesc){
        StringBuilder content=new StringBuilder();
        content.append("【").append(publishModel.getCreateUser()).append("】").append("执行了")
                .append("【").append(publishModel.getApp()).append("】")
                .append("应用,").append(",版本号")
                .append("【")
                .append(publishModel.getVersion())
                .append("】")
                .append("执行了")
                .append("【")
                .append(nodeText)
                .append("】")
                .append("节点")
                .append(publishTypeDesc);
        return content.toString();
    };

    public ServerResponse record(Integer publishId,String name,String content,
                                               String userName,Long exucteTime) {
        //发布明细model
        t_publish_log_model publishLogModel=new t_publish_log_model();
        //发布id
        publishLogModel.setPublishId(publishId);

        //组成规则  当前发布id+发布流程（回滚流程）+当前第一个节点名称+年月日时分秒时间格式

        //发布日志名称
        publishLogModel.setName(name);
        //发布内容
        publishLogModel.setContent(content);
        publishLogModel.setCreateUser(userName);
        publishLogModel.setCreateTime(new Date());
        //本次执行耗时
        publishLogModel.setTimeWatch(exucteTime);
        boolean isOk=DbHelper.get(DbConfig.getDbSource(), c-> {
            return new t_publish_log_base_dal().add(c,publishLogModel);
        });
        if(isOk){
            return ServerResponse.createBySuccessMessage("记录日志成功");
        }else{
            return ServerResponse.createBySuccessMessage("记录日志失败功");
        }
    }



}
