package com.csx.ops.service;

import com.csx.ops.core.common.ServerResponse;
import com.csx.ops.dao.model.auto.t_publish_log_model;
import com.csx.ops.dao.model.auto.t_publish_model;
import com.csx.ops.dao.model.auto.t_user_model;

public interface PublishLogService {

    public String buildFileName(Integer publishId,String operateType,String nodeType,String fileSuffix);


    public String buildPublishContent(t_publish_model publishModel,String nodeText,String publishTypeDesc);


    public ServerResponse record(Integer publishId,String name,String content,
                                 String userName,Long exucteTime);

}
