# ops
 基于<a href='https://gitee.com/yhcsx/csx-bsf-all/'>彩食鲜BSF框架</a>，开发的标准项目模板（脚手架）,后端java项目都按照这个项目模板快速搭建项目。
## 愿景
* 业务开发只关注业务需求,专注编写业务代码; 对于技术只关注使用,不需要关注基础服务的集成和相关配置(因为所有都已经集成完毕)。

* 标准化项目结构：定义标准的项目分层结构,便于技术资源共享和交叉业务协作开发。

* 标准化技术选型：定义标准的分布式技术选型,便于运维人员统一维护,同时简化使用和统一性能监控和优化。

## 项目结构

```
ops
    -- ops-api  	 #api端第三方调用提供request，response类库 （api协议层）
    -- ops-core 	 #公共代码 （核心层）
    -- ops-dao 	 #数据库操作 （数据层）
    -- ops-service  #业务逻辑 （服务层）
    -- ops-task 	 #批处理任务 （任务层）
    -- ops-provider #api端接口实现（api实现层，提供者模式）
 -- doc 					 #项目资料 （文档资料）
 	--client.sh 			 #脚手架脚本
 	--mybatis-generator.zip  #mybatis 代码生成工具
 -- README.md 				 #项目文档 （说明文档）

```

## 通过脚手架初始化项目
脚手架目录: /doc/client.sh
```
# 使用说明: [windows 下面使用git bash运行]
# curl -O  http://gitee.com/chejiangyi/ops/raw/{分支名:默认master}/doc/client.sh
#sh client.sh {项目名：projectName} {groupId} {分支名：version ,默认master}
#  
curl -O  https://gitee.com/chejiangyi/ops/raw/master/doc/client.sh
sh client.sh ops com.csx.ops master
```



##### 架构师：车江毅