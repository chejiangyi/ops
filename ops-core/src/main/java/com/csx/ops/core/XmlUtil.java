package com.csx.ops.core;


import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.yh.csx.bsf.core.base.BsfException;
import lombok.var;

import java.util.Map;

/**
 * @author: chejiangyi
 * @version: 2019-11-03 10:38
 **/
public class XmlUtil {
    @SuppressWarnings("unchecked")
    public static <T> T deserialize(String xml,Class<T> cls) {
        try {
            XStream xStream = new XStream(new DomDriver("utf-8"));
            xStream.processAnnotations(cls);

            return (T)xStream.fromXML(xml);
        }catch (Exception e){
            throw new BsfException("反序列化异常",e);

        }
    }

    public static String serialize(Object entity) {
        try {
            XStream xStream = XStreamFactory.getXStream(new XmlFriendlyNameCoder("_-","_"));
            xStream.processAnnotations(entity.getClass());
            return xStream.toXML(entity);
        }catch (Exception e){
            throw new BsfException("序列化异常",e);
        }
    }
}
