package com.csx.ops.core.common;

/**
 * 常量类
 * @author yls
 * @version: 2020-12-16
 */
public class Const {

    public static final String CURRENT_USER="currentUser";

    public static final Integer ADMIN_ACCOUNT_NAME=0;


    //默认系统配置
    public interface  SystemConfig{
        //应用的key
        public static final String APP_KEY="APP_KEY";
    }

}
