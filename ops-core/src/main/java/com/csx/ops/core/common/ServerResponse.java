package com.csx.ops.core.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 高复用对象
 * @author yls
 * @version: 2020-12-15
 * @param <T>
 */
//保证序列化json的时候，如果是null对象，key也会消失
//@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServerResponse<T> implements Serializable {

    private Integer code;
    private String msg;
    private T data;
    private Integer count;

    private ServerResponse(int code){
        this.code=code;
    }

    private ServerResponse(int code,T data){
        this.code=code;
        this.data=data;
    }

    private ServerResponse(int code,String msg,T data){
        this.code=code;
        this.msg=msg;
        this.data=data;
    }

    private ServerResponse(int code,String msg,T data,Integer count){
        this.code=code;
        this.msg=msg;
        this.data=data;
        this.count=count;
    }

    private ServerResponse(int code,String msg){
        this.code=code;
        this.msg=msg;
    }

    @JsonIgnore
    //使之不在json序列化结果中
    public boolean isSuccess(){
        return  this.code== ResponseCode.SUCCESS.getCode();
    }

    public int getCode(){
        return code;
    }

    public T getData(){
        return data;
    }

    public String getMsg(){
        return msg;
    }

    public Integer getCount() {
        return count;
    }

    public static <T>  ServerResponse<T> createBySuccess(){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode());
    }

    public static <T>  ServerResponse<T> createBySuccessMessage(String msg){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(),msg);
    }

    public static <T>  ServerResponse<T> createBySuccess(T data){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(),data);
    }

    public static <T>  ServerResponse<T> createBySuccess(T data,String msg){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(),msg,data);
    }


    public static <T>  ServerResponse<T> createBySuccess(T data,String msg,Integer count){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(),msg,data,count);
    }



    public static <T>  ServerResponse<T> createByError(){
        return new ServerResponse<>(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
    }

    public static <T> ServerResponse<T> createByErrorMessage(String errorMsg){
        return new ServerResponse<>(ResponseCode.ERROR.getCode(),errorMsg);
    }

    public static <T> ServerResponse<T> createByErrorMessage(int errorCode,String errorMsg){
        return new ServerResponse<>(errorCode,errorMsg);
    }

}