package com.csx.ops.api.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: robin.wang
 * @version: 2019-12-31 16:03
 **/
@Data
@AllArgsConstructor
public class Page<T> implements Serializable {

    private int pageNum;

    private int pageSize;

    private int totalNum;

    private List<T> data;

}