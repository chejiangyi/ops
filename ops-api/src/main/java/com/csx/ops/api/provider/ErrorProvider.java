package com.csx.ops.api.provider;

import com.csx.ops.api.model.response.CommonResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Api("错误测试")
@FeignClient(name = "csx-b2b-ops-provider", path = "/api/error")
@RequestMapping("/error")
public interface ErrorProvider {

	@GetMapping("/callBiz")
	@ApiOperation("测试错误")
    CommonResponse<String> callBiz();
}

