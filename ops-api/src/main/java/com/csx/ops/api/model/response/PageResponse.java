package com.csx.ops.api.model.response;


/**
 * @author: robin.wang
 * @version: 2019-12-31 15:48
 **/
public class PageResponse<T> {

    public static <T> PageResponse<T> success(String message, Page<T> data) {
        return new PageResponse<T>(CommonResponse.SUCCESS, message, data);
    }

    public static <T> PageResponse<T> success(Page<T> data) {
        return new PageResponse<T>(CommonResponse.SUCCESS, CommonResponse.SUCCESS_MSG, data);
    }

    public static <T> PageResponse<T> success() {
        return new PageResponse<T>(CommonResponse.SUCCESS, CommonResponse.SUCCESS_MSG, null);
    }

    public static <T> PageResponse<T> error(int code, String message, Page<T> data) {
        return new PageResponse<T>(code, message, data);
    }

    public static <T> PageResponse<T> error(int code, String message) {
        return new PageResponse<T>(code, message, null);
    }

    public static <T> PageResponse<T> error(String message, Page<T> data) {
        return new PageResponse<T>(CommonResponse.ERROR, message, data);
    }

    public static <T> PageResponse<T> error(String message) {
        return new PageResponse<T>(CommonResponse.ERROR, message, null);
    }

    public static <T> PageResponse<T> error() {
        return new PageResponse<T>(CommonResponse.ERROR, CommonResponse.ERROR_MSG, null);
    }

    public static <T> PageResponse<T> error(Throwable e) {
        
          return PageResponse.error(5000, e.getMessage());
        
    }

    protected int code;
    protected String message;
    protected Page<T> data;

    public PageResponse(int code, String message, Page<T> data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Page<T> getData() {
        return data;
    }
}

