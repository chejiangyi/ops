package com.csx.ops.api.provider;


import com.csx.ops.api.model.response.Page;
import com.csx.ops.api.model.vo.Log;


/**
 * 日志管理服务层
 * @author	Robin.Wang
 * @date	2019-08-12
 *
 */
public interface LogService {
	
	/**
	 * 查询列表
	 * @param type
	 * @param level
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	Page<Log> list(Log ddslog, Integer pageNum, Integer pageSize);
	/**
	 * 查看详情
	 * */
	Log findById(Integer id);
	/**
	 * 清理日志
	 * */
	void clearLog();
	/**
	 * 清理监控日志
	 * */
	void clearMonitorLog(String agentIp);
	

	
}
