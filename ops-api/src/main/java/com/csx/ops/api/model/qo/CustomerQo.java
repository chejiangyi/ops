package com.csx.ops.api.model.qo;
import com.csx.ops.api.model.response.Pagination;

import lombok.Data;


@Data
public class CustomerQo extends Pagination {
	private Long id;
	private String customerNumber;
	private String customerName;
}
