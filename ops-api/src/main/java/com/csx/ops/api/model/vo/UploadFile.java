package com.csx.ops.api.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Huang Zhaoping
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UploadFile {
    private String fileUrl;
    private String fileName;
}
