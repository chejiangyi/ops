//
//package com.csx.ops.web.base.controller.manager;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.yh.csx.bsf.core.base.Ref;
//import com.yh.csx.bsf.core.util.JsonUtils;
//import com.yh.csx.bsf.flow.core.base.FlowException;
//import com.yh.csx.bsf.flow.core.base.Model;
//import com.yh.csx.bsf.flow.core.base.Variable;
//import com.yh.csx.bsf.flow.core.base.Xml;
//import com.yh.csx.bsf.flow.core.db.DbHelper;
//import com.yh.csx.bsf.flow.core.enums.ModelTypeEnum;
//import com.yh.csx.bsf.flow.core.util.XmlUtil;
//import com.yh.csx.bsf.flow.dao.dal.t_flow_dal;
//import com.yh.csx.bsf.flow.dao.dal.t_model_dal;
//import com.yh.csx.bsf.flow.dao.model.auto.t_model_model;
//import com.yh.csx.bsf.flow.engine.PaintProvider;
//import com.yh.csx.bsf.flow.web.base.SpringMvcController;
//import com.yh.csx.bsf.flow.web.base.User;
//import com.yh.csx.bsf.flow.web.base.pager.Pager1;
//import lombok.val;
//import lombok.var;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.ModelAndView;
//
//import java.util.ArrayList;
//import java.util.Date;
//
///**
// * @author: chejiangyi
// * @version: 2019-10-23 09:41
// **/
//@Controller
//@RequestMapping("/manager/model")
//public class ModelController extends SpringMvcController {
//
//    @RequestMapping("/index")
//    public ModelAndView index(String code,String title,String user_by,Integer model_category_id,Integer pageindex,Integer pagesize) {
//        val pageIndex2=(pageindex == null?1:pageindex);
//        val pageSize2=(pagesize == null?10:pagesize);
//        // 将表单当中获取到的值设置到request中的Attribute当中
//        html.s2("code",code).s2("title",title).s2("user_by",user_by).s2("model_category_id",model_category_id).s2("pageindex",pageindex).s2("pagesize",pagesize);
//        return pageVisit((m)-> {
//                    Ref<Integer> totalSize = new Ref<>(0);
//                    val list = DbHelper.get(c->{
//                       return new t_model_dal().getPage(c,code,title,user_by,model_category_id,0,pageIndex2,pageSize2,totalSize);
//                    });
//                    new Pager1(pageIndex2,totalSize.getData()).setPageSize(pageSize2).out();
//                    request.setAttribute("model",list);
//                }
//        );
//    }
//
//    @RequestMapping("/detail")
//    public ModelAndView detail(Integer id)
//    {
//        val id2 = (id==null?0:id);
//        html.s2("id",id2);
//        return pageVisit((m)-> {
//            t_model_model model = DbHelper.get(c -> {
//                return new t_model_dal().get(c, id2);
//            });
//            if (model == null) {
//                model = new t_model_model();
//                var xml = new Xml(new ArrayList<Variable>(),new ArrayList<Model.Node>(),new ArrayList<Model.FormPermission>());
//                model.setXml(XmlUtil.serialize(xml));
//                model.setModel_type((byte) ModelTypeEnum.common.getValue());
//            }
//            //val mobile_html = DefaultHttpClient.Default.get(model.mobile_form_url);
//            //val xml = XmlUtil.deserialize(model.getXml(), new TypeReference<Model.Xml>() {})
//            var xml = XmlUtil.deserialize(model.getXml(),Xml.class);
//            var paint = new PaintProvider().paint(xml.getNodes());
//            html.s2("parint_nodes",JsonUtils.serialize(paint.getNodes()));
//            html.s2("parint_links",JsonUtils.serialize(paint.getLinks()));
//            html.s2("model", model);
//        });
//    }
//    @RequestMapping("/histroy")
//    public ModelAndView histroy(String code)
//    {
//        html.s2("code",code);
//        return pageVisit((m)-> {
//            Ref<Integer> totalSize = new Ref<>(0);
//            val list = DbHelper.get(c->{
//                return new t_model_dal().getHistroy(c,code,0,1000,totalSize);
//            });
//            request.setAttribute("model",list);
//        });
//    }
//    @RequestMapping("/save")
//    public ModelAndView save(Integer id,String sys_id,String code,String title,String xml,String pc_form,String mobile_form,String type,
//                             Byte model_type,String external_pc_form_url,String external_mobile_form_url,Byte is_external_form) {
//        return jsonVisit((m) -> {
//            try {
//                var xml1 = XmlUtil.deserialize(xml, Xml.class);
//            } catch (Exception e) {
//                throw new FlowException("xml 格式错误", e);
//            }
//
//            String pc_form_content = JsonUtils.deserialize(pc_form, new TypeReference<String>() {
//            });
//            String mobile_form_content = JsonUtils.deserialize(mobile_form, new TypeReference<String>() {
//            });
//            if("debug".equals(type)){
//                DbHelper.call((c) -> {
//                    var model = new t_model_dal().get(c, id);
//                    model.setTitle(title);
//                    model.setPc_form_url(pc_form_content);
//                    model.setMobile_form_url(mobile_form_content);
//                    model.setExternal_mobile_form_url(external_mobile_form_url);model.setExternal_pc_form_url(external_pc_form_url);
//                    model.setIs_external_form(is_external_form);
//                    model.setUpdate_time(new Date());
//                    model.setUpdate_by(User.getCurrent().getUsername());
//                    model.setXml(xml);
//                    new t_model_dal().edit(c, model);
//                });
//            } else{
//                DbHelper.call((c) -> {
//                    int version = new t_model_dal().getMaxVersion(c, code);
//                    new t_model_dal().add(c, new t_model_model(0, sys_id, code, title, pc_form_content, mobile_form_content, version + 1, (byte) (version==0?1:0),
//                            new Date(), new Date(), User.getCurrent().getUsername(), User.getCurrent().getUsername(), xml,model_type,external_pc_form_url,external_mobile_form_url,is_external_form,0));
//                });
//            }
//            return "ok";
//        });
//    }
//    @RequestMapping("/del")
//        public ModelAndView del(String code) {
//        return jsonVisit((m) -> {
//            DbHelper.call((c) -> {
//                val dal = new t_model_dal();
//                val model = dal.get(c, code);
//                int usedcount = new t_flow_dal().getCountOfUsed(c, model.getCode());
//                if (usedcount > 0) {
//                    throw new FlowException("当前模型还有流程在使用状态,不能删除!");
//                }
//                new t_model_dal().delete(c, model.getCode());
//            });
//            return "ok";
//        });
//    }
//    @RequestMapping("/setUse")
//    public ModelAndView setUse(Integer id) {
//        return jsonVisit((m) -> {
//            DbHelper.call((c) -> {
//                val dal = new t_model_dal();
//                val item = dal.get(c,id);
//                val oldUse = dal.getUse(c,item.getCode());
//                dal.setUse(c, id,true);
//                if(oldUse!=null&&oldUse.id!=id)
//                {dal.setUse(c, oldUse.id,false);}
//            });
//            return "ok";
//        });
//    }
//        @RequestMapping("/delbyid")
//        public ModelAndView delbyid(Integer id) {
//            return jsonVisit((m) -> {
//                DbHelper.call((c) -> {
//                    val dal = new t_model_dal();
//                    val model = dal.get(c, id);
//                    int usedcount = new t_flow_dal().getCountOfUsed(c, model.getId());
//                    if (usedcount > 0) {
//                        throw new FlowException("当前模型还有流程在使用状态,不能删除!");
//                    }
//                    new t_model_dal().delete(c, model.id);
//                });
//                return "ok";
//            });
//        }
//}
