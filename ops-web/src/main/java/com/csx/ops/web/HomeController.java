package com.csx.ops.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeController {

//	@Value("${bsf.flow.user.admin}")
//	String[] admins;

//	@Autowired
//	LoginProvider loginProvider;

    @RequestMapping("/")
    public String index()
    {
        return "/home/index";
    }
//    /**
//     * 登录页面
//     * */
//    @RequestMapping(value="/login",method=RequestMethod.GET)
//    public String login_page()
//    {
//        return "/home/index";
//    }
//    /**
//     * 登录
//     * */
//    @RequestMapping(value="/login.do",method=RequestMethod.POST)
//    public String login(String username,String password)
//    {
//    	UserLoginReq userLoginReq =new UserLoginReq();
//    	userLoginReq.setUsername(username);
//    	userLoginReq.setPassword(password);
//    	userLoginReq.setBusinessCode("");
//    	CommonResponse<UserResp> reponse=loginProvider.login(userLoginReq);
//    	if(reponse.getCode()>0)
//    	{
//    	    // 登录成功
//    		User user=new User(reponse.getData().getName(),reponse.getData().getUserNumber(),reponse.getData().getEmail(),reponse.getData().getTelephone());
//    		User.setCurrent(user);
//            //log.info("登录信息："+JSON.toJSONString(reponse));
//            return "redirect:user/flow/lunched_list";
//    	}else{
//    	    // 登入失败
//    	    return "/home/index";
//        }
//    }
//
//    @RequestMapping(value="/logout",method=RequestMethod.GET)
//    public String logout()
//    {
//    	User.setCurrent(null);
//        return "/home/index";
//    }
}
