package com.csx.ops.web.controller.manager;

import com.csx.ops.web.base.SpringMvcController;
import com.yh.csx.bsf.core.base.Ref;
import com.yh.csx.bsf.core.db.DbConn;
import com.yh.csx.bsf.core.db.DbHelper;
import com.yh.csx.bsf.core.util.StringUtils;
import com.yh.csx.bsf.web.pager.Pager1;
import lombok.val;
import lombok.var;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author: chejiangyi
 * @version: 2019-10-28 18:49
 **/
@Controller
@RequestMapping("/manager/flowlog")
public class FlowLogController extends SpringMvcController {
    @RequestMapping("/index")
    public ModelAndView index(String name, String flow_title, Long flow_id, String log, Integer type, Integer pageindex, Integer pagesize) {
        val pageIndex2 = (pageindex == null ? 1 : pageindex);
        val pageSize2 = (pagesize == null ? 10 : pagesize);
        html.s2("name", name).s2("flow_title", flow_title).s2("flow_id", flow_id).s2("log", log).s2("type", StringUtils.nullToEmpty(type)).s2("pageindex", pageindex).s2("pagesize", pagesize);
        return pageVisit((m) -> {

                    Ref<Integer> totalSize = new Ref<>(0);
//                    val list = DbHelper.get(c -> {
//                        return new t_flow_log_dal().getPage(c, flow_title, flow_id, log, type, pageIndex2, pageSize2, totalSize);
//                    });
                    new Pager1(pageIndex2, totalSize.getData()).setPageSize(pageSize2).out();
//                    request.setAttribute("model", list);
                }
        );
    }
}
