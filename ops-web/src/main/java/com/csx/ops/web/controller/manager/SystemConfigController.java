package com.csx.ops.web.controller.manager;

import com.csx.ops.core.common.Const;
import com.csx.ops.core.common.ServerResponse;
import com.csx.ops.dao.DbConfig;
import com.csx.ops.dao.dal.auto.t_config_base_dal;
import com.csx.ops.dao.dal.auto.t_publish_base_dal;
import com.csx.ops.dao.dal.auto.t_user_base_dal;
import com.csx.ops.dao.model.auto.t_config_model;
import com.csx.ops.dao.model.auto.t_config_model;
import com.csx.ops.dao.model.auto.t_publish_model;
import com.csx.ops.dao.model.auto.t_user_model;
import com.csx.ops.service.UserService;
import com.csx.ops.web.base.SpringMvcController;
import com.csx.ops.web.base.User;
import com.dianping.cat.util.StringUtils;
import com.yh.csx.bsf.core.base.Ref;
import com.yh.csx.bsf.core.db.DbHelper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 系统配置控制器
 *
 * @author yls
 * @date 2020-12-17
 * 
 * 
 */
@Slf4j
@Controller
@RequestMapping(value="/systemConfig")
public class SystemConfigController extends SpringMvcController {




	@GetMapping("/pageList")
	@ResponseBody
	public ServerResponse  pageList(String username,Integer pageIndex, Integer pageSize){
            t_user_model user=User.getCurrent();
            if(user==null){
                return ServerResponse.createByErrorMessage("用户会话已失效");
            }
			val pageIndex2=(pageIndex == null?1:pageIndex);
			val pageSize2=(pageSize == null?10:pageSize);
//            if(Const.ADMIN_ACCOUNT_NAME.equals(user.getRole())) {
//                Ref<Integer> totalSize = new Ref<>(0);
//                return DbHelper.get(DbConfig.getDbSource(), c->{
//                    Map<String,Object> data= new t_config_base_dal().getPage(c,username,pageIndex2,pageSize2,totalSize,null);
//                    List<t_config_model>  dataList=(List<t_config_model> )data.get("data");
//                    Integer  count=(Integer) data.get("count");
//                    return ServerResponse.createBySuccess(dataList,"查询成功",count);
//                });
//            }else{
//                Ref<Integer> totalSize = new Ref<>(0);
//                return DbHelper.get(DbConfig.getDbSource(), c->{
//                    Map<String,Object> data= new t_config_base_dal().getPage(c,username,pageIndex2,pageSize2,totalSize,user.getId());
//                    List<t_config_model>  dataList=(List<t_config_model> )data.get("data");
//                    Integer  count=(Integer) data.get("count");
//                    return ServerResponse.createBySuccess(dataList,"查询成功",count);
//                });
//            }

                //TODO 暂时没有办法做系统配置的权限管理，否则ConfigParse中的config解析都得做相关修改，包含具体节点对全局配置的获取
                Ref<Integer> totalSize = new Ref<>(0);
                return DbHelper.get(DbConfig.getDbSource(), c->{
                    Map<String,Object> data= new t_config_base_dal().getPage(c,username,pageIndex2,pageSize2,totalSize,null);
                    List<t_config_model>  dataList=(List<t_config_model> )data.get("data");
                    Integer  count=(Integer) data.get("count");
                    return ServerResponse.createBySuccess(dataList,"查询成功",count);
                });
	}



    /**
     * 详情
     * @param
     * @return
     */
    @GetMapping("/detail")
    @ResponseBody
    public ServerResponse  deatil(@RequestParam(required = true) Integer id){
        return DbHelper.get(DbConfig.getDbSource(), c->{
            t_config_model user=new t_config_base_dal().get(c,id);
            return ServerResponse.createBySuccess(user);
        });
    }


    /**
     * 添加
     * @param configModel
     * @return
     */
    @PostMapping("/save")
    @ResponseBody
    public ServerResponse  save(t_config_model configModel){
        t_user_model user= User.getCurrent();
        if(user==null){
            return ServerResponse.createByErrorMessage("用户会话已失效");
        }
        return DbHelper.get(DbConfig.getDbSource(), c->{
            configModel.setCreateTime(new Date());
            configModel.setUpdateTime(new Date());
            configModel.setCreateUser(user.getName());
            configModel.setUpdateUser(user.getName());
            configModel.setCreateUserId(user.getId());
            configModel.setUpdateUserId(user.getId());
            boolean rs=new t_config_base_dal().add(c,configModel);
            if(rs){
                return ServerResponse.createBySuccessMessage("新增成功");
            }else {
                return ServerResponse.createBySuccessMessage("新增失败");
            }
        });
    }


    /**
     * 修改用户信息
     * @param configModel
     * @return
     */
    @PostMapping("/update")
    @ResponseBody
    public ServerResponse  update(t_config_model configModel){
        t_user_model user=User.getCurrent();
        if(user==null){
            return ServerResponse.createByErrorMessage("用户会话已失效");
        }
        return DbHelper.get(DbConfig.getDbSource(), c->{
            configModel.setUpdateTime(new Date());
            configModel.setUpdateUser(user.getName());
            configModel.setUpdateUserId(user.getId());
            boolean rs=new t_config_base_dal().edit(c,configModel);
            if(rs){
                return ServerResponse.createBySuccessMessage("修改成功");
            }else {
                return ServerResponse.createBySuccessMessage("修改失败");
            }
        });
    }



    /**
     * 删除用户信息
     * @param id  主键id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public ServerResponse  delete(Integer id){
        return DbHelper.get(DbConfig.getDbSource(), c->{
            boolean rs=new t_config_base_dal().delete(c,id);
            if(rs){
                return ServerResponse.createBySuccessMessage("修改成功");
            }else {
                return ServerResponse.createBySuccessMessage("修改失败");
            }
        });
    }


}
