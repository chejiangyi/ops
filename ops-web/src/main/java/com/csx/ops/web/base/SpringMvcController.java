package com.csx.ops.web.base;

import com.csx.ops.api.model.response.CommonResponse;
import com.yh.csx.bsf.core.util.ExceptionUtils;
import com.yh.csx.bsf.core.util.JsonUtils;
import com.yh.csx.bsf.core.util.TimeWatchUtils;
import com.yh.csx.bsf.web.template.HtmlHelper;
import com.yh.csx.bsf.web.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 扩展SpringController的实现，采用lambada表达式的方式进行类似aop的拦截
 */
@Slf4j
public class SpringMvcController extends BaseController {
    protected void checkAdmin() {
        checkUser();
        if (!new User().isAdmin()) {
            throw new RuntimeException("无权限访问");
        }
    }

    protected void checkUser() {
        if (new User().getCurrent() == null) {
            throw new RuntimeException("用户未登陆");
        }
    }
}
