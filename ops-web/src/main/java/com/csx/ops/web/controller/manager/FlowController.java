//package com.csx.ops.web.base.controller.manager;
//
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.csx.ops.web.base.SpringMvcController;
//import com.yh.csx.bsf.core.base.Ref;
//import com.yh.csx.bsf.core.util.JsonUtils;
//import lombok.val;
//import lombok.var;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.servlet.ModelAndView;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author: chejiangyi
// * @version: 2019-10-23 09:43
// **/
//@Controller
//@RequestMapping("/manager/flow")
//public class FlowController extends SpringMvcController {
//    @RequestMapping("/index")
//    public ModelAndView index(Long id, String title, String approve_node, String approve_by, String create_by, Integer flow_state,String model_code,Integer model_version,String create_time_from,String create_time_to, Integer pageindex, Integer pagesize)
//    {
//        val pageIndex2=(pageindex == null?1:pageindex);
//        val pageSize2=(pagesize == null?10:pagesize);
//
//        html.s2("id",id).s2("title",title).s2("approve_node",approve_node).s2("approve_by",approve_by).s2("create_by",create_by)
//                .s2("flow_state", flow_state==null?0:flow_state).s2("model_code",model_code).s2("model_version",model_version)
//                .s2("create_time_from",create_time_from).s2("create_time_to",create_time_to).s2("pageindex",pageindex)
//                .s2("pagesize",pagesize);
//        return pageVisit((m)-> {
//                    Ref<Integer> totalSize = new Ref<>(0);
//                    val list = DbHelper.get(c->{
//                        return new t_flow_dal().getPage(c,id, title,  approve_node,  approve_by, null,null, create_by,  flow_state, model_code, model_version,create_time_from,create_time_to,pageIndex2,pageSize2,totalSize);
//                    });
//                    new Pager1(pageIndex2,totalSize.getData()).setPageSize(pageSize2).out();
//                    request.setAttribute("model",list);
//                }
//        );
//    }
//    @RequestMapping("/changevariable")
//    public ModelAndView changevariable(Long id) {
//        html.s2("id",id);
//        return pageVisit((m) -> {
//            String variable_json = DbHelper.get((c) -> {
//               val o= new t_flow_dal().get(c, id);
//               return o.getFlow_variable_json();
//            });
//            List<Variable> variables =  JsonUtils.deserialize(variable_json, new TypeReference<List<Variable>>() {});
//            if(variables==null || variables.size()==0){
//                variables = new ArrayList<>();variables.add(new Variable("","","",""));
//            }
//            html.s2("variables",variables);
//        });
//    }
//    @RequestMapping("/saveVariable")
//    public ModelAndView saveVariable(Long id,String variable_json) {
//
//        return jsonVisit((m) -> {
//            try {
//                var json = JsonUtils.deserialize(variable_json, new TypeReference<List<Variable>>() {});
//            } catch (Exception e) {
//                throw new FlowException("json 格式错误", e);
//            }
//            DbHelper.call((c) -> {
//                if(!new t_flow_dal().updateFlowVariable(c, id, variable_json,"系统")){
//                    throw new FlowException("流程状态异常请刷新重试");
//                }
//            });
//
//            return "ok";
//        });
//    }
//    @RequestMapping("/changenode")
//    public ModelAndView changenode(Long id) {
//        html.s2("id",id);
//        return pageVisit((m) -> {
//            String xml = DbHelper.get((c) -> {
//                val o= new t_flow_dal().get(c, id);
//                val nodes = new t_model_dal().get(c,o.model_id);
//                return nodes.getXml();
//            });
//            val nodes =  XmlUtil.deserialize(xml,Xml.class);
//            html.s2("nodes",nodes.getNodes());
//        });
//    }
//    @RequestMapping("/saveNode")
//    public ModelAndView saveNode(Long id,String node) {
//        return jsonVisit((m) -> {
//            new StateEngine().changeNode(id,node,User.getCurrent().getUserno(),"系统管理员切换审批节点",null);
//            return "ok";
//        });
//    }
//    @RequestMapping("/changemodel")
//    public ModelAndView changemodel(Long id) {
//        html.s2("id",id);
//        return pageVisit((m) -> {
//            List<t_model_model> data = DbHelper.get((c) -> {
//                val o= new t_flow_dal().get(c, id);
//                html.s2("currentid",o.model_id);
//                Ref<Integer> total = new Ref<>(0);
//                val histroy = new t_model_dal().getHistroy(c,o.getModel_code(),0,1000,total);
//                return histroy;
//            });
//
//            html.s2("models",data);
//        });
//    }
//
//    //setUse
//    @RequestMapping("/setUse")
//    public ModelAndView setUse(Long id,Integer modelid) {
//        return jsonVisit((m) -> {
//            DbHelper.call((c) -> {
//               val f = new t_flow_dal().get(c,id);
//               val m2 = new t_model_dal().get(c,modelid);
//               new t_flow_dal().updateModel(c,f.id,m2.id,m2.code,m2.title,m2.version);
//            });
//            return "ok";
//        });
//    }
//
//    @RequestMapping("/del")
//    public ModelAndView del(Long id) {
//        return jsonVisit((m) -> {
//            DbHelper.call((c) -> {
//                new t_task_dal().deleteTasks(c,id);
//                new t_flow_dal().delete(c,id);
//            });
//
//            return "ok";
//        });
//    }
//
//    @RequestMapping("/close")
//    public ModelAndView close(Long id) {
//        return jsonVisit((m) -> {
//            new StateEngine().close(id, User.getCurrent().getUserno(),"系统管理员关闭流程");
//            return "ok";
//        });
//    }
//
//    @RequestMapping("/reflow")
//    public ModelAndView reflow(Long id) {
//        return jsonVisit((m) -> {
//            new StateEngine().flow(id,null,null,User.getCurrent().getUserno());
//            return "ok";
//        });
//    }
//
//}
