package com.csx.ops.web.base;

import com.csx.ops.core.common.Const;
import com.csx.ops.dao.model.auto.t_user_model;
import com.yh.csx.bsf.core.util.PropertyUtils;
import com.yh.csx.bsf.core.util.WebUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    public static t_user_model getCurrent() {
        Object u = WebUtils.getRequest().getSession().getAttribute(Const.CURRENT_USER);
        if (u == null) {
            return null;
        } else {
            return (t_user_model) u;
        }
    }

    public static void setCurrent(t_user_model value) {
        WebUtils.getRequest().getSession().setAttribute(Const.CURRENT_USER, value);
    }


    public boolean isAdmin() {
        Object admins = PropertyUtils.getProperty("bsf.flow.user.admin");
        if (admins == null || StringUtils.isBlank(admins.toString())) {
            return false;
        } else {
            String[] admin = admins.toString().split(",");
            for (int i = 0; i < admin.length; i++) {
                if (admin[i].equals(this.getUserno())) {
                    return true;
                }
            }
        }
        return false;
    }

    private String username;
    private String userno;
    private String email;
    private String telephone;


}
