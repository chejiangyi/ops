package com.csx.ops.web.controller.manager;

import com.csx.ops.core.common.Const;
import com.csx.ops.core.common.ServerResponse;
import com.csx.ops.dao.ConfigParse;
import com.csx.ops.dao.DbConfig;
import com.csx.ops.dao.dal.auto.t_publish_base_dal;
import com.csx.ops.dao.dal.auto.t_publish_log_base_dal;
import com.csx.ops.dao.model.auto.t_publish_log_model;
import com.csx.ops.dao.model.auto.t_publish_model;
import com.csx.ops.dao.model.auto.t_user_model;
import com.csx.ops.service.node.StateEngine;
import com.csx.ops.web.base.SpringMvcController;
import com.csx.ops.web.base.User;
import com.google.common.collect.Lists;
import com.yh.csx.bsf.core.db.DbHelper;
import com.yh.csx.bsf.core.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;


/**
 * home控制器
 * @author yls
 * @date 2020-12-16
 */
@Slf4j
@Controller("ospHomeCOntroller")
@RequestMapping(value="/home", method = RequestMethod.GET)
public class HomeController  extends SpringMvcController {

    /**
     * 创建流程页面
     * @param  isView 是否预览 false表示创建,true表示预览
     * @param  id 发布记录主键id
     * @param  type 类型  "normal":正常流程  "rollback":回滚流程
     * @author yls
     * @return
     */
    @RequestMapping("/addProcessUI")
    public ModelAndView createProcessUI(@RequestParam(required = true) Boolean isView,
                                        @RequestParam(required = false) Integer id,
                                        @RequestParam(required = false) String type) {
        t_user_model user= User.getCurrent();
        if(user==null){
            return new ModelAndView("redirect:/user/login");
        }
        ModelAndView modelAndView=new ModelAndView("/home/flow/addProcess");
        StateEngine stateEngine= new StateEngine();
        //如果没有传递参数,默认是编辑状态
        if(isView==null){
            isView=new Boolean("false");
        }
        if(id!=null){    //回显配置的对应节点
            if(!StringUtils.isEmpty(type)){
                t_publish_model publisModel=DbHelper.get(DbConfig.getDbSource(), c->{
                    return new t_publish_base_dal().get(c,id);
                });
                if("normal".equals(type)){
                    modelAndView.getModel().put("showNodes",JsonUtils.serialize(stateEngine.fromJsonWithOperate(publisModel.getCommonFlow(),type)));
                }
                if("rollback".equals(type)){
                    modelAndView.getModel().put("showNodes",JsonUtils.serialize(stateEngine.fromJsonWithOperate(publisModel.getRollFlow(),type)));
                }
            }
        }
        ConfigParse configParse=new ConfigParse();
//        if(Const.ADMIN_ACCOUNT_NAME.equals(user.getRole())) {
//            //获取所有的应用名称
//            modelAndView.getModel().put("appNames", configParse.getAppList(null));
//            //获取所有的db配置信息
//            Map<String, Map<String, String>> dbResultMap = configParse.getDatabaseList(null);
//            //获取shellServer配置信息
//            Map<String, Map<String, String>> serverResultMap = configParse.getServerList(null);
//            modelAndView.getModel().put("dbNames", Lists.newArrayList(dbResultMap.keySet()));
//            modelAndView.getModel().put("serverNames", Lists.newArrayList(serverResultMap.keySet()));
//        }else{
//            //获取所有的应用名称
//            modelAndView.getModel().put("appNames", configParse.getAppList(user.getId()));
//            //获取所有的db配置信息
//            Map<String, Map<String, String>> dbResultMap = configParse.getDatabaseList(user.getId());
//            //获取shellServer配置信息
//            Map<String, Map<String, String>> serverResultMap = configParse.getServerList(user.getId());
//            modelAndView.getModel().put("dbNames", Lists.newArrayList(dbResultMap.keySet()));
//            modelAndView.getModel().put("serverNames", Lists.newArrayList(serverResultMap.keySet()));
//        }
        //获取所有的应用名称  TODO 暂时没有办法做系统配置的权限管理，否则ConfigParse中的config解析都得做相关修改，包含具体节点对全局配置的获取
        modelAndView.getModel().put("appNames", configParse.getAppList(null));
        //获取所有的db配置信息
        Map<String, Map<String, String>> dbResultMap = configParse.getDatabaseList(null);
        //获取shellServer配置信息
        Map<String, Map<String, String>> serverResultMap = configParse.getServerList(null);
        modelAndView.getModel().put("dbNames", Lists.newArrayList(dbResultMap.keySet()));
        modelAndView.getModel().put("serverNames", Lists.newArrayList(serverResultMap.keySet()));
        modelAndView.getModel().put("isView",isView);
        return modelAndView;
    }





    /**
     * 发布管理页面
     * @author yls
     * @return
     */
    @RequestMapping("/publishManagerUI")
    public ModelAndView  publishManagerUI(@RequestParam(required = true) Integer id) {
        ModelAndView modelAndView=new ModelAndView("/home/flow/publishManagerUI");
       //回显配置的对应节点
        t_publish_model publisModel=DbHelper.get(DbConfig.getDbSource(), c->{
            return new t_publish_base_dal().get(c,id);
        });

        //查询改发布对应的发布历史明细
        List<t_publish_log_model> publisLogModel=DbHelper.get(DbConfig.getDbSource(), c->{
            return new t_publish_log_base_dal().getListByPublishId(c,id);
        });

        modelAndView.getModel().put("publisModel",publisModel);
        modelAndView.getModel().put("publisLogModel",publisLogModel);
        return  modelAndView;
    }



    /**
     * 发布列表页面
     * @author yls
     * @return
     */
    @RequestMapping("/publishListUI")
    public ModelAndView  publishListUI() {
        return new ModelAndView("/home/flow/publishList");
    }


    /**
     * 创建用户页面
     * @author yls
     * @return
     */
    @RequestMapping("/createUserUI")
    public ModelAndView  createUserUI() {
        return new ModelAndView("/home/user/addUser");
    }



    /**
     * 用户列表页面
     * @author yls
     * @return
     */
    @RequestMapping("/userListUI")
    public ModelAndView  userListUI() {
        ModelAndView modelAndView=new ModelAndView("/home/user/userList");
        return modelAndView;
    }




    /**
     * 操作日志列表页面
     * @author yls
     * @return
     */
    @RequestMapping("/operateLogListUI")
    public ModelAndView  operateLogListUI() {
        return new ModelAndView("/home/log/operateLogList");
    }


    /**
     * 系统配置列表
     * @author yls
     * @return
     */
    @RequestMapping("/systemConfigListUI")
    public ModelAndView  systemConfigListUI() {
        return new ModelAndView("/home/system/systemConfigList");
    }

    /**
     * 添加系统配置
     * @author yls
     * @return
     */
    @RequestMapping("/addSystemConfigUI")
    public ModelAndView  addSystemConfigUI() {
        return new ModelAndView("/home/system/addSystemConfig");
    }
}
