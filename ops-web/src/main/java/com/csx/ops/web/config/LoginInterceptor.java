package com.csx.ops.web.config;

import com.csx.ops.core.common.Const;
import com.csx.ops.dao.model.auto.t_user_model;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 用户登录会后
 * @author yls
 * @date 2020-12-24
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //用Session提取userId来验证登陆
        HttpSession session = request.getSession();
        //userId，登陆时放入session的
        t_user_model user=(t_user_model)session.getAttribute(Const.CURRENT_USER);
        //未登陆
        if (user == null){
            //在返回头部设置标志
            response.addHeader("FLAG", "-1");
            response.setHeader("SESSIONSTATUS", "TIMEOUT");
            //重定向目标地址
            response.setHeader("CONTEXTPATH", "/");
            response.sendRedirect("/user/login");
            //这个方法返回false表示忽略当前请求
            return false;
        }else {
            //放行，用户即可继续调用自己需要的接口
            return true;
        }
    }
}
