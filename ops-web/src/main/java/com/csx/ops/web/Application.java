package com.csx.ops.web;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * @Author ly
 * @Description
 * @Date 10:05 2020/9/27
 **/
//@EnableDiscoveryClient
//@EnableAsync(proxyTargetClass = true)
//@EnableTransactionManagement
//@EnableFeignClients(basePackages = "com.yh.csx")
@SpringBootApplication
@ComponentScan(basePackages = "com.csx.ops")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}