package com.csx.ops.web.controller.manager;


import com.csx.ops.core.common.ServerResponse;
import com.csx.ops.dao.DbConfig;
import com.csx.ops.dao.dal.auto.t_log_base_dal;
import com.csx.ops.dao.dal.auto.t_user_base_dal;
import com.csx.ops.dao.model.auto.t_log_model;
import com.yh.csx.bsf.core.base.Ref;
import com.yh.csx.bsf.core.db.DbHelper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 操作日志控制器
 * @author yls
 * @date 2019-10-29
 *
 *
 */
@Slf4j
@Controller
@RequestMapping(value="/operateLog")
public class OperateLogController {




    @GetMapping("/pageList")
    @ResponseBody
    public ServerResponse pageList(String content,String timeType,String errorType,
                                   Integer pageIndex, Integer pageSize){
        val pageIndex2=(pageIndex == null?1:pageIndex);
        val pageSize2=(pageSize == null?10:pageSize);
        Ref<Integer> totalSize = new Ref<>(0);
        return DbHelper.get(DbConfig.getDbSource(), c->{
            Map<String,Object> data= new t_log_base_dal().getPage(c,content,timeType,errorType,
                    pageIndex2,pageSize2,totalSize);
            List<t_log_model> dataList=(List<t_log_model> )data.get("data");
            Integer  count=(Integer) data.get("count");
            return ServerResponse.createBySuccess(dataList,"查询成功",count);
        });
    }


    @GetMapping("/deleteAll")
    @ResponseBody
    public ServerResponse deleteAll(){
        return DbHelper.get(DbConfig.getDbSource(), c->{
            boolean rs=new t_log_base_dal().deleteAll(c);
            if(rs) {
                return ServerResponse.createBySuccess("操作成功");
            }else{
                return ServerResponse.createBySuccess("操作失败");
            }
        });
    }

}
