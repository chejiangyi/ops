package com.csx.ops.web.controller;


import com.csx.ops.web.base.SpringMvcController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * spingboot的正常用法，还可以继续使用。
 * 自动requestmapping只是约定默认实现。
 */
@Controller
@RequestMapping("/systemerror")
public class SystemErrorController extends SpringMvcController {
    @RequestMapping(value="/index")
    public ModelAndView index() {
        return this.pageVisit((m)->{
        });
    }

}
