package com.csx.ops.web.controller.manager;

import cn.hutool.http.server.HttpServerRequest;
import cn.hutool.http.server.HttpServerResponse;
import com.alibaba.fastjson.JSON;
import com.csx.ops.core.common.Const;
import com.csx.ops.core.common.ServerResponse;
import com.csx.ops.dao.DbConfig;
import com.csx.ops.dao.dal.auto.t_user_base_dal;
import com.csx.ops.dao.model.auto.t_user_model;
import com.csx.ops.service.UserService;
import com.csx.ops.web.base.SpringMvcController;
import com.csx.ops.web.base.User;
import com.dianping.cat.util.StringUtils;
import com.yh.csx.bsf.core.base.Ref;
import com.yh.csx.bsf.core.db.DbHelper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 用户控制
 *
 * @author yls
 * @date 2020-12-17
 * 
 * 
 */
@Slf4j
@Controller
@RequestMapping(value="/user")
public class UserController extends SpringMvcController {

	@Autowired
	private UserService userService;

	/**
	 * 用户登录页面
	 * @author yls
	 * @return
	 */
	@RequestMapping("/login")
	public ModelAndView  login() {
		return new ModelAndView("login");
	}


	/**
	 * 登录
	 * @author yls
	 * @return
	 */
	@PostMapping("/doLogin")
	public ModelAndView  doLogin(String username, String password, HttpSession session) {
		ModelAndView mv=null;
		ServerResponse<t_user_model>   response =userService.doLogin(username,password);
		t_user_model user=response.getData();
		if(response.isSuccess()){
             User.setCurrent(user);
			 mv = new ModelAndView("/home/index");
			return mv;
		}else{
			mv = new ModelAndView("login");
			mv.getModel().put("error",response.getMsg());
			return mv;
		}
	}



	/**
	 * 退出
	 * @author yls
	 * @return
	 */
	@PostMapping("/logout")
	@ResponseBody
	public ServerResponse  logout(HttpSession session) {
		session.invalidate(); //销毁session;
		return ServerResponse.createBySuccessMessage("退出成功");
	}



	@GetMapping("/pageList")
	@ResponseBody
	public ServerResponse  pageList(String username,Integer pageIndex, Integer pageSize){
			val pageIndex2=(pageIndex == null?1:pageIndex);
			val pageSize2=(pageSize == null?10:pageSize);
			Ref<Integer> totalSize = new Ref<>(0);
            return DbHelper.get(DbConfig.getDbSource(), c->{
                Map<String,Object> data= new t_user_base_dal().getPage(c,username,pageIndex2,pageSize2,totalSize);
                List<t_user_model>  dataList=(List<t_user_model> )data.get("data");
                Integer  count=(Integer) data.get("count");
				return ServerResponse.createBySuccess(dataList,"查询成功",count);
            });
	}



    /**
     * 添加用户
     * @param
     * @return
     */
    @GetMapping("/detail")
    @ResponseBody
    public ServerResponse  deatil(@RequestParam(required = true) Integer id){
        return DbHelper.get(DbConfig.getDbSource(), c->{
            t_user_model user=new t_user_base_dal().get(c,id);
            return ServerResponse.createBySuccess(user);
        });
    }


    /**
     * 添加用户
     * @param user
     * @return
     */
    @PostMapping("/save")
    @ResponseBody
    public ServerResponse  save(t_user_model user){
        return DbHelper.get(DbConfig.getDbSource(), c->{
            if(!StringUtils.isEmpty(user.getPswd())) {
              user.setPswd(DigestUtils.md5DigestAsHex(user.getPswd().getBytes()));
            }
            user.setCreateTime(new Date());
            user.setUpdateTime(new Date());
            boolean rs=new t_user_base_dal().add(c,user);
            if(rs){
                return ServerResponse.createBySuccessMessage("新增成功");
            }else {
                return ServerResponse.createBySuccessMessage("新增失败");
            }
        });
    }


    /**
     * 修改用户信息
     * @param user
     * @return
     */
    @PostMapping("/update")
    @ResponseBody
    public ServerResponse  update(t_user_model user){
        return DbHelper.get(DbConfig.getDbSource(), c->{
            if(!StringUtils.isEmpty(user.getPswd())) {
                user.setPswd(DigestUtils.md5DigestAsHex(user.getPswd().getBytes()));
            }
            user.setUpdateTime(new Date());
            boolean rs=new t_user_base_dal().edit(c,user);
            if(rs){
                return ServerResponse.createBySuccessMessage("修改成功");
            }else {
                return ServerResponse.createBySuccessMessage("修改失败");
            }
        });
    }



    /**
     * 删除用户信息
     * @param id  主键id
     * @return
     */
    @PostMapping("/delete")
    @ResponseBody
    public ServerResponse  delete(Integer id){
        return DbHelper.get(DbConfig.getDbSource(), c->{
            boolean rs=new t_user_base_dal().delete(c,id);
            if(rs){
                return ServerResponse.createBySuccessMessage("修改成功");
            }else {
                return ServerResponse.createBySuccessMessage("修改失败");
            }
        });
    }


}
