<#import "../../_layout.ftl" as layout>
${Html.s("pagetitle","人员角色管理")}
<@layout._layout>

    <div class="head">
        <div class="title">
            ${pagetitle}
        </div>
    </div>

    <div class="orderlist m10 myorder">
        <form action="/rolemanagement/userlevel/index/" method="post" id="searchForm">
            <div class="search">
                <label>工号</label><input type="text" class="text midtext" style="width:150px;" name="userNo" value="${userNo!}" />
                <label>分管业务</label><input type="text" class="text midtext" style="width:150px;" name="bizName" value="${bizName!}" />
                <label>汇报对象工号</label><input type="text" class="text midtext" style="width:150px;" name="nextUserNo" value="${nextUserNo!}" />
                <label>创建人</label><input type="text" class="text midtext" style="width:150px;" name="createBy" value="${createBy!}" />
                <label>更新人</label><input type="text" class="text midtext" style="width:150px;" name="updateBy" value="${updateBy!}" />
                <input type="submit" class="btn1" value="搜索" accesskey="S" />
            </div>
        </form>

        <div class="tab_cont">
            <div class="List">
                <@_list/>
            </div>
        </div>
    </div>

    <script>
        $(function(){

        });
        function delbyid(id) {
            if(!confirm("请确认汇报线不再使用，删除后不可恢复？"))
            {
                return;
            }
            $.ajax({
                url: '/rolemanagement/userlevel/delbyid/',
                type: "post",
                data: {
                    id: id
                },
                success: function (data) {
                    if (data.code > 0) {
                        window.location.reload();
                    }
                    else {
                        alert(data.message);
                    }
                }
            });
        }
    </script>

</@layout._layout>

<#macro _list >

    <div>
        <a href="/rolemanagement/userlevel/detail" class="btn1" target="_blank">新增</a>
    </div>

    <table class="mytable" width="100%">
        <tr>
            <th style="width:3%">Id</th>
            <th style="width:9%">工号</th>
            <th style="width:7%">分管业务</th>
            <th style="width:9%">汇报对象工号</th>
            <th style="width:7%">创建人</th>
            <th style="width:7%">更新人</th>
            <th style="width:7%">操作</th>
        </tr>
        <#list model as item>
            <tr data-id="${item.id}">
                <td>${item.id}</td>
                <td>${item.user_no}</td>
                <td>${item.biz_name}</td>
                <td>${item.next_user_no}</td>
                <td>${item.create_by}</td>
                <td>${item.update_by}</td>
                <td>
                    <a href="/rolemanagement/userlevel/detail?id=${item.id}" target="_blank" class="edit">修改</a>
                    <a href="javascript:delbyid('${item.id}')" class="delbyid">删除</a>
                </td>
            </tr>
        </#list>
    </table>

    <div class="total pt10">
        <@layout._pager/>
    </div>
</#macro>