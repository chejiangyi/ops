<#import "../../_layout.ftl" as layout>
${Html.s("pagetitle",Html.w(id==0,"新建","修改")+"汇报线")}
<@layout._layout>

    <style>
        .mleft {
            margin-left: 200px;
        }
        .tb textarea{
            width: 95%;
        }
    </style>

    <script type="text/javascript">
        function save()
        {
            $.post("/rolemanagement/userlevel/save",
                {
                    "id": ${id!"0"},
                    "biz_name":$("#biz_name").val(),
                    "user_no":$("#user_no").val(),
                    "next_user_no":$("#next_user_no").val(),
                    "create_by":$("#create_by").val(),
                    "update_by":$("#update_by").val(),
                },
                function(data){
                    if(data.code<0){
                        alert(data.message);
                    }else{
                        alert("保存成功");
                        window.location = '/rolemanagement/userlevel/index';
                    }
                }, "json");
        }
    </script>

    <div class="head">
        <div class="title">
            ${Html.w(id==0,"新增","修改")}流程模型 ${Html.help("新建汇报线")}
        </div>
    </div>

    <div>
        <ul class="mydetail">
            <li>
                <label>工号</label><input class="mylongtext" type="text" id="user_no" name="user_no" value="${model.user_no!}" />
            </li>
            <li>
                <label>分管业务</label><input class="mylongtext" type="text" id="biz_name" name="biz_name" value="${model.biz_name!}" />
            </li>
            <li>
                <label>汇报对象工号</label><input class="mylongtext" type="text" id="next_user_no" name="next_user_no" value="${model.next_user_no!}" />
            </li>
<#--            <li>
                <label>创建人</label><input class="mylongtext" type="text" id="create_by" name="create_by" value="${model.create_by!}" />
            </li>-->
<#--            <li>
                <label>更新人</label><input class="mylongtext" type="text" id="update_by" name="update_by" value="${model.update_by!}" />
            </li>-->
            <li>
                <input type="button" class="btn1" style="margin-left: 300px" value="更新汇报线" onclick="return save()" />
            </li>
        </ul>
    </div>

    <script src="https://cdn.bootcss.com/gojs/1.8.13/go-debug.js"></script>


</@layout._layout>