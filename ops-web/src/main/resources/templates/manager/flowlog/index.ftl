﻿<#import "../../_layout.ftl" as layout>
${Html.s("pagetitle","流程日志查看")}
<@layout._layout>
<div class="head">
    <div class="title">
    ${pagetitle}
    </div>
</div>

<div class="orderlist m10 myorder">
    <form action="/manager/flowlog/index/" method="post" id="searchForm">
        <div class="search">
            <label>流程id</label><input type="text" class="text midtext" style="width:150px;" name="flow_id" value="${flow_id!}" />
            <label>流程标题</label><input type="text" class="text midtext" style="width:150px;" name="flow_title" value="${flow_title!}" />
            <label>日志</label><input type="text" class="text midtext" style="width:150px;" name="log" value="${log!}" />
            <label>类型</label>
            <select name="type">
                <option value="-1" ${Html.w(type=="0","selected='selected'","")}>所有</option>
                <option value="0" ${Html.w(type=="0","selected='selected'","")}>普通日志</option>
                <option value="1" ${Html.w(type=="1","selected='selected'","")}>错误日志</option>
                <option value="2" ${Html.w(type=="2","selected='selected'","")}>调试日志</option>
            </select>
            <input type="submit" class="btn1" value="搜索" accesskey="S" />
        </div>
    </form>
    <div class="tab_cont">
        <div class="List">
            <@_list/>
        </div>
    </div>
</div>
<script>
</script>
</@layout._layout>
<#macro _list >
<table class="mytable" width="100%">
    <tr>
        <th style="width:3%">Id</th>
        <th style="width:3%">流程id</th>
        <th style="width:10%">模型标题</th>
        <th style="width:30%">日志</th>
        <th style="width:3%">类型</th>
        <th style="width:5%">时间</th>
    </tr>
        <#list model as item>
            <tr data-id="${item.id!}">
                <td>${item.id!}</td>
                <td>${item.flow_id!}</td>
                <td style="text-align:left;">${item.flow_title!}</td>
                <td style="text-align:left" title="${item.log!}">${Html.substring3(item.log,100)!}</td>
                <td>${Html.enumDesc("com.yh.csx.bsf.flow.core.enums.FlowLogEnum",item.type)}</td>
                <td>${Html.p(item.create_time)}</td>
            </tr>
        </#list>
</table>
<div class="total pt10">
    <@layout._pager/>
</div>
</#macro>