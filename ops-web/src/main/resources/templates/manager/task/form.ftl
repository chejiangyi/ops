<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="/content/css/css.css" rel="stylesheet"/>
    <script src="/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
</head>
<body>
<script type="text/javascript">
    var variables = ${variables!};
    var flow = ${flow!};
    var task = ${task!};
    var permission=${permission!};
    var nodeTitle='${nodeTitle!}';
    $(function(){
        if(typeof(variables_init)!="function")
        {
            alert("表单未定义variables_init函数");
            return;
        }
        if(typeof(permission_init)!="function")
        {
            alert("表单未定义permission_init函数");
            return;
        }
//        if(variables[filed]!=undefined)
//        {$('#'+filed).val(variables[filed]);}
        variables_init();
        //alert(2);
        permission_init();
        //alert(3);
    });
//    第三方
//    function variables_init() {
//
//    }
//    function permission_init() {
//
//    }
//    function form_submit() {
//
//    }
</script>
<#--流程form表单-->
<div id="formdetail">
    ${form_html!}
</div>
</body>
</html>