﻿<#import "../../_layout.ftl" as layout>
${Html.s("pagetitle","任务列表")}
<@layout._layout>
<div class="head">
    <div class="title">
    ${pagetitle}
    </div>
</div>
<script type="text/javascript">
    function del(id) {
        if(!confirm("请确认任务不再使用,删除后不可恢复？"))
        {
            return;
        }
        $.ajax({
            url: '/manager/task/del/',
            type: "post",
            data: {
                id: id
            },
            success: function (data) {
                if (data.code > 0) {
                    window.location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    }
    function close(id) {
        if(!confirm("请确认任务不再使用,关闭后不可恢复？"))
        {
            return;
        }
        $.ajax({
            url: '/manager/task/close/',
            type: "post",
            data: {
                id: id
            },
            success: function (data) {
                if (data.code > 0) {
                    window.location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    }
</script>

<div class="orderlist m10 myorder">
    <form action="/manager/task/index/" method="post" id="searchForm">
        <div class="search">
            <label>任务id</label><input type="text" class="text midtext" style="width:150px;" name="id" value="${id!}" />
            <label>流程id</label><input type="text" class="text midtext" style="width:150px;" name="flow_id" value="${flow_id!}" />
            <label>流程标题</label><input type="text" class="text midtext" style="width:150px;" name="flow_title" value="${flow_title!}" />
            <label>审批节点</label><input type="text" class="text midtext" style="width:150px;" name="flow_approve_node" value="${flow_approve_node!}" />
            <label>审批人</label><input type="text" class="text midtext" style="width:200px;" name="flow_approve_by" value="${flow_approve_by!}" />
            <label>发起人</label><input type="text" class="text midtext" style="width:150px;" name="create_by" value="${create_by!}" />
            <label>任务状态</label>
            <select name="task_state">
                <option value="-1"  ${Html.w(task_state==-1,"selected='selected'","")}>所有</option>
                <#list Html.enums("com.yh.csx.bsf.flow.core.enums.TaskStateEnum") as item>
                    <option value="${item.value}" ${Html.w(task_state==item.value,"selected='selected'","")}>${item.desc}</option>
                </#list>
            </select>
            <label>创建时间</label><input type="text" class="text midtext" style="width:150px;" name="create_time_from" value="${create_time_from!}" /> 至 <input type="text" class="text midtext" style="width:150px;" name="create_time_to" value="${create_time_to!}" />
            <input type="submit" class="btn1" value="搜索" accesskey="S" />
        </div>
    </form>
    <div class="tab_cont">
        <div class="List">
            <@_list/>
        </div>
    </div>
</div>
</@layout._layout>
<#macro _list >
<#--<div>-->
    <#--<a href="/manager/task/add/" class="btn1" target="_blank">新增</a>-->
<#--</div>-->
<table class="mytable" width="100%">
    <tr>
        <th style="width:3%">Id</th>
        <th style="width:10%">流程标题</th>
        <th style="width:5%">审批节点</th>
        <th style="width:5%">审批人</th>
        <th style="width:3%">任务状态</th>
        <th style="width:3%">待分发审批人</th>
        <th style="width:3%">审批结果</th>
        <th style="width:5%">超时时间</th>
        <th style="width:5%">更新</th>
        <th style="width:5%">创建</th>
        <th style="width:7%">操作</th>
    </tr>
    <#list model as item>
        <tr data-id="${item.id}">
            <td>任务id:${item.id}<br/>流程id:${item.flow_id}</td>
            <td style="text-align:left;"><a href="/manager/flow/index/?id=${item.flow_id}" class="history" title="">${item.flow_title}</a></td>
            <td>${Html.w(item.flow_approve_node=="","无",item.flow_approve_node)}</td>
            <td style="text-align: left">${Html.w(item.flow_approve_by=="","无",item.flow_approve_by+"("+item.flow_approve_userno+")")}</td>
            <td>${Html.enumDesc("com.yh.csx.bsf.flow.core.enums.TaskStateEnum",item.task_state)}</td>
            <td>${item.next_userno}</td>
            <td>${item.task_result!}</td>
            <td style="text-align: left">超时周期:${item.timeout!}<br/>超时时间:${Html.p(item.timeout_time)!}</td>
            <td style="text-align: left">更新时间:${Html.p(item.update_time)}<br/>更新人:${item.update_by}</td>
            <td style="text-align: left">创建时间:${Html.p(item.create_time)}<br/>创建人:${item.create_by}</td>
            <td>
                <a href="/manager/task/detail/?id=${item.id}" class="history" title="">流程审批</a>
                <#if item.task_state !=2 && item.task_state !=3 >
                    <a href="javascript:close(${item.id})" class="del">关闭</a>
                </#if>
                <a href="javascript:del(${item.id})" class="del">强制删除</a>
            </td>
        </tr>
    </#list>
</table>
<div class="total pt10">
    <@layout._pager/>
</div>
</#macro>