﻿<#import "../../_layout.ftl" as layout>
${Html.s("pagetitle","审批流程")}
<@layout._layout>


<style>
    .mleft {
        margin-left: 200px;
    }
    .tb textarea{
        width: 95%;
    }
    .flowinfo td,.flowinfo td:last-child{
        text-align: left;
    }
    .flowinfo td label {
        text-align: left;
        margin-right: 0px;
        display:block;
    }
</style>

<script type="text/javascript">
    $(function(){
    });

    function save(action)
    {
        if(action == "转交"){
            $("#dialogTransfer").dialog({
                // bgiframe: true,
                resizable: true, //是否可以重置大小
                height: 150, //高度
                width: 250, //宽度
                draggable: false, //是否可以拖动。
                title: "转交用户工号",
                modal: true,
                open: function (e) {  //打开的时候触发的事件
                },
                close: function () { //关闭Dialog时候触发的事件

                }

            });
        }
        else{
            //approve
            //Integer id,String approveResult,String remark,String variables_json
            if(typeof($("#frform")[0].contentWindow.form_submit)!="function")
            {
                alert("表单未定义form_submit函数");
                return;
            }
            var formvariables = $("#frform")[0].contentWindow.form_submit();
            console.log("表单提交参数:"+JSON.stringify(formvariables));
            if(formvariables == false){
                alert("表单校验异常");
                return;
            }
            $.post("/manager/task/approve",
                    { "id":${task.id},
                        "approveResult":action,
                        "remark":$("#remark").val(),
                        "variables_json": JSON.stringify(formvariables)
                    },
                    function(data){
                        if(data.code>0){
                           alert("审批成功");
                            window.location = '/manager/task/index';
                        }else{
                            alert("转交出错:"+data.message);
                        }
                    }, "json");
        }
    }

    function getuser() {
        $.post("/manager/task/getuser",
                { "userno":$('#transferuser').val()
                },
                function(data){
                    if(data.code>0){
                        $('#userinfo').html(data.data);
                    }else{
                        $('#userinfo').html("");
                        alert(data.message);
                    }
                }, "json");
    }

    function dotransfer() {
        $.post("/manager/task/transfer",
                {
                    "id":${task.id},
                    "userno":$('#transferuser').val()
                },
                function(data){
                    if(data.code>0){
                       alert("转交成功");
                       window.location = '/manager/task/index';
                    }else{
                        alert("转交出错:"+data.message);
                    }
                }, "json");

    }
</script>
<#--转交-->
<div style="display:none;overflow:hidden;padding:3px" id="dialogTransfer">
    <span>工号:</span><input class="text midtext" style="width: 100px" type="text" id="transferuser" name="transferuser" value="" /><input class="btn1" style="margin-left: 5px" type="button" onclick="getuser()" value="查找">
    <div id="userinfo" style="text-align: center"></div>
    <div style="text-align: center;margin-top: 10px;"><input class="btn1" type="button" onclick="dotransfer()" value="确认转交"></div>
</div>
<div class="head">
    <div class="title">
    流程审批 ${Html.help("流程审批")}
    </div>
</div>
<div>
    <ul class="mydetail">
        <li>
            <label>任务流程信息</label>
            <div style="text-align:left;width: 80%;">
                <table id="tb-info" class="mytable mleft flowinfo"  width="50%">
                    <tbody>
                        <tr>
                            <td>
                                <label>当前节点:</label>${task.flow_approve_node!}
                            </td>
                            <td>
                                <label>当前审批人:</label>${task.flow_approve_by!}(${task.flow_approve_userno!})
                            </td>
                            <td>
                                <label>任务状态:</label>${Html.enumDesc("com.yh.csx.bsf.flow.core.enums.TaskStateEnum",task.task_state)!}
                            </td>
                            <td>
                                <label>任务更新时间:</label>${Html.p(task.update_time)!}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>流程节点:</label>${Html.w(flow.approve_node=="","无",flow.approve_node)!}
                            </td>
                            <td>
                                <label>流程发起人:</label>${flow.create_by!}(${flow.create_userno!})
                            </td>
                            <td>
                                <label>流程状态:</label>${Html.enumDesc("com.yh.csx.bsf.flow.core.enums.FlowStateEnum",flow.flow_state)!}
                            </td>
                            <td>
                                <label>流程创建时间:</label>${Html.p(flow.create_time)!}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </li>
        <li>
            <label>审批表单</label>
            <iframe class="mleft" frameborder="no" border="0" src="/manager/task/form/?taskid=${task.id}"  marginwidth="0" marginheight="0" id="frform"  scrolling="yes"  width="100%" height="100%"></iframe>
        </li>
        <li>
            <label>审批记录</label>
            <table id="tb-logs" class="mytable mleft" width="50%">
                <tbody>
                <tr>
                    <th style="width:5%">审批节点</th>
                    <th style="width:5%">审批人</th>
                    <th style="width:5%">审批结果</th>
                    <th style="width:20%">审批备注</th>
                    <th style="width:10%">审批时间</th>
                </tr>
                <#list approvelogs as item>
                <tr>
                    <td>${item.node!}</td>
                    <td>${item.user_by!}(${item.user_no!})</td>
                    <td>${item.result!}</td>
                    <td>${item.remark!}</td>
                    <td>${Html.p(item.update_time)!}</td>
                </tr>
                </#list>
                </tbody>
            </table>
        </li>
        <#if isapprove>
        <li>
            <label>审批操作</label>
            <div class="mleft">
                <textarea id="remark" name="remark" rows="5" cols="2" ></textarea>
                <div style="margin-top: 10px;">
                <#list approveResults as item>
                    <input class="btn1" style="margin-left: 2px;" type="button" onclick="save('${item}')" value="${item}">
                </#list>
                </div>
            </div>
        </li>
        </#if>
        <li>
            <label>审批流程图</label>
            <div id="myDiagramDiv" class="mleft" style="width:50%; height: 500px; background-color: #DAE4E4;"></div>
        </li>

    </ul>
</div>
<script src="https://cdn.bootcss.com/gojs/1.8.13/go-debug.js"></script>
<script src="/content/js/flow.js"></script>
<script>
    var model = paint(go.GraphLinksModel);
    //
    model.nodeDataArray =${parint_nodes!};
    //
    model.linkDataArray = ${parint_links!};
    myDiagram.model = model;
</script>
</@layout._layout>