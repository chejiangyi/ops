﻿<#import "../../_layout.ftl" as layout>
${Html.s("pagetitle","新建流程模型")}
<@layout._layout>
<style>
    .mleft {
        margin-left: 200px;
    }
    .toptd{
        text-align:left;
        vertical-align: top;
    }
    .tb textarea{
        width: 95%;
    }
</style>

<script type="text/javascript">
    $(function(){
        changetab('pc');
    });
    function deltr(item) {
        var a = $(item).parent().parent();
        if (a.prev().find('td').length >0) {
            a.remove();
        }else{
            alert("不能删除最后的元素内容");
        }
    }
    function add(tableid) {
        $("#"+tableid).find("tbody").append("<tr>"+$("#"+tableid).find("tr").last().html()+"</tr>");
        $("#"+tableid).find("tr").last().find("input").val("");
    }
    function up(item) {
       // alert(1);
        var a = $(item).parent().parent();
        //alert(a.prev().html());
        a.prev().before(a);
    }
    function down(item) {
       var a = $(item).parent().parent();
       a.next().after(a);
    }
    function changetab(div) {
        $('div[name="tab"]').hide();
        $('#'+div).show();
    }
    function save()
    {
        alert(jsonVariables());
        //Integer id,String name,String title,String variable_define_json,String nodes_define_json,String pc_form,String mobile_form,String form_permission_json
        $.post("/manager/model/save",
                { "id": ${id!"0"},"name":$("#name").val(),"title":$("#title").val(),
                    "variable_define_json":jsonVariables(),
                    "nodes_define_json":jsonNodes(),
                    "pc_form":JSON.stringify($('[filed="pc_form"]').val()),
                    "mobile_form":JSON.stringify($('[filed="mobile_form"]').val()),
                    "form_permission_json":jsonPermissions()
                },
                function(data){
                    if(data.code<0){
                        alert(data.message);
                    }else{
                        alert("保存成功");
                        window.location='/manage/model/index';
                    }
                }, "json");
    }
    function jsonVariables() {
        var rs = [];
        $('#tb-variables').find("tr").each(function(){
            var r={};
            r.k=$(this).find('[filed="k"]').val();
            r.v=$(this).find('[filed="v"]').val();
            r.d=$(this).find('[filed="d"]').val();
            //alert(r.k);
            if(r.k!=undefined) {
                rs.push(r);
            }
        });
        return JSON.stringify(rs);
    }
    function jsonNodes() {
        var rs = [];
        $('#tb-nodes').find("tr").each(function(){
            var r={};
            r.typeEnum=$(this).find('[filed="typeEnum"]').val();
            r.title=$(this).find('[filed="title"]').val();
            r.beginScript=$(this).find('[filed="beginScript"]').val();
            r.approveUser=$(this).find('[filed="approveUser"]').val();
            if(r.approveUser=="")
            {
                r.approveUser=null;
            }else{
                r.approveUser=eval(r.approveUser);
            }
            r.approveResults=$(this).find('[filed="approveResults"]').val();
            if(r.approveResults==""){
                r.approveResults=null;
            }
            r.endScript=$(this).find('[filed="endScript"]').val();
            r.startNotify=$(this).find('[filed="startNotify"]').val();
            r.endNotify=$(this).find('[filed="endNotify"]').val();
            r.timeOutRule=$(this).find('[filed="timeOutRule"]').val();
            if(r.timeOutRule=="")
            {
                r.timeOutRule=null;
            }
            if(r.typeEnum!=undefined) {
                rs.push(r);
            }
        });
        return JSON.stringify(rs);
    }
    function jsonPermissions() {
        var rs = [];
        $('#tb-nodes').find("tr").each(function(){
            var r={};
            r.node=$(this).find('[filed="node"]').val();
            r.field=$(this).find('[filed="field"]').val();
            r.endNotify=$(this).find('[filed="endNotify"]').val();
            r.approveUser=$(this).find('[filed="approveUser"]').val();
            if(r.node!=undefined) {
                rs.push(r);
            }
        });
        return JSON.stringify(rs);
    }

</script>
<div class="head">
    <div class="title">
    ${Html.w(Html.isnull(id),"新增","修改")}流程模型 ${Html.help("新建流程模型")}
    </div>
</div>
<div>
    <ul class="mydetail">
        <li>
            <label>模型名称</label><input class="mylongtext" type="text" id="name" name="name" value="${model.name!}" />
        </li>
        <li>
            <label>模型标题</label><input class="mylongtext" type="text" id="title" name="title" value="${model.title!}"/>
        </li>
        <li>
            <label>表单变量</label>
            <div style="text-align:left;width: 80%;">
                <a href="#" onclick="add('tb-variables');" class="btn1">新增变量</a>
                <table id="tb-variables" class="mytable mleft" width="50%">
                    <tbody>
                    <tr>
                        <th style="width:5%">变量key</th>
                        <th style="width:10%">变量value</th>
                        <th style="width:10%">描述</th>
                        <th style="width:2%">操作</th>
                    </tr>
                    <#list variables as item>
                        <tr>
                            <td><input type="text" filed="k" class="text midtext" value="${item.k}" /></td>
                            <td><input type="text" filed="v" class="text midtext" value="${item.v}" /></td>
                            <td><input class="text midtext" type="text" filed="d" value="${item.d}" /></td>
                            <td>
                                <a href="javascript:void(0)" onclick="deltr(this)" class="del">删除</a>
                            </td>
                        </tr>
                    </#list>
                    </tbody>
                </table>
            </div>
        </li>

        <li>
            <label>流程设计</label>
            <a href="#" class="btn1" style="margin-left: 0"  onclick="add('tb-nodes');">添加</a>
            <table id="tb-nodes" class="mytable tb mleft"  width="80%">
                <tr>
                    <th style="width:5%">流程节点</th>
                    <th style="width:10%">执行前脚本</th>
                    <th style="width:10%">审批人</th>
                    <th style="width:7%">审批结果</th>
                    <th style="width:10%">路由</th>
                    <th style="width:10%">执行后脚本</th>
                    <th style="width:10%">通知</th>
                    <th style="width:10%">超时规则</th>
                    <th style="width:4%;">操作</th>
                </tr>

                <#list nodes as item>
                    <tr >
                        <td class="toptd">
                            <select filed="typeEnum">
                                <#list Html.enums("com.yh.csx.bsf.flow.core.base.Model$TypeEnum") as ddd>
                                    <option value="${ddd}" ${Html.w(Html.filed2(item,"typeEnum.value")==ddd.value,"selected='selected'","")}>${ddd.desc}</option>
                                </#list>
                            </select>
                            <input type="text" filed="title" value="${item.title!}" />
                        </td>
                        <td class="toptd">
                            <textarea name="n-beginScript" rows="5" cols="3" filed="beginScript" >${item.beginScript!}</textarea>
                        </td>
                        <td class="toptd">
                            <textarea name="n-approveUser" rows="5" cols="3" filed="approveUser">${Html.empty(Html.toJson(item.approveUser))!}</textarea>
                        </td>
                        <td class="toptd">
                            <textarea name="n-approveResults" rows="5" cols="2" filed="approveResults">${Html.empty(Html.toJson(item.approveResults))!}</textarea>
                        </td>
                        <td class="toptd">
                            <textarea name="n-route" rows="5" cols="3" filed="routeScript">${item.routeScript!}</textarea>
                        </td>
                        <td class="toptd">
                            <textarea name="n-endScript" rows="5" cols="3" filed="endScript">${item.endScript!}</textarea>
                        </td>
                        <td class="toptd">
                            <div>提醒任务</div>
                            <select id="n-startnotify" filed="startNotify">
                                <#list Html.enums("com.yh.csx.bsf.flow.core.base.Model$NotifyEnum") as ddd>
                                    <option value="${ddd}" ${Html.w2("==",Html.filed2(item,"startNotify.value"),ddd.value,"selected='selected'","")}>${ddd.desc}</option>
                                </#list>
                            </select>
                            <div>通知发起人</div>
                            <select id="n-endnotify" filed="endNotify">
                                <#list Html.enums("com.yh.csx.bsf.flow.core.base.Model$NotifyEnum") as ddd>
                                    <option value="${ddd}" ${Html.w2("==",Html.filed2(item,"endNotify.value"),ddd.value,"selected='selected'","")}>${ddd.desc}</option>
                                </#list>
                            </select>
                        </td>
                        <td class="toptd">
                            <textarea name="n-timeoutScript" rows="5" cols="3" filed="timeOutRule" value="${Html.toJson(item.timeOutRule)!}"></textarea>
                        </td>
                        <td>
                            <a href="javascript:void(0)" style="margin-left: 0" class="btn1" onclick="deltr(this)">删除</a>
                            <a href="javascript:void(0)" style="margin-left: 0" class="btn1" onclick="up(this)">上移</a>
                            <a href="javascript:void(0)" style="margin-left: 0" class="btn1" onclick="down(this)">下移</a>
                        </td>
                    </tr>
                </#list>
            </table>
        </li>
        <li>
            <label>表单权限</label>
            <a href="#" class="btn1" style="margin-left: 0" onclick="add('tb-formpermission');">添加</a>
            <table id="tb-formpermission" class="mytable mleft" width="80%">
                <tr>
                    <th style="width:5%">节点</th>
                    <th style="width:10%">字段</th>
                    <th style="width:10%">读写</th>
                    <th style="width:10%">用户</th>
                    <th style="width:10%">操作</th>
                </tr>
                <#list formpermission as item>
                    <tr>
                        <td><input type="text" name="node" filed="node" value="${item.node}" /></td>
                        <td><input type="text" name="field" filed="field" value="${item.field}" /></td>
                        <td>
                            <select id="n-endnotify" filed="endNotify">
                                <#list Html.enums("com.yh.csx.bsf.flow.core.base.Model$PermissionEnum") as ddd>
                                    <option value="${ddd}" ${Html.w(Html.filed2(item,"endNotify.value")==ddd.value,"selected='selected'","")}>${ddd.desc}</option>
                                </#list>
                            </select>
                        </td>
                        <td>
                            <textarea name="f-approveUser" rows="5" cols="3" filed="approveUser" value="${Html.toJson(item.approveUser)!}"></textarea>
                        </td>
                        <td>
                            <a href="javascript:void(0)" onclick="deltr(this)" class="del">删除</a>
                        </td>
                    </tr>
                </#list>
            </table>
        </li>
        <li>
            <label>表单设计器</label>
            <input style="margin-left: 0px" type="button" value="Web-PC端" class="btn1" onclick="changetab('pc')"><input type="button" style="margin-left: 5px" value="Web-手机端" class="btn1" onclick="changetab('mobile')">
            <div id="pc" name="tab" class="mleft" style="margin-top: 5px">
                <div>PC显示Html</div>
                <textarea id="pc_html" rows="50" cols="20" filed="pc_form" style="width: 80%"></textarea>
            </div>
            <div id="mobile" name="tab" class="mleft" style="margin-top: 5px">
                <div>手机显示Html</div>
                <textarea id="mobile_html" rows="50" cols="20" filed="mobile_form" style="width: 80%"></textarea>
            </div>
        </li>
        <li>
            <input type="submit" class="btn1" value="提交" onclick="return save()" />
        </li>
    </ul>
</div>
</@layout._layout>