﻿<#import "../../_layout.ftl" as layout>
${Html.s("pagetitle",Html.w(id==0,"新建","修改")+"流程模型")}
<@layout._layout>
<style>
    .mleft {
        margin-left: 200px;
    }
    .tb textarea{
        width: 95%;
    }
</style>

<script type="text/javascript">
    $(function(){
        changeformtab('pc');
        changeflowtab('xml');
    });

    function changeformtab(div) {
        $('div [name="formtab"]').hide();
        $('#'+div).show();
    }
    function changeflowtab(div) {
        $('div [name="flowtab"]').hide();
        $('#'+div).show();
    }
    function save(type)
    {
        $.post("/manager/model/save",
                { "id": ${id!"0"},"sys_id":$("#sys_id").val(),"code":$("#code").val(),"title":$("#title").val(),
                    "xml":$("#textxml").val(),
                    "pc_form":JSON.stringify($('[filed="pc_form"]').val()),
                    "mobile_form":JSON.stringify($('[filed="mobile_form"]').val()),
                    'type':type,
                    'model_type':$('#model_type').val(),
                    "external_pc_form_url":$("#external_pc_form_url").val(),
                    "external_mobile_form_url":$("#external_mobile_form_url").val(),
                    "is_external_form":$("#is_external_form").val(),
                },
                function(data){
                    if(data.code<0){
                        alert(data.message);
                    }else{
                        alert("保存成功");
                        if(type!="debug") {
                            window.location = '/manager/model/index';
                        }
                    }
                }, "json");
    }

</script>
<div class="head">
    <div class="title">
    ${Html.w(id==0,"新增","修改")}流程模型 ${Html.help("新建流程模型")}
    </div>
</div>
<div>
    <ul class="mydetail">
        <li>
            <label>应用ID</label><input class="mylongtext" type="text" id="sys_id" name="sys_id" value="${model.sys_id!}" />
        </li>
        <li>
            <label>模型名称</label><input class="mylongtext" type="text" id="code" name="code" value="${model.code!}" />
        </li>
        <li>
            <label>模型标题</label><input class="mylongtext" type="text" id="title" name="title" value="${model.title!}"/>
        </li>
        <li>
            <label>模型类型</label>
            <select id="model_type" name="model_type">
                <#list Html.enums("com.yh.csx.bsf.flow.core.enums.ModelTypeEnum") as item>
                    <option value="${item.value}" ${Html.w(model.model_type==item.value,"selected='selected'","")}>${item.desc}</option>
                </#list>
            </select>
        </li>
        <li id="liflow">
            <label>流程设计器</label>
            <input style="margin-left: 0px" type="button" value="Xml编辑器" class="btn1" onclick="changeflowtab('xml')"><input type="button" style="margin-left: 5px" value="浏览" class="btn1" onclick="changeflowtab('flow');">
            <div id="xml" name="flowtab" class="mleft" style="margin-top: 5px">
                <div>Xml编辑器</div>
                <textarea id="textxml" rows="50" cols="20" style="width: 80%">${model.xml!}</textarea>
            </div>
            <div id="flow" name="flowtab" class="mleft" style="margin-top: 5px">
                <div>流程图(<b>若有更新则重新保存并刷新生效</b>)</div>
                <div id="myDiagramDiv" style="width:50%; height: 500px; background-color: #DAE4E4;"></div>
            </div>
        </li>
        <li>
            <label>是否外部表单</label>
            <select id="is_external_form" name="is_external_form">
                    <option value="1" selected="true">是</option>
                    <option value="0" >否</option>
            </select>
        </li>
        <li id="liflow">
            <label>表单url</label>
            <input style="margin-left: 0px" type="button" value="Web-PC端" class="btn1" onclick="changeformtab('pcurl')"><input type="button" style="margin-left: 5px" value="Web-手机端" class="btn1" onclick="changeformtab('mobileurl')">
            <div id="pcurl" name="formtab" class="mleft" style="margin-top: 5px">
                <div>PC显示url</div>
                <textarea id="external_pc_form_url" rows="1" cols="20" filed="external_pc_form_url" style="width: 80%">${model.external_pc_form_url!}</textarea>
            </div>
            <div id="mobileurl" name="formtab" class="mleft" style="margin-top: 5px">
                <div>手机显示url</div>
                <textarea id="external_mobile_form_url" rows="1" cols="20" filed="external_mobile_form_url" style="width: 80%">${model.external_mobile_form_url!}</textarea>
            </div>
        </li>

        <li id="liform">
            <label>表单设计器</label>
            <input style="margin-left: 0px" type="button" value="Web-PC端" class="btn1" onclick="changeformtab('pc')"><input type="button" style="margin-left: 5px" value="Web-手机端" class="btn1" onclick="changeformtab('mobile')">
            <div id="pc" name="formtab" class="mleft" style="margin-top: 5px">
                <div>PC显示Html</div>
                <textarea id="pc_html" rows="50" cols="20" filed="pc_form" style="width: 80%">${model.pc_form_url!}</textarea>
            </div>
            <div id="mobile" name="formtab" class="mleft" style="margin-top: 5px">
                <div>手机显示Html</div>
                <textarea id="mobile_html" rows="50" cols="20" filed="mobile_form" style="width: 80%">${model.mobile_form_url!}</textarea>
            </div>
        </li>
        <li>
            <#if id!=0>
                <input type="button" class="btn1" style="margin-left: 300px" value="保存当前版本" onclick="return save('debug')" />
                <input type="button" class="btn1" style="margin-left: 10px" value="发布新版本" onclick="return save('new')" />
            <#else>
                <input type="button" class="btn1" style="margin-left: 300px" value="发布新版本" onclick="return save('new')" />
            </#if>
        </li>
    </ul>
</div>
<script src="https://cdn.bootcss.com/gojs/1.8.13/go-debug.js"></script>
<script src="/content/js/flow.js"></script>
<script>
    var model = paint(go.GraphLinksModel);
    //
    model.nodeDataArray =${parint_nodes!};
    //
    model.linkDataArray = ${parint_links!};
    myDiagram.model = model;
</script>
</@layout._layout>