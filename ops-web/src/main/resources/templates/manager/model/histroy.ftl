<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="/content/css/css.css" rel="stylesheet"/>
    <script src="/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
</head>
<body>
<script>
    function del(id) {
        if(!confirm("请确认模型不再使用,删除后不可恢复？"))
        {
            return;
        }
        $.ajax({
            url: '/manager/model/delbyid/',
            type: "post",
            data: {
                id: id
            },
            success: function (data) {
                if (data.code > 0) {
                    window.location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    }

    function setUse(id) {
        if(!confirm("请确认使用该模型,从此刻起新流程将切换该版本？"))
        {
            return;
        }
        $.ajax({
            url: '/manager/model/setUse/',
            type: "post",
            data: {
                id: id
            },
            success: function (data) {
                if (data.code >0) {
                    window.location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    }
</script>
<table class="mytable" width="100%">
    <tr>
        <th style="width:5%">Id</th>
        <th style="width:10%">模型名称</th>
        <th style="width:10%">模型标题</th>
        <th style="width:5%">版本号</th>
        <th style="width:5%">更新时间</th>
        <th style="width:5%">更新人</th>
        <th style="width:5%">是否使用</th>
        <th style="width:15%">操作</th>
    </tr>
    <#list model as item>
        <tr data-id="${item.id}">
            <td>${item.id}</td>
            <td style="text-align:left;">${item.code}</td>
            <td style="text-align:left">${item.title}</td>
            <td>${item.version}</td>
            <td>${Html.p(item.update_time)}</td>
            <td>${item.update_by}</td>
            <td>${Html.w(item.is_use==0,"否","是")}</td>
            <td>
                <#if item.is_use==0>
                    <a href="javascript:setUse(${item.id})" class="history" title="">设置使用</a>
                </#if>
                <a href="/manager/model/detail/?id=${item.id}" target="_blank" class="edit">修改</a>
                <#if item.is_use==0>
                    <a href="javascript:del(${item.id})" class="del">删除</a>
                </#if>
            </td>
        </tr>
    </#list>
</table>
</body>
</html>