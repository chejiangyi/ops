﻿<#import "../../_layout.ftl" as layout>
${Html.s("pagetitle","模型列表")}
<@layout._layout>
<div class="head">
    <div class="title">
    ${pagetitle}
    </div>
</div>

<div class="orderlist m10 myorder">
    <form action="/manager/model/index/" method="post" id="searchForm">
        <div class="search">
            <label>模型名称</label><input type="text" class="text midtext" style="width:150px;" name="code" value="${code!}" />
            <label>模型标题</label><input type="text" class="text midtext" style="width:150px;" name="title" value="${title!}" />
            <label>创建人</label><input type="text" class="text midtext" style="width:200px;" name="user_by" value="${user_by!}" />
            <label>业务分类</label><input type="text" class="text midtext" style="width:150px;" name="model_category_id" value="${model_category_id!}" />
            <input type="submit" class="btn1" value="搜索" accesskey="S" />
        </div>
    </form>
    <div class="tab_cont">
        <div class="List">
            <@_list/>
        </div>
    </div>
</div>

<script>
    $(function(){
    });
function del(code) {
    if(!confirm("请确认模型不再使用,删除后不可恢复？"))
    {
        return;
    }
    $.ajax({
        url: '/manager/model/del/',
        type: "post",
        data: {
            code: code
        },
        success: function (data) {
            if (data.code > 0) {
                window.location.reload();
            }
            else {
                alert(data.message);
            }
        }
    });
}
function showHistory(code) {
    $("#frhistory").attr("src","/manager/model/histroy/?code=" + code); //设置IFRAME的SRC;
    $("#dialog").dialog({
        // bgiframe: true,
        resizable: true, //是否可以重置大小
        height: 423, //高度
        width: 626, //宽度
        draggable: false, //是否可以拖动。
        title: "历史版本",
        modal: true,
        open: function (e) {  //打开的时候触发的事件
        },
        close: function () { //关闭Dialog时候触发的事件
        }
    });
}
</script>
</@layout._layout>
<#macro _list >
<#--历史-->
<div style="display:none;overflow:hidden;padding:3px" id="dialog">
    <iframe frameborder="no" border="0" marginwidth="0" marginheight="0" id="frhistory"  scrolling="yes"  width="100%" height="100%"></iframe>
</div>
<div>
    <a href="/manager/model/detail/" class="btn1" target="_blank">新增</a>
</div>
<table class="mytable" width="100%">

    <tr>
        <th style="width:3%">Id</th>
        <th style="width:20%">模型名称</th>
        <th style="width:20%">模型标题</th>
        <th style="width:3%">版本号</th>
        <th style="width:7%">更新时间</th>
        <th style="width:3%">更新人</th>
        <th style="width:20%">业务分类</th>
        <th style="width:7%">操作</th>
    </tr>
        <#list model as item>
            <tr data-id="${item.id}">
            <td>${item.id}</td>
            <td style="text-align:left;">${item.code}</td>
            <td style="text-align:left" title="${item.title}">${item.title}</td>
            <td>${item.version}</td>
            <td>${Html.p(item.update_time)}</td>
            <td>${item.update_by}</td>
            <td>${item.model_category_id}</td>
            <td>
                <a href="javascript:showHistory('${item.code}')" class="history" title="">历史版本</a>
                <a href="/manager/model/detail/?id=${item.id}" target="_blank" class="edit">修改</a>
                <a href="javascript:del('${item.code}')" class="del">删除</a>
            </td>
            </tr>
        </#list>
</table>
<div class="total pt10">
    <@layout._pager/>
</div>
</#macro>