<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="/content/css/css.css" rel="stylesheet"/>
    <script src="/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
</head>
<body>
<script>
    function save(id) {
        if(!confirm("请确认更新,可能影响线上流程？"))
        {
            return;
        }
        var json = jsonVariables();
        //alert(json);
        $.ajax({
            url: '/manager/flow/saveVariable/',
            type: "post",
            data: {
                id: id,
                variable_json:json
            },
            success: function (data) {
                if (data.code >0) {
                    alert("保存成功");
                }
                else {
                    alert(data.message);
                }
            }
        });
    }
    function deltr(item) {
        var a = $(item).parent().parent();
        //alert(a.parent().find('tr').length );
        if (a.parent().find('tr').length >2) {
            a.remove();
        }else{
            alert("不能删除最后的元素内容");
        }
    }
    function add(tableid) {
        $("#"+tableid).find("tbody").append("<tr>"+$("#"+tableid).find("tr").last().html()+"</tr>");
        $("#"+tableid).find("tr").last().find("input").val("");
    }
    function jsonVariables() {
        var rs = [];
        $('#tb-variables').find("tr").each(function(){
            var r={};
            r.key=$(this).find('[name="key"]').val();
            r.value=$(this).find('[name="value"]').val();
            r.desc=$(this).find('[name="desc"]').val();
            if(r.key!=undefined) {
                rs.push(r);
            }
        });
        return JSON.stringify(rs);
    }
</script>
<div>
    <ul class="mydetail">
        <li>
            <a href="javascript:add('tb-variables')" style="margin-left: 0px" class="btn1">新增变量</a>
            <table id="tb-variables" class="mytable" width="100%">
                <tr>
                    <th style="width:10%">变量key</th>
                    <th style="width:10%">变量value</th>
                    <th style="width:10%">描述</th>
                    <th style="width:3%">操作</th>
                </tr>
                <#list variables as item>
                    <tr>
                        <td><input style="width: 100%" type="text" name="key"  value="${item.key!}" /></td>
                        <td><input style="width: 100%" type="text" name="value" value="${item.value!}" /></td>
                        <td><input style="width: 100%" type="text" name="desc"  value="${item.desc!}" /></td>
                        <td>
                            <a href="javascript:" class="del" onclick="deltr(this)">删除</a>
                        </td>
                    </tr>
                </#list>
            </table>
        </li>
        <li>
            <input type="submit" class="btn1" value="提交" onclick="return save('${id}')" />
        </li>
    </ul>
</div>
</body>
</html>