<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="/content/css/css.css" rel="stylesheet"/>
    <script src="/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
</head>
<body>
<script>
    function setUse(id,modelid) {
        if(!confirm("请确认模型不再使用,删除后不可恢复？"))
        {
            return;
        }
        $.ajax({
            url: '/manager/flow/setUse/',
            type: "post",
            data: {
                id: id,
                modelid:modelid
            },
            success: function (data) {
                if (data.code >0) {
                    window.location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    }
</script>
<table class="mytable" width="100%">
    <tr>
        <th style="width:5%">Id</th>
        <th style="width:10%">模型名称</th>
        <th style="width:10%">模型标题</th>
        <th style="width:5%">版本号</th>
        <th style="width:5%">更新时间</th>
        <th style="width:5%">更新人</th>
        <th style="width:5%">是否使用</th>
        <th style="width:15%">操作</th>
    </tr>
    <#list models as item>
        <tr data-id="${item.id}">
            <td>${item.id}</td>
            <td style="text-align:left;">${item.name}</td>
            <td style="text-align:left">${item.title}</td>
            <td>${item.version}</td>
            <td>${Html.p(item.update_time)}</td>
            <td>${item.update_by}</td>
            <td>${Html.w(item.id==currentid,"是","否")}</td>
            <td>
                <#if item.id!=currentid>
                    <a href="javascript:" onclick="setUse('${id}','${item.id}')" title="">切换版本</a>
                </#if>
            </td>
        </tr>
    </#list>
</table>
</body>
</html>