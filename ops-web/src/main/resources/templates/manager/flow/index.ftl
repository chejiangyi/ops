﻿<#import "../../_layout.ftl" as layout>
${Html.s("pagetitle","流程列表")}
<@layout._layout>
<div class="head">
    <div class="title">
    ${pagetitle}
    </div>
</div>

<div class="orderlist m10 myorder">
    <form action="/manager/flow/index/" method="post" id="searchForm">
        <div class="search">
            <label>流程id</label><input type="text" class="text midtext" style="width:150px;" name="id" value="${id!}" />
            <label>流程标题</label><input type="text" class="text midtext" style="width:150px;" name="title" value="${title!}" />
            <label>审批节点</label><input type="text" class="text midtext" style="width:150px;" name="approve_node" value="${approve_node!}" />
            <label>审批人</label><input type="text" class="text midtext" style="width:200px;" name="approve_by" value="${approve_by!}" />
            <label>发起人</label><input type="text" class="text midtext" style="width:150px;" name="create_by" value="${create_by!}" />
            <label>流程状态</label>
            <select name="flow_state">
                <option value="-1"  ${Html.w(flow_state==-1,"selected='selected'","")}>所有</option>
                <#list Html.enums("com.yh.csx.bsf.flow.core.FlowStateEnum") as item>
                    <option value="${item.value}" ${Html.w(flow_state==item.value,"selected='selected'","")}>${item.desc}</option>
                </#list>
            </select>
            <label>流程模型名称</label><input type="text" class="text midtext" style="width:150px;" name="model_code" value="${model_code!}" />
            <label>流程模型版本</label><input type="text" class="text midtext" style="width:150px;" name="model_version" value="${model_version!}" />
            <label>创建时间</label><input type="text" class="text midtext" style="width:150px;" name="create_time_from" value="${create_time_from!}" /> 至 <input type="text" class="text midtext" style="width:150px;" name="create_time_to" value="${create_time_to!}" />
            <input type="submit" class="btn1" value="搜索" accesskey="S" />
        </div>
    </form>
    <div class="tab_cont">
        <div class="List">
            <@_list/>
        </div>
    </div>
</div>
<script>
    function del(id) {
        if(!confirm("请确认模型不再使用,删除后不可恢复？"))
        {
            return;
        }
        $.ajax({
            url: '/manager/flow/del/',
            type: "post",
            data: {
                id: id
            },
            success: function (data) {
                if (data.code > 0) {
                    window.location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    }
    function show(name,url,title,height,width) {
        $("#fr"+name).attr("src",url); //设置IFRAME的SRC;
        $("#dialog"+name).dialog({
            // bgiframe: true,
            resizable: true, //是否可以重置大小
            height: height, //高度
            width: width, //宽度
            draggable: false, //是否可以拖动。
            title: title,
            modal: true,
            open: function (e) {  //打开的时候触发的事件
            },
            close: function () { //关闭Dialog时候触发的事件

            }

        });
    }
    function close(id) {
        if(!confirm("请确认流程不再使用,关闭后不可恢复？"))
        {
            return;
        }
        $.ajax({
            url: '/manager/flow/close/',
            type: "post",
            data: {
                id: id
            },
            success: function (data) {
                if (data.code >0) {
                    window.location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    }
    function reflow(id) {
        if(!confirm("请确认流程重新发起,将关闭所有任务重启流程？"))
        {
            return;
        }
        $.ajax({
            url: '/manager/flow/reflow/',
            type: "post",
            data: {
                id: id
            },
            success: function (data) {
                if (data.code >0) {
                    window.location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    }
</script>
</@layout._layout>
<#macro _list >
<#--修改变量-->
<div style="display:none;overflow:hidden;padding:3px" id="dialogvariables">
    <iframe frameborder="no" border="0" marginwidth="0" marginheight="0" id="frvariables"  scrolling="yes"  width="100%" height="100%"></iframe>
</div>
<#--切换模型版本-->
<div style="display:none;overflow:hidden;padding:3px" id="dialogmodels">
    <iframe frameborder="no" border="0" marginwidth="0" marginheight="0" id="frmodels"  scrolling="yes"  width="100%" height="100%"></iframe>
</div>
<#--切换节点-->
<div style="display:none;overflow:hidden;padding:3px" id="dialognodes">
    <iframe frameborder="no" border="0" marginwidth="0" marginheight="0" id="frnodes"  scrolling="yes"  width="100%" height="100%"></iframe>
</div>
<#--<div>-->
    <#--<a href="/manager/flow/add/" class="btn1" target="_blank">新增</a>-->
<#--</div>-->
<table class="mytable" width="100%">
    <tr>
        <th style="width:3%">Id</th>
        <th style="width:10%">流程标题</th>
        <th style="width:5%">审批节点</th>
        <th style="width:5%;">审批人</th>
        <th style="width:5%">流程状态</th>
        <th style="width:5%">发起人</th>
        <th style="width:5%">更新时间</th>
        <th style="width:5%">异常信息</th>
        <th style="width:5%">流程模型</th>
        <th style="width:5%">创建时间</th>
        <th style="width:15%">操作</th>
    </tr>
    <#list model as item>
        <tr data-id="${item.id}">
            <td>${item.id}</td>
            <td style="text-align:left;">${item.title}</td>
            <td>${Html.w(item.approve_node=="","无",item.approve_node)}</td>
            <td style="text-align:left">${Html.w(item.approve_by=="","无",item.approve_by+"("+item.approve_userno+")")}</td>
            <td>${Html.enumDesc("com.yh.csx.bsf.flow.core.FlowStateEnum",item.flow_state)}</td>
            <td>${item.create_by} (${item.create_userno})</td>
            <td>${Html.p(item.update_time)}</td>
            <td title="${item.flow_error}">${Html.substring3(item.flow_error,100)}</td>
            <td><a href="/manager/model/index/?name=${item.model_code}">${item.model_code}</a> (<a href="/manager/model/detail/?id=${item.model_id}">${item.model_version}</a>)</td>
            <td>${Html.p(item.create_time)}</td>
            <td>
                <a href="/manager/task/index/?flow_id=${item.id}" class="history" title="">流程任务</a>
                <#if item.flow_state!=3&&item.flow_state!=4>
                    <a href="javascript:close(${item.id})" class="del">关闭</a>
                </#if>
                <a href="javascript:reflow(${item.id})" class="reflow" title="">重新发起</a><br/>
                <a href='javascript:show("variables","/manager/flow/changevariable/?id=${item.id}","修改变量",500,600)' class="edit">修改变量</a>
                <a href='javascript:show("models","/manager/flow/changemodel/?id=${item.id}","切换模型版本",400,600)' class="edit">切换模型版本</a>
                <a href='javascript:show("nodes","/manager/flow/changenode/?id=${item.id}","切换节点",400,600)'>切换节点</a>
                <a href="javascript:del(${item.id})" class="del">强制删除</a>
            </td>
        </tr>
    </#list>
</table>
<div class="total pt10">
    <@layout._pager/>
</div>
</#macro>