<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <link href="/content/css/css.css" rel="stylesheet"/>
    <script src="/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
</head>
<body>
<style>

</style>

<script>
    function save(id) {
        if(!confirm("请确认更新,可能影响线上流程？"))
        {
            return;
        }
        $.ajax({
            url: '/manager/flow/saveNode/',
            type: "post",
            data: {
                id: id,
                node:$('#nodes').val()
            },
            success: function (data) {
                if (data.code >0) {
                    window.location.reload();
                }
                else {
                    alert(data.message);
                }
            }
        });
    }
</script>
<div>
    <ul class="mydetail">
        <li>
            <label>切换流程节点</label>
            <select id="nodes">
                <#list nodes as item>
                    <option value="${item.title}">${item.title}</option>
                </#list>
            </select>
        </li>
        <li>
            <input type="submit" class="btn1" value="提交" onclick="return save('${id}')" />
        </li>
    </ul>
</div>
</body>
</html>