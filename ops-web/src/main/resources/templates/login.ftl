﻿<#assign pagetitle="登陆" >

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>${pagetitle}!</title>
    <link href="/content/css/css.css" rel="stylesheet"/>
    <link href="/content/css/login.css" rel="stylesheet"/>
    <script type="text/javascript">
    </script>

<style>
#wrap .logGet {
	height: 338px;
	width: 368px;  
	position: absolute;
	background-color: #FFFFFF;
	top: 20%;
	right: 50%-184;
}
.logC input {
	width: 100%;
	height: 45px;
	background-color: #ee7700;
	border: none;
	color: white;
	font-size: 18px;
}
.logGet .logD.logDtip .p1 {
	display: inline-block;
	font-size: 28px;
	margin-top: 30px;
	width: 86%;
}
#wrap .logGet .logD.logDtip {
	width: 86%;
	margin-bottom: 20px;
	margin-top: 0px;
	margin-right: auto;
	margin-left: auto;
	color:#000;
	text-align:center;
	
}
.logGet .lgD img {
	position: absolute;
	top: 12px;
	left: 8px;
}
.logGet .lgD input {
	width: 100%;
	height: 42px;
	text-indent: 2.5rem;
}
#wrap .logGet .lgD {
	width: 86%;
	position: relative;
	margin-bottom: 30px;
	margin-top: 30px;
	margin-right: auto;
	margin-left: auto;
}
#wrap .logGet .logC {
	width: 86%;
	margin-top: 0px;
	margin-right: auto;
	margin-bottom: 0px;
	margin-left: auto;
}


</style>
</head>
<body>

<div class="login">
<form action="/user/doLogin" method="POST">
   <div class="wrap" id="wrap">
	<div class="logGet">
			<!-- 头部提示信息 -->
			<div class="logD logDtip">
				<p class="p1">流程管理系统</p>
			</div>
			<!-- 输入框 -->
			<div class="lgD">			
				<input type="text" name="username" id="username"
					placeholder="输入用户名" />
			</div>
			<div class="lgD">			
				<input type="password" name="password" id="password"
					placeholder="输入用户密码" />
			</div>
			<div class="logC">
				<a href="index.html" target="_self"></a><input type="submit" value="登 录"/>
			</div>
		</div>
</div>
	</form>
<#if error??>
    <span sytle='color:red'>${error!}</span>
</#if>
</div>
</body>
</html>
