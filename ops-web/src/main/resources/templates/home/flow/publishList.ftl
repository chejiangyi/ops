﻿<#assign pagetitle="发布列表" >

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>${pagetitle}</title>
    <script type="text/javascript">
    </script>
    <link rel="stylesheet" href="/content/layui-v2.5.7/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div>
    <fieldset class="layui-elem-field site-demo-button" style="margin-top: 30px;">
        <legend>发布列表</legend>
        <div>
            <button type="button" id="addBtn" class="layui-btn layui-btn-primary" >新增</button>
        </div>
    </fieldset>
</div>
<table class="layui-hide" id="userList" lay-filter="userListFilter"></table>
<!--编辑组件-->
<script type="text/html" id="barDemo">
    <#--<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>-->
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-xs" lay-event="publishManager">发布管理</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<!--新增对话框-->
<div id="add-user-con" style="display: none;">
    <form class="layui-form" id="add-form"  lay-filter="add-form-filter">
        <div class="layui-form-item center" >
            <label class="layui-form-label" style="width: 100px" >名称</label>
            <div class="layui-input-block">
                <input type="text" name="name" required value="" style="width: 240px" lay-verify="required" placeholder="请输入配置名称" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 100px">应用名</label>
            <div class="layui-input-block">
                <input type="text" name="app" required style="width: 240px" lay-verify="required" placeholder="请输入配置类型" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 100px">版本号&emsp;</label>
            <div class="layui-input-block">
                <input type="text" name="version" required style="width: 240px" lay-verify="required" placeholder="请输入配置值" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 100px">发布流程定义 <a href="javascript::" style="color:red;font-size: 18px" class="edit-publish-btn">编辑</a></label>
            <div class="layui-input-block">
                <iframe src="/home/addProcessUI?isView=true" id="publish-process-iframe" style="width:675px;height: 100px;"></iframe>
            </div>

        </div>
        <div class="layui-form-item">
            <label class="layui-form-label" style="width: 100px">回滚流程定义 <a href="javascript::"  style="color:red;font-size: 18px" class="rollback-publish-btn">编辑</a></label>
            <div class="layui-input-block">
                <iframe src="/home/addProcessUI?isView=true"  id="rollback-process-iframe"  style="width:675px;height: 100px;"></iframe>
            </div>
        </div>
        <!--流程定义json-->
        <div class="layui-form-item">
            <div class="layui-input-block">
                <input type="hidden" id="commonFlow" name="commonFlow" required style="width: 240px" lay-verify="required" placeholder="请输入配置值" autocomplete="off" class="layui-input">
                <input type="hidden" id="rollFlow" name="rollFlow" required style="width: 240px" lay-verify="required" placeholder="请输入配置值" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" id="save-btn" lay-submit lay-filter="save" >保存</button>
                <button type="reset" class="layui-btn layui-btn-primary" id="closeBtn" >关闭</button>
            </div>
        </div>
    </form>
</div>


<script src="/scripts/jquery-1.8.2.js"></script>
<!--ajax请求工具类-->
<script src="/util/mm.js"></script>
<script src="/content/layui-v2.5.7/layui/layui.js"></script>

<script>
    layui.use(['element', 'layer', 'jquery','table','form'], function () {
        var $ = layui.$;
        var layer = layui.layer;
        var element = layui.element;
        var form = layui.form;
        var table = layui.table;

        var dialogIndex;

        //0 是新增  1是编辑
        var saveOrUpdateFlag=0;
        //当前编辑记录id
        var editId;
        //默认新增操作
        var  requestUrl="${request.contextPath}/publish/save";

        //渲染表格
        table.render({
            elem: '#userList'
            ,url:'/publish/pageList'
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,cols: [[
                {field:'id', width:80, title: 'ID', sort: true}
                ,{field:'name', width:180, title: '发布名称'}
                ,{field:'app',  title: '应用名'}
                ,{field:'version', width:80, title: '版本号'}
                ,{field:'createUser', title: '创建人', }
                ,{field:'createUser', title: '更新人', }
                ,{field:'updateTime', title: '更新时间',}
                ,{field:'createTime', title: '创建时间'}
                ,{fixed: 'right', width: 190, align:'center', toolbar: '#barDemo'}
            ]]
            ,page: true //开启分页
            ,request:{  //更改分页请求参数
                pageName:'pageIndex',
                limitName:'pageSize'
            }
            ,limit: 10 //每页默认显示的数量
        });


        //监听行工具事件
        table.on('tool(userListFilter)', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data //获得当前行数据
                    ,layEvent = obj.event; //获得 lay-event 对应的值
            if(layEvent === 'publishManager'){
                // layer.msg('点击了发布管理');

                //打开发布管理页面
                openwindow('/home/publishManagerUI?isView=false&id='+data.id,"发布管理",700,400);

            } else if(layEvent === 'del'){
                layer.confirm('真的删除行么', function(index){
                    //向服务端发送删除指令
                    _mm.request({
                        "method":"post",
                        "url":"${request.contextPath}/publish/delete",
                        "data":{
                            id:	data.id
                        },
                        success:function (res) {
                            layer.msg("删除成功");
                            obj.del(); //删除对应行（tr）的DOM结构
                            layer.close(index);
                            //执行重载
                            table.reload('userList', {}, 'data');
                        },
                        error:function (error) {
                            layer.msg("删除出错了，请稍后再试")
                        }
                    });
                });
            } else if(layEvent === 'edit'){
                saveOrUpdateFlag=1;
                //layer.msg('编辑操作');
                editId=data.id;
                _mm.request({
                    "method":"get",
                    "url":"${request.contextPath}/publish/detail",
                    "data":{
                        id:	editId
                    },
                    success:function (res) {
                        requestUrl="${request.contextPath}/publish/update";
                        var title='编辑发布';
                        //回显表单
                        form.val('add-form-filter',{
                            //表单回显
                            "name":res.name,
                            "app":res.app,
                            "version":res.version,
                            "commonFlow":res.commonFlow,
                            "rollFlow":res.rollFlow,
                            "serverAuth":res.serverAuth
                        })
                        //回显对应的iframe编辑流程
                        $("#publish-process-iframe").attr("src","/home/addProcessUI?isView=true&id="+res.id+"&type=normal")
                        $("#rollback-process-iframe").attr("src","/home/addProcessUI?isView=true&id="+res.id+"&type=rollback")

                        $("#save-btn").html("修改");
                        //添加主键id
                        $("#add-form").append("<input type='hidden' name='id' value='"+res.id+"'/>");
                        showSaveOrUpdateDialog(title);
                    },
                    error:function (error) {
                        layer.msg("编辑出错了，请稍后再试")
                    }
                });
            }
        });


        //表单保存监听时间
        form.on('submit(save)', function (data) {
            debugger;
            _mm.request({
                "method":"post",
                "url":requestUrl,
                "data":data.field,
                success:function (res) {
                    layer.msg("保存成功");
                    //执行重载
                    table.reload('userList', {}, 'data');
                    return false;
                },
                error:function (error) {
                    layer.msg("退出出错了，请稍后再试")
                    return false;
                }
            });
        });

        //新增按钮点击事件
        $("#addBtn").click(function(){
            //新增操作
            saveOrUpdateFlag=0;
            var title='新增发布';
            //回显对应的iframe编辑流程
            $("#publish-process-iframe").attr("src","/home/addProcessUI?isView=true&type=normal")
            $("#rollback-process-iframe").attr("src","/home/addProcessUI?isView=true&type=rollback")
            showSaveOrUpdateDialog(title);
        });


        //关闭对话框事件
        $("#closeBtn").click(function(){
            CloseWin();
        });


        function showSaveOrUpdateDialog(title){
            //页面层-自定义
            dialogIndex=layer.open({
                type: 1,
                title:title,
                closeBtn: false,
                shift: 2,
                area: ['800px', '600px'],
                shadeClose: true,
                //btn: ['新增', '取消'],
                // btnAlign: 'c',
                content: $("#add-user-con"),
                success: function(layero, index){

                },
                yes:function(){

                }
            });
        }


        //关闭页面
        function CloseWin(){
            //parent.location.reload(); // 父页面刷新
            // var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            // parent.layer.close(index); //再执行关闭
            if(dialogIndex){
                layer.close(dialogIndex);
                dialogIndex="";
            }
        }





        //发布流程定义编辑按钮
        $(".edit-publish-btn").click(function(){
            if(saveOrUpdateFlag===1){
                //编辑操作,回显原来定义的流程,并且允许编辑
                window .open("/home/addProcessUI?isView=false&id="+editId+"&type=normal","child","status=yes,toolbar=no,menubar=no,location=no");
            }else {
                //新增操作
                window.open("/home/addProcessUI?isView=false&type=normal", "child", "status=yes,toolbar=no,menubar=no,location=no");
            }

        });




        //回滚流程定义编辑按钮
        $(".rollback-publish-btn").click(function(){
            if(saveOrUpdateFlag===1) {
                //编辑操作,回显原来定义的流程,并且允许编辑
                window .open("/home/addProcessUI?isView=false&id="+editId+"&type=rollback","child","status=yes,toolbar=no,menubar=no,location=no");
            }else{
                //新增操作
                window.open("/home/addProcessUI?isView=false&type=rollback", "child", "status=yes,toolbar=no,menubar=no,location=no");
            }
        });



    })


    //正常流程回调
    function  saveFlowCallback(json){
        document.getElementById("commonFlow").value=JSON.stringify(json);
    }



    //回滚流程回调
    function  roolbackFlowCallback(json){
        document.getElementById("rollFlow").value=JSON.stringify(json);
    }

    function openwindow(url,name,iWidth,iHeight)
    {
        var url;                            //转向网页的地址;
        var name;                           //网页名称，可为空;
        var iWidth;                         //弹出窗口的宽度;
        var iHeight;                        //弹出窗口的高度;
        //window.screen.height获得屏幕的高，window.screen.width获得屏幕的宽
        var iTop = (window.screen.height-30-iHeight)/2;       //获得窗口的垂直位置;
        var iLeft = (window.screen.width-10-iWidth)/2;        //获得窗口的水平位置;
        window.open(url,name,'height='+iHeight+',,innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',top='+iTop+',left='+iLeft+',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no');
    }
</script>
</body>
</html>
