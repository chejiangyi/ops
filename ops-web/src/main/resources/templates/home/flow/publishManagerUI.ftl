﻿<#assign pagetitle="发布管理" >

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>${pagetitle}!</title>
    <script type="text/javascript">
    </script>
    <link rel="stylesheet" href="/content/layui-v2.5.7/layui/css/layui.css">
    <style>
        .publish-body{
            height: 800px;
            border: solid 1px #dcdcdc;
            padding: 58px;
        }
        .title{
            text-align: center;
            font-size: 24px;
            font-weight: bold;
        }

        .console-log{
            height:300px;
            border: solid 1px grey;
            overflow: scroll;
        }

    </style>
</head>
<body >
<!--新增对话框-->
<div class="publish-body">
    <div>
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">名称</label>
                    <div class="layui-input-inline">
                        <input type="text" name="number"  autocomplete="off" class="layui-input"  value="${publisModel.name}" readonly>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">应用</label>
                    <div class="layui-input-inline">
                        <input type="text" name="date" id="date"  placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input"  value="${publisModel.app}" readonly>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">版本号</label>
                    <div class="layui-input-inline">
                        <input type="tel" name="url"  autocomplete="off" class="layui-input"  value="${publisModel.version}"  readonly>
                    </div>
                </div>
            </div>
            <div class="publish-con">
                <div class="layui-form-item">
                    <label class="layui-form-label">发布流程</label>
                    <div class="layui-input-inline">
                        <button type="button" id="onlyPublishBtn" class="layui-btn" lay-submit lay-filter="queryBtnFilter" >仅发布</button>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label"></label>
                    <div class="layui-inline">
                        <iframe src="/home/addProcessUI?isView=true&type=normal&id=${publisModel.id}&manager=true" id="publish-process-iframe" style="width:850px;height: 100px;"></iframe>
                    </div>
                </div>
           <div>
           <div class="publish-con">
                   <div class="layui-form-item">
                       <label class="layui-form-label">回滚流程</label>
                       <div class="layui-input-inline">
                           <button type="button" id="onlyRobackBtn" class="layui-btn" lay-submit lay-filter="queryBtnFilter" >仅回滚</button>
                       </div>
                   </div>
                   <div class="layui-form-item">
                       <label class="layui-form-label"></label>
                       <div class="layui-inline">
                           <iframe src="/home/addProcessUI?isView=true&type=rollback&id=${publisModel.id}&manager=true" id="publish-process-iframe" style="width:850px;height: 100px;"></iframe>
                       </div>
                   </div>
            <div>

            <#--<div class="layui-form-item">-->
                <#--<label class="layui-form-label"></label>-->
                <#--<div class="layui-input-block">-->
                    <#--<button type="button" id="autoPublishBtn" class="layui-btn" lay-submit lay-filter="queryBtnFilter" >一键发布</button>-->
                <#--</div>-->
            <#--</div>-->


            <div class="layui-form-item">
                    <label class="layui-form-label">发布历史(显示最近30条)</label>
                    <div class="layui-input-inline" style="width:400px">
                        <select name="fileNameList" lay-verify=""  lay-search id="fileNameList"  lay-filter="search_type">
                            <#list publisLogModel as item>
                                <option value="${item.name}">${item.name}</option>
                            </#list>
                        </select>
                    </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">控制台</label>
                <div class="layui-input-block console-log" id="console-log">
                   <pre class="console-output" id="consoleLogContent">
                    </pre>
                </div>
            </div>
        </form>
    </div>
</div>


<script src="/scripts/jquery-1.8.2.js"></script>
<!--ajax请求工具类-->
<script src="/util/mm.js"></script>
<script src="/content/layui-v2.5.7/layui/layui.js"></script>
<script>
    var url;
    var lastTimeFileSize=0;
    var timer;
    var scrollTimer;

    var scrollh=0;
    var delay=180;

    //JavaScript代码区域
    layui.use(['element', 'layer', 'jquery','table','form'], function(){
        var element = layui.element;
        var $ = layui.$;
        var form=layui.form;
        form.render();


        form.on('select(search_type)', function(data){
            console.log(data.elem); //得到select原始DOM对象
            console.log(data.value); //得到被选中的值
            console.log(data.othis); //得到美化后的DOM对象
            loadLog(data.value);
        });

        //初始化加载
        $(function(){
            readLog();//初始化加载日志
        })


        //仅发布点击事件
        $("#onlyPublishBtn").click(function () {
            var id=_mm.getUrlParam('id');
            var result=check(id);
            if('error'===result){
                return;
            }
            generatePublishLogFileByPublishId('normal',id).then(function (res) {
                //调用调用当前回调;
                refresh(res)
                //读取数据库发布记录，进行发布操作
                _mm.request({
                    "method":"post",
                    "url":'${request.contextPath}/publish/publishOper',
                    "data":{
                        "id":id,
                        "type":'normal',
                        "logFileNname":res
                    },
                    success:function (res) {
                        alert("发布成功");
                    },
                    error:function (error) {
                        alert(context);
                    }
                });
            },function(reason){

            })
        });


        //仅回滚点击事件
        $("#onlyRobackBtn").click(function () {
            var id=_mm.getUrlParam('id');
            var type=_mm.getUrlParam('type');
            var result=check(id,type);
            if('error'===result){
                return;
            }
            generatePublishLogFileByPublishId('rollback',id).then(function (res) {
                //调用当前刷新日志;
                refresh(res)
                //读取数据库发布记录，进行发布操作
                _mm.request({
                    "method":"post",
                    "url":'${request.contextPath}/publish/publishOper',
                    "data":{
                        "id":id,
                        "type":'rollback',
                        "logFileNname":res
                    },
                    success:function (res) {
                        alert("回滚成功");
                    },
                    error:function (error) {
                        alert(error);
                    }
                });
            },function(reason){

            })
        });



        //一键发布点击事件
        $("#autoPublishBtn").click(function () {
            var id=_mm.getUrlParam('id');
            var type=_mm.getUrlParam('type');
            var result=check(id,type);
            if('error'===result){
                return;
            }
            //读取数据库发布记录，进行发布操作
            _mm.request({
                "method":"post",
                "url":'${request.contextPath}/publish/autoPublishOper',
                "data":{
                    "id":id,
                },
                success:function (res) {
                    alert("执行成功");
                    window.location.reload();
                },
                error:function (error) {
                    alert(error);
                }
            });
        });


        function check(id){
            if(!id){
                alert('参数缺失不支持测试！');
                return  "error";
            }
        }
    });


    function scrollBar()//设置滚动条的滚动
    {

        scrollh=document.getElementById("console-log").scrollHeight;
        if(document.getElementById("console-log").scrollTop<=scrollh)
            document.getElementById("console-log").scrollTop+=scrollh;
        if(document.getElementById("console-log").scrollTop>=scrollh+6)
            document.getElementById("console-log").scrollTop-=scrollh;
        scrollTimer=window.setTimeout("scrollBar()",delay);
    }


    function  readLog(){
        if($("#fileNameList").find("option").first()){
            $("#fileNameList").siblings("div.layui-form-select").find("dd:first").click();
        }
        var  selectFileName=$("#fileNameList").find("option").first().val();
        loadLog(selectFileName);
    }

    function loadLog(selectFileName){
        $("#consoleLogContent").html("");
        if(selectFileName){
            checkFile(selectFileName).then(function (type) {
                if("readAll"===type){
                    url="${request.contextPath}/publish/readLog?type=all&fileName="+selectFileName;
                    renderLogHtml(url,type);
                }else{
                    scrollBar();
                    //定时渲染
                    timer=window.setInterval(function (args) {
                        renderLogHtml(url,type,selectFileName);
                    },300);
                }
            }, function (reason) {
                alert(reason)
            })
        }
    }



    function  renderLogHtml(url,readType,fileName) {
        if ("readAll" != readType) {
            url = "${request.contextPath}/publish/readLog?type=ohter&lastTimeFileSize=" + lastTimeFileSize+"&fileName="+fileName;
        }
        //读取日志信息
        _mm.request({
            "method":"get",
            "url":url,
            success:function (res) {
                if(res){
                    //读取内容
                    var content=res.content;
                    //判断读取类型
                    if("readAll"===readType){
                        $("#consoleLogContent").append(content);
                    }else {
                        if (res.nextRandomIndex) {
                            console.log('当前:' + lastTimeFileSize);
                            //修改读取指针
                            lastTimeFileSize = res.nextRandomIndex;
                            console.log('修改过:' + lastTimeFileSize);
                            $("#consoleLogContent").append("\n").append(content);
                        }else{
                            if(timer){
                                window.clearInterval(timer);
                            }
                            if(scrollTimer){
                                window.clearInterval(scrollTimer);
                            }
                        }
                    }
                }else{
                    if(timer){
                        window.clearInterval(timer);
                    }
                    if(scrollTimer){
                        window.clearInterval(scrollTimer);
                    }
                }

            },
            error:function (error) {
                alert(error);
            }
        });

    }

    function checkFile(fileName){
        return new Promise(function(resolve, reject){
            //检查文件是否写完
            _mm.request({
                "method":"get",
                "url":"${request.contextPath}/publish/checkLogStatus?fileName="+fileName,
                success:function (res) {
                    resolve(res);
                },
                error:function (error) {
                    reject(error)
                }
            });
        });
    }


    //点击发布生成日志文件成功后立刻回调
    function refresh(res){
        var selectFileName=$("#fileNameList").prepend("<option value="+res+">"+res+"</option>");
        renderForm();
        //每次发布回清空文件读取指针
        lastTimeFileSize=0;
        //发布执行完，重新查看日志
        readLog();
    }



    function renderForm(){
        layui.use('form', function(){
            var form = layui.form;
            form.render();
        });
    }

    function generatePublishLogFileByPublishId(type,id){
        return new Promise(function(resolve, reject){
            //检查文件是否写完
            _mm.request({
                "method":"post",
                "url":"${request.contextPath}/publish/generatePublishLogFileByPublishId",
                "data":{
                    "id":id,
                    "type":type
                },
                success:function (res) {
                    resolve(res);
                },
                error:function (error) {
                    reject(error)
                }
            });
        });
    }
</script>
</body>
</html>
