﻿<#assign pagetitle="流程定义" >

<!DOCTYPE html>
<html>
<head>

    <script type="text/javascript">
    </script>
<#--<link rel="stylesheet" href="/content/layui-v2.5.7/layui/css/layui.css">-->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${pagetitle}!</title>
    <meta charset="UTF-8">
    <link href="/content/css/css.css" rel="stylesheet"/>
    <link href="/content/themes/base/jquery.ui.all.css" rel="stylesheet"/>
    <script src="/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/scripts/jquery-ui-1.8.24.js" type="text/javascript"></script>
    <script src="/content/gojs/1.8.13/go.js"></script>
    <script src="/content/js/flowV1.1.js"></script>


    <!--ajax请求工具类-->
    <script src="/util/mm.js"></script>
    <script type="text/javascript">

        //浏览
        var isview=${isView?c};
        //不存在的时候为空,赋值为0也就是False，巧妙的避开了!'' 空窜导致的js错误，还能利用好if判断
        var showNodes =${showNodes!0};

        //只有发布管理，才有上下文菜单
        var manager=_mm.getUrlParam('manager');


        $(function (){
            // if(window.location.href.indexOf('onlyview')>-1) {
            //     isview =true;
            //     $(".ui-dialog-buttonpane button").hide();
            //     $('#left').hide();
            // }


            // //新增初始化
            // if(manager){
            //     initWithContextMenu(false);
            // }else{
            //     initWithContextMenu(false);
            // }
            initWithContextMenu(true);


            if(showNodes){
                //为0不会也就是false不会执行此逻辑
                //存在的时候，直接取出序列化好的节点
                //如果节点有内容就回显
                load(showNodes);
            }


            if(isview){
                //预览模式,只允许查看
                $(".ui-dialog-buttonpane button").hide();
                $('#left').hide();
                //myDiagram.isModified=false;
                myDiagram.isReadOnly=true;
                // //仅测试
                // if(showNodes){
                //     var jsShowNodes=JSON.parse(showNodes);
                //     load(jsShowNodes);
                // }
            }
        });
        var lastDialog =null;
        /***节点双击事件***/
        function nodeDoubleClick(e,node){
            if(!isview){
                var info = node.part.data;
                if(lastDialog!=null){
                    $(lastDialog).dialog("close");
                }
                lastDialog = $("#"+info.dialog);
                lastDialog.dialog('open');
                jsonToDialog(info.dialog,info.key);
            }
        }
        /** 右键菜单*/
        function menu(name,e, obj){
            var node = obj.part.adornedPart;
            var currentNodeData=node.data;//获取节点的信息
            var nodeJson=currentNodeData.json;
            var nodeType=nodeJson.type;
            //当前操作类型，是否正常流程还是回滚流程
            var type=_mm.getUrlParam('type');
            var id=_mm.getUrlParam('id');
            if(name=="发布"){
                if('end'===nodeType){
                    alert('结束节点不支持发布！');
                    return;
                }
                if(!type&&!id){
                    alert('参数缺失不支持发布！');
                    return;
                }
                var nodeJson;
                if('start'===nodeType){
                    if(showNodes){
                        //获取回显的节点
                        nodeJson = showNodes;
                    }else{
                        //获取整个流程json,执行发布
                        nodeJson = modelToJson();
                    }
                }else{
                    //获取当前选中节点下的所有节点
                    nodeJson=currentNodeFlowModelToJson(currentNodeData);
                }
                if('error'===nodeJson){
                    return;
                }
                console.log(nodeJson)
                //向服务端发送请求测试整个流程
                //发布之前要生成对应发布的文件名称
                generatePublishLogFile(nodeJson,type,id).then(function(res){
                    if(window.parent.refresh){
                        //调用父页面的发布回调;
                        window.parent.refresh(res);
                    }
                    _mm.request({
                        "method":"post",
                        "url":"${request.contextPath}/publish/testPublish",
                        "data":{
                            "nodeJson":JSON.stringify(nodeJson),
                            "type":type,
                            "id":id,
                            "logFileNname":res
                        },
                        success:function (res) {
                            alert('发布流程测试执行ok！')
                        },
                        error:function (error) {
                            alert('发布流程测试执行出错！：'+error);
                        }
                    });
                },function(reason){
                    alert(reason);
                })
            }else if(name=="测试"){
                if(isview){
                    alert('流程查看状态不支持测试！');
                    return;
                }

                if(!type&&!id){
                    alert('参数缺失不支持测试！');
                    return;
                }
                if('start'===nodeType){
                    alert('开始节点不支持测试！');
                    return;
                }
                if('end'===nodeType){
                    alert('结束节点不支持测试！');
                    return;
                }
                //向服务端发送请求，测试当前节点
                _mm.request({
                    "method":"post",
                    "url":"${request.contextPath}/publish/testCurrentNode",
                    "data":{"nodeJson":JSON.stringify(nodeJson)},
                    success:function (res) {
                        alert('测试当前节点执行ok！');
                    },
                    error:function (error) {
                        alert('测试当前节点执行出错！'+error);
                    }
                });
            }
        }


        function generatePublishLogFile(nodeJson,type,id){
            return new Promise(function(resolve, reject){
                //检查文件是否写完
                _mm.request({
                    "method":"post",
                    "url":"${request.contextPath}/publish/generatePublishLogFile",
                    "data":{
                        "nodeJson":JSON.stringify(nodeJson),
                        "id":id,
                        "type":type
                    },
                    success:function (res) {
                        resolve(res);
                    },
                    error:function (error) {
                        reject(error)
                    }
                });
            });
        }
    </script>
    <style>
        .midtext{
            width: 50px;
        }
        label{min-width: auto;}
        .mydetail label{width:auto;}
        .mydetail li{margin-top: 5px;}
    </style>
</head>
<body class="layui-layout-body">
<div>
    <div style="width:100%; white-space:nowrap;">
    <span id="left" style="display: inline-block; vertical-align: top; width:105px">
      <div id="myPaletteDiv" style="border: solid 1px black; height: 620px"></div>
    </span>
        <span style="display: inline-block; vertical-align: top; width:100%">
      <div id="myDiagramDiv" style="border: solid 1px black; height: 620px"></div>
    </span>
    </div>
</div>
<!--start配置-->
<div id="start-dialog" title="开始配置">
    <div>
        <button onclick="save()">保存</button>
        <button onclick="debug()">调试</button>
        <!--        <button onclick="load()">Load</button>-->
    </div>
    <script type="text/javascript">
        //保存流程操作
        function save(){
            var j = modelToJson();
            console.log(j);
            if('error'===j){
                return;
            }
            var type= getUrlParam('type');
            if('normal'===type){
                //回调父窗口
                window.opener.saveFlowCallback(j);
            }else if('rollback'===type){
                //回调父窗口
                window.opener.roolbackFlowCallback(j);
            }else{
                alert('参数不合法');
                return;
            }
            window.close();
        }
        //流程测试操作
        function debug() {
            myDiagram.model.modelData.position = go.Point.stringify(myDiagram.position);
            alert(myDiagram.model.toJson());
            //myDiagram.isModified = false;
        }
        function load(json) {
            myDiagram.model = go.Model.fromJson(jsonToModel(json));
            //var pos = myDiagram.model.modelData.position;
            //if (pos) myDiagram.initialPosition = go.Point.parse(pos);
        }
        $('#start-dialog').dialog({
            autoOpen:false,
            height: 150,
            width: 150,
            modal: true,
            draggable: true,
            minWidth: 150,
            buttons: {
                "关闭": function() {
                    $(this).dialog("close");
                }
            }
        });
    </script>
</div>
<!--jenkins配置-->
<div id="jenkins-dialog" title="jenkins配置">
    <ul class="mydetail" name="key">
        <li>
            <label>节点</label>
            <input class="midtext" type="text" name="text" json="text"/>
            <input type="hidden" json="type" data="jenkins" />
        </li>
        <li>
            <label>应用</label><select name="app" json="app"><#list appNames as value><option value="${value}">${value}</option></#list></select>
        </li>
        <li>
            <label>参数</label><textarea rows="5" cols="15" name="param" json="param"></textarea>
        </li>
    </ul>
    <script type="text/javascript">
        $('#jenkins-dialog').dialog({
            autoOpen:false,
            height: 350,
            width: 400,
            modal: true,
            draggable: true,
            minWidth: 300,
            buttons: {
                "关闭": function() {
                    $(this).dialog("close");
                },
                "保存": function() {
                    dialogToJson('jenkins-dialog');
                    $(this).dialog("close");
                }
            }
        });
    </script>
</div>
<!--shell配置-->
<div id="shell-dialog" title="shell配置">
    <ul class="mydetail" name="key">
        <li>
            <label>节点</label><input class="midtext" type="text" name="text" json="text"/>
            <input type="hidden" json="type" data="shell" />
        </li>
        <li>
            <label>服务器</label><select name="server" json="server"><#list serverNames as value><option value="${value}">${value}</option></#list></select>
        </li>
        <li>
            <label>shell脚本</label><textarea rows="5" cols="15" name="shell" json="shell"></textarea>
        </li>
    </ul>
    <script type="text/javascript">
        $('#shell-dialog').dialog({
            autoOpen:false,
            height: 350,
            width: 400,
            modal: true,
            draggable: true,
            minWidth: 300,
            buttons: {
                "关闭": function() {
                    $(this).dialog("close");
                },
                "保存": function() {
                    dialogToJson('shell-dialog');
                    $(this).dialog("close");
                }
            }
        });
    </script>
</div>
<!--apollo配置-->
<div id="apollo-dialog" title="apollo配置">
    <ul class="mydetail" name="key">
        <li>
            <label>节点</label><input class="midtext" type="text" name="text" json="text"/>
            <input type="hidden" json="type" data="apollo" />
        </li>
        <li>
            <label>app</label><select name="app" json="app"><#list appNames as value><option value="${value}">${value}</option></#list></select>
        </li>
        <li>
            <label>配置</label><textarea rows="5" cols="15" name="config" json="config"></textarea>
        </li>
    </ul>
    <script type="text/javascript">
        $('#apollo-dialog').dialog({
            autoOpen:false,
            height: 350,
            width: 400,
            modal: true,
            draggable: true,
            minWidth: 300,
            buttons: {
                "关闭": function() {
                    $(this).dialog("close");
                },
                "保存": function() {
                    dialogToJson('apollo-dialog');
                    $(this).dialog("close");
                }
            }
        });
    </script>
</div>
<!--sql配置-->
<div id="sql-dialog" title="sql配置">
    <ul class="mydetail" name="key">
        <li>
            <label>节点</label><input class="midtext" type="text" name="text" json="text"/>
            <input type="hidden" json="type" name="type" data="sql" />
        </li>
        <li>
            <label>数据库</label><select name="db" json="db"><#list dbNames as value><option value="${value}">${value}</option></#list></select>
        </li>
        <li>
            <label>sql</label><textarea rows="5" cols="15" name="sql" json="sql"></textarea>
        </li>
    </ul>
    <script type="text/javascript">
        $('#sql-dialog').dialog({
            autoOpen:false,
            height: 350,
            width: 400,
            modal: true,
            draggable: true,
            minWidth: 300,
            buttons: {
                "关闭": function() {
                    $(this).dialog("close");
                },
                "保存": function() {
                    dialogToJson('sql-dialog');
                    $(this).dialog("close");
                }
            }
        });
    </script>
</div>
<!--notify配置-->
<div id="notify-dialog" title="notify配置">
    <ul class="mydetail" name="key">
        <li>
            <label>节点</label><input class="midtext" type="text" name="text" json="text"/>
            <input type="hidden" json="type" data="notify" />
        </li>
        <li>
            <label>内容</label><textarea rows="5" cols="15" name="content" json="content"></textarea>
        </li>
        <li>
            <label>接收人</label><input class="midtext" type="text" name="senders" json="senders">
        </li>
    </ul>
    <script type="text/javascript">
        $('#notify-dialog').dialog({
            autoOpen:false,
            height: 350,
            width: 400,
            modal: true,
            draggable: true,
            minWidth: 300,
            buttons: {
                "关闭": function() {
                    $(this).dialog("close");
                },
                "保存": function() {
                    dialogToJson('notify-dialog');
                    $(this).dialog("close");
                }
            }
        });
    </script>
</div>

<script src="/content/layui-v2.5.7/layui/layui.js"></script>
<script>
    //JavaScript代码区域
    layui.use('element', function(){
        var element = layui.element;

    });


    function getUrlParam(key) {
        // 获取参数
        var url = window.location.search;
        //localhost:8080/doc?name=zhangsan&password=abc123
        // 正则筛选地址栏
        var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
        // 匹配目标参数
        var result = url.substr(1).match(reg);
        //返回参数值
        return result ? decodeURIComponent(result[2]) : null;
    }

</script>
</body>
</html>
