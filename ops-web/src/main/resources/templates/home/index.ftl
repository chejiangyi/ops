<#assign pagetitle="登陆" >

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>${pagetitle}!</title>
    <#--<link href="/content/css/css.css" rel="stylesheet"/>-->
    <#--<link href="/content/css/login.css" rel="stylesheet"/>-->
    <script type="text/javascript">
    </script>
    <link rel="stylesheet" href="/content/layui-v2.5.7/layui/css/layui.css">
    <#--<link rel="stylesheet" href="//res.layui.com/layui/dist/css/layui.css"  media="all">-->

</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">流程发布管理系统</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <#--<ul class="layui-nav layui-layout-left">-->
            <#--<li class="layui-nav-item"><a href="">控制台</a></li>-->
            <#--<li class="layui-nav-item"><a href="">商品管理</a></li>-->
            <#--<li class="layui-nav-item"><a href="">用户</a></li>-->
            <#--<li class="layui-nav-item">-->
                <#--<a href="javascript:;">其它系统</a>-->
                <#--<dl class="layui-nav-child">-->
                    <#--<dd><a href="">邮件管理</a></dd>-->
                    <#--<dd><a href="">消息管理</a></dd>-->
                    <#--<dd><a href="">授权管理</a></dd>-->
                <#--</dl>-->
            <#--</li>-->
        <#--</ul>-->
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    ${currentUser.name}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="">安全设置</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="javascript:" class="logout">退出</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">发布平台</a>
                    <dl class="layui-nav-child">
                        <dd>
                            <li class="layui-nav-item layui-nav-itemed">
                                <a class="" href="javascript:;">发布管理</a>
                                <dl class="layui-nav-child">
                                    <dd><a href="javascript:;" data-id="4" class="menu-item" data-url="/home/publishListUI" data-title="发布列表">发布列表</a></dd>
                                    <#--<dd><a href="javascript:;" data-id="1" class="menu-item" data-url="/home/addPublishUI" data-title="创建发布">创建发布</a></dd>-->
                                    <#--<dd><a href="javascript:;" data-id="2" class="menu-item" data-url="/home/addProcessUI" data-title="创建流程">创建流程</a></dd>-->
                                    <#--<dd><a href="javascript:;" data-id="3" class="menu-item" data-url="/home/publishUI"  data-title="发布页面">发布管理</a></dd>-->
                                </dl>
                            </li>
                        </dd>
                        <#if Session["currentUser"]?exists>
                            <#if Session["currentUser"].name=='admin'>
                             <dd>
                                 <li class="layui-nav-item layui-nav-itemed">
                                     <a class="" href="javascript:;">用户管理</a>
                                     <dl class="layui-nav-child">
                                         <dd><a href="javascript:;" data-id="5"  class="menu-item" data-url="/home/userListUI" data-title="用户列表">用户列表</a></dd>
                                     </dl>
                                 </li>
                             </dd>
                            </#if>
                        </#if>
                        <dd><a href="javascript:;"  data-id="6" class="menu-item" data-url="/home/systemConfigListUI"  data-title="系统配置">系统配置</a></dd>
                        <dd><a href="javascript:;" data-id="7"  class="menu-item" data-url="/home/operateLogListUI" data-title="操作日志">操作日志</a></dd>
                    </dl>
                </li>




                <#--<li class="layui-nav-item">-->
                    <#--<a href="javascript:;">解决方案</a>-->
                    <#--<dl class="layui-nav-child">-->
                        <#--<dd><a href="javascript:;">列表一</a></dd>-->
                        <#--<dd><a href="javascript:;">列表二</a></dd>-->
                        <#--<dd><a href="">超链接</a></dd>-->
                    <#--</dl>-->
                <#--</li>-->
                <#--<li class="layui-nav-item"><a href="">云市场</a></li>-->
                <#--<li class="layui-nav-item"><a href="">发布商品</a></li>-->
            </ul>
        </div>
    </div>

    <!--tab标签-->
    <div class="layui-tab" lay-filter="tab-con" lay-allowclose="true" style="margin-left: 200px;">
        <!--tab标题-->
        <ul class="layui-tab-title"></ul>
        <!--嵌入iframe页面-->
        <div class="layui-tab-content layui-body" ></div>
    </div>


    <div class="layui-footer">
        <!-- 底部固定区域 -->
    ${springMacroRequestContext.contextPath}
    顶顶顶顶${request.contextPath}
        Copyright © 2019 stylefeng All rights reserved.
    </div>
</div>

<script src="/scripts/jquery-1.8.2.js"></script>
//ajax请求工具类
<script src="/util/mm.js"></script>
<script src="/content/layui-v2.5.7/layui/layui.js"></script>

<script>
    layui.use(['element', 'layer', 'jquery'], function () {
        var layer = layui.layer;
        var element = layui.element;
        // var layer = layui.layer;
        var $ = layui.$;
        // 配置tab实践在下面无法获取到菜单元素
        $('.menu-item').on('click', function () {
            var menuItem = $(this);
            //这时会判断右侧.layui-tab-title属性下的有lay-id属性的li的数目，即已经打开的tab项数目
            if ($(".layui-tab-title li[lay-id]").length <= 0) {
                //如果比零小，则直接打开新的tab项
                active.tabAdd(menuItem.attr("data-url"), menuItem.attr("data-id"), menuItem.attr("data-title"));
            } else {
                //否则判断该tab项是否以及存在
                var isData = false; //初始化一个标志，为false说明未打开该tab项 为true则说明已有
                $.each($(".layui-tab-title li[lay-id]"), function () {
                    //如果点击左侧菜单栏所传入的id 在右侧tab项中的lay-id属性可以找到，则说明该tab项已经打开
                    if ($(this).attr("lay-id") == menuItem.attr("data-id")) {
                        isData = true;
                    }
                })
                if (isData == false) {
                    //标志为false 新增一个tab项
                    active.tabAdd(menuItem.attr("data-url"), menuItem.attr("data-id"), menuItem.attr("data-title"));
                }
            }
            //最后不管是否新增tab，最后都转到要打开的选项页面上
            active.tabChange(menuItem.attr("data-id"));
        });

        var active = {
            //在这里给active绑定几项事件，后面可通过active调用这些事件
            tabAdd: function (url, id, name) {
                //新增一个Tab项 传入三个参数，分别对应其标题，tab页面的地址，还有一个规定的id，是标签中data-id的属性值
                //关于tabAdd的方法所传入的参数可看layui的开发文档中基础方法部分
                element.tabAdd('tab-con', {
                    title: name,
                    content: '<iframe data-frameid="' + id + '" scrolling="yes" frameborder="0" src="' + url + '" style="width:99%;height:99%;"></iframe>',
                    id: id //规定好的id
                })
                FrameWH();  //计算ifram层的大小
            },
            tabChange: function (id) {
                //切换到指定Tab项
                element.tabChange('tab-con', id); //根据传入的id传入到指定的tab项
            },
            tabDelete: function (id) {
                element.tabDelete("tab-con", id);//删除
            }
        };
        function FrameWH() {
            var h = $(window).height();
            $("iframe").css("height",h+"px");
        }


        //退出登录点击事件
        $('.logout').on('click', function () {
            _mm.request({
                "method":"post",
                "url":"${request.contextPath}/user/logout",
                success:function (res) {
                    layer.msg("退出成功");
                    window.location.href="${request.contextPath}/user/login"
                },
                error:function (error) {
                    alert(context);
                    layer.msg("退出出错了，请稍后再试")
                }
            });
        });
    })
</script>
</body>
</html>
