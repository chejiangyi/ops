<#assign pagetitle="用户列表" >

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>${pagetitle}</title>
    <script type="text/javascript">
    </script>
    <link rel="stylesheet" href="/content/layui-v2.5.7/layui/css/layui.css">
</head>
<body class="layui-layout-body">
    <div>
        <fieldset class="layui-elem-field site-demo-button" style="margin-top: 30px;">
            <legend>用户列表</legend>
            <div>
                <button type="button" id="addBtn" class="layui-btn layui-btn-primary" >新增</button>
            </div>
        </fieldset>
    </div>
    <table class="layui-hide" id="userList" lay-filter="userListFilter"></table>
    <!--编辑组件-->
    <script type="text/html" id="barDemo">
        <#--<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>-->
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
    <!--新增对话框-->
    <div id="add-user-con" style="display: none;">
        <form class="layui-form" id="add-form"  lay-filter="add-form-filter">
            <div class="layui-form-item center" >
                <label class="layui-form-label" style="width: 100px" >姓名</label>
                <div class="layui-input-block">
                    <input type="text" name="name" required value="" style="width: 240px" lay-verify="required" placeholder="请输入配置名称" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 100px">密码</label>
                <div class="layui-input-block">
                    <input type="password" name="pswd" required style="width: 240px" lay-verify="required" placeholder="请输入配置类型" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 100px">角色&emsp;</label>
                <div class="layui-input-block">
                    <input type="text" name="role" required style="width: 240px" lay-verify="required" placeholder="请输入配置值" autocomplete="off" class="layui-input">
                    <!-- <input type="hidden" name="id" style="width: 240px" autocomplete="off" class="layui-input"> -->
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 100px">app权限</label>
                <div class="layui-input-block">
                    <input type="text" name="appAuth" required style="width: 240px" lay-verify="required" placeholder="请输入配置类型" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 100px">db权限</label>
                <div class="layui-input-block">
                    <input type="text" name="dbAuth" required style="width: 240px" lay-verify="required" placeholder="请输入配置类型" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 100px">serverAuth</label>
                <div class="layui-input-block">
                    <input type="text" name="serverAuth" required style="width: 240px" lay-verify="required" placeholder="请输入配置类型" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" id="save-btn" lay-submit lay-filter="save" >保存</button>
                    <button type="reset" class="layui-btn layui-btn-primary" id="closeBtn" >关闭</button>
                </div>
            </div>
        </form>
    </div>


    <script src="/scripts/jquery-1.8.2.js"></script>
    <!--ajax请求工具类-->
    <script src="/util/mm.js"></script>
    <script src="/content/layui-v2.5.7/layui/layui.js"></script>

    <script>
        layui.use(['element', 'layer', 'jquery','table','form'], function () {
			var $ = layui.$;
            var layer = layui.layer;
            var element = layui.element;
			var form = layui.form;
            var table = layui.table;

            var dialogIndex;
			
			//0 是新增  1是编辑
			var saveOrUpdateFlag=0;
			//默认新增操作
			var  requestUrl="${request.contextPath}/user/save";
			
			//渲染表格
            table.render({
                elem: '#userList'
                ,url:'/user/pageList'
                ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                ,cols: [[
                    {field:'id', width:80, title: 'ID', sort: true}
                    ,{field:'name', width:200, title: '姓名'}
                    ,{field:'role', width:80, title: '角色', sort: true,templet:function(e){
                        var roleName;
                        if(e.role===0){
                            roleName="管理员"
                        }else if(e.role===0){
                            roleName="普通用户"
                        }else{
                            roleName="其他"
                        }
                        return roleName;
                    }}
                    ,{field:'appAuth', width:200, title: 'app权限'}
                    ,{field:'dbAuth', title: 'db权限', width: '30%', minWidth: 100} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                    ,{field:'serverAuth', title: '服务器权限', sort: true}
                    ,{field:'updateTime', title: '更新时间', sort: true}
                    ,{field:'createTime', title: '创建时间'}
                    ,{fixed: 'right', width: 165, align:'center', toolbar: '#barDemo'}
                    ]]
                ,page: true //开启分页
                ,request:{  //更改分页请求参数
                    pageName:'pageIndex',
                    limitName:'pageSize'
                }
                ,limit: 10 //每页默认显示的数量
            });


            //监听行工具事件
            table.on('tool(userListFilter)', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
                var data = obj.data //获得当前行数据
                        ,layEvent = obj.event; //获得 lay-event 对应的值
                if(layEvent === 'detail'){
                    layer.msg('查看操作');
                } else if(layEvent === 'del'){
                    layer.confirm('真的删除行么', function(index){
                        //向服务端发送删除指令
                        _mm.request({
                            "method":"post",
                            "url":"${request.contextPath}/user/delete",
                            "data":{
								id:	data.id	
							},
                            success:function (res) {
                                layer.msg("删除成功");
								obj.del(); //删除对应行（tr）的DOM结构
								layer.close(index);
                                //执行重载
                                table.reload('userList', {}, 'data');
                            },
                            error:function (error) {
                                layer.msg("删除出错了，请稍后再试")
                            }
                        });
                    });
                } else if(layEvent === 'edit'){
                    //layer.msg('编辑操作');
                    _mm.request({
                        "method":"get",
                        "url":"${request.contextPath}/user/detail",
                        "data":{
                            id:	data.id
                        },
                        success:function (res) {
                            requestUrl="${request.contextPath}/user/update";
                            var title='编辑用户';
							//回显表单
							form.val('add-form-filter',{
								//表单回显
								"name":res.name,
								"pswd":res.pswd,
								"role":res.role,
								"appAuth":res.appAuth,
								"dbAuth":res.dbAuth,
								"serverAuth":res.serverAuth
							})
                            $("#save-btn").html("修改");
							//添加主键id
                            $("#add-form").append("<input type='hidden' name='id' value='"+res.id+"'/>");
                            showSaveOrUpdateDialog(title);
                        },
                        error:function (error) {
                            layer.msg("编辑出错了，请稍后再试")
                        }
                    });
                }
            });


			 //表单保存监听时间
			 form.on('submit(save)', function (data) {
					_mm.request({
						"method":"post",
						"url":requestUrl,
						"data":data.field,
                        success:function (res) {
							layer.msg("保存成功");
							//执行重载
							table.reload('userList', {}, 'data');
                            return false;
						},
						error:function (error) {
							layer.msg("退出出错了，请稍后再试")
                            return false;
                        }
					});
			});

            //新增按钮点击事件
            $("#addBtn").click(function(){
                var title='新增用户';
                showSaveOrUpdateDialog(title);
            });


            //关闭对话框事件
            $("#closeBtn").click(function(){
                CloseWin();
            });


            function showSaveOrUpdateDialog(title){
                //页面层-自定义
                dialogIndex=layer.open({
                    type: 1,
                    title:title,
                    closeBtn: false,
                    shift: 2,
                    area: ['400px', '500px'],
                    shadeClose: true,
                    //btn: ['新增', '取消'],
                    // btnAlign: 'c',
                    content: $("#add-user-con"),
                    success: function(layero, index){

                    },
                    yes:function(){
                      
                    }
                });
            }


            //关闭页面
            function CloseWin(){
                //parent.location.reload(); // 父页面刷新
                // var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                // parent.layer.close(index); //再执行关闭
                if(dialogIndex){
                    layer.close(dialogIndex);
                    dialogIndex="";
                }
            }
        })
    </script>
</body>
</html>
