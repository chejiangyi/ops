var _mm={
    request:function(param){
        var _this=this;
        $.ajax({
            type	:param.method ||'get',
            url		:param.url	 || '',
            dataType:param.type   ||'json',
            data     :param.data || '',
            success  :function(res){
                if(0===res.code){
                    //请求成功
                    typeof param.success==='function'&&param.success(res.data,res.msg);
                }else if(1===res.code){
                    typeof param.error==='function'&&param.error(res.msg);
                }
            },
            error    :function(err){
                typeof param.error==='function'&&param.error(err.statusText);
            },
            //自己加的项目中没有，防止后台每次产生新的session，源码中没有
            xhrFields: {
                withCredentials: true
            },
            //自己加的项目中没有，防止后台每次产生新的session,源码中没有
            crossDomain: true

        });
    },
    //获取服务器地址
    getServerUrl:function(path){
        return conf.serverHost+path;
    },
    //获取url参数
    getUrlParam:function(name){
        //list?keyword=1&page=1
        var reg=new RegExp('(^|&)'+name+'=([^&]*)(&|$)');
        var result=window.location.search.substr(1).match(reg);
        return result?decodeURIComponent(result[2]):null;
    },
    //渲染html模板
    renderHtml:function(htmlTemplate,data){
        var template=Hogan.compile(htmlTemplate),
            result = template.render(data);
        return result;
    },
    successTips:function(msg){
        alert(msg||"操作成功!");
    },
    errorTips:function(msg){
        alert(msg||"哪里不对了~");
    },
    //字段通过，支持是非空，手机、邮箱
    validate:function(value,type){
        var value=$.trim(value);
        //非空验证
        if('require'===type){
            return !!value;
        }
        //手机号验证
        if('phone'===type){
            return /^1\d{10}$/.test(value);
        }
        //邮箱验证
        if('email'===type){
            return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(value);
        }
    },
    //统一登录处理
    doLogin:function(){
        window.location.href="./user-login.html?redirect="+encodeURIComponent(window.location.href);
    },
    goHome:function(){
        window.location.href='./index.html';
    }
};