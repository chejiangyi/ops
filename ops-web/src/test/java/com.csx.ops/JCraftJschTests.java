package com.csx.ops;


import com.alibaba.fastjson.JSON;
import com.csx.ops.dao.model.auto.t_config_model;
import com.csx.ops.web.Application;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.yh.csx.bsf.core.db.DbHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 远程调用liunx服务器测试工具类测试
 * @author  yls
 */
@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
@Slf4j
public class JCraftJschTests {

    //远程主机IP
    private static final String REMOTE_HOST = "";
    //远程主机用户名
    private static final String USERNAME = "";
    //远程主机密码
    private static final String PASSWORD = "";
    //SSH服务端口
    private static final int REMOTE_PORT = 22;
    //会话超时时间
    private static final int SESSION_TIMEOUT = 10000;
    //管道流超时时间(执行脚本超时时间)
    private static final int CHANNEL_TIMEOUT = 5000;



    @Test
    public void contextLoad(){

    }


    /**
     * 测试执行liunx脚本,参考https://www.cnblogs.com/jing1617/p/7132100.html
     * @throws Exception
     */
    @Test
    public void testExceuteLinuxScript()  throws Exception{
            //脚本名称及路径，与上文要对上
            String remoteShellScript = "/root/hello.sh";
            Session jschSession = null;
            try {
                JSch jsch = new JSch();
                //两种方式都是可行的
               //String script="dir \n cd /home \n cd ops \n  pwd \n";
                String script="dir ; cd /home ; cd ops ; pwd;";
//             //SSH授信客户端文件位置，一般是用户主目录下的.ssh/known_hosts
//              jsch.setKnownHosts("/home/web/.ssh/known_hosts");
                jschSession = jsch.getSession(USERNAME, REMOTE_HOST, REMOTE_PORT);
                //设置第一次登陆的时候不提示，
                jschSession.setConfig("StrictHostKeyChecking", "no");
                // 密码认证
                jschSession.setPassword(PASSWORD);
                // 建立session
                jschSession.connect(SESSION_TIMEOUT);
                //建立可执行管道
                ChannelExec channelExec = (ChannelExec) jschSession.openChannel("exec");
                // 执行脚本命令"sh /root/hello.sh zimug"
                channelExec.setCommand(script);
                // 获取执行脚本可能出现的错误日志
                channelExec.setErrStream(System.err);
                //脚本执行结果输出，对于程序来说是输入流
                InputStream in = channelExec.getInputStream();
                // 5 秒执行管道超时
                channelExec.connect(CHANNEL_TIMEOUT);
                // 从远程主机读取输入流，获得脚本执行结果
                byte[] tmp = new byte[1024];
                while (true) {
                    while (in.available() > 0) {
                        int i = in.read(tmp, 0, 1024);
                        if (i < 0) break;
                        //执行结果打印到程序控制台
                        System.out.print(new String(tmp, 0, i));
                    }
                    if (channelExec.isClosed()) {
                        if (in.available() > 0) continue;
                        //获取退出状态，状态0表示脚本被正确执行
                        System.out.println("exit-status: "
                                + channelExec.getExitStatus());
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (Exception ee) {
                    }
                }
                channelExec.disconnect();
            } catch (JSchException | IOException e) {
                e.printStackTrace();
            } finally {
                if (jschSession != null) {
                    jschSession.disconnect();
                }
            }
    }
}
