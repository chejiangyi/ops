package com.csx.ops;


import com.alibaba.fastjson.JSON;
import com.csx.ops.web.Application;
import com.ctrip.framework.apollo.openapi.client.ApolloOpenApiClient;
import com.ctrip.framework.apollo.openapi.dto.NamespaceGrayDelReleaseDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenItemDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenReleaseDTO;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Locale;


/**
 * 携程Apollo单元测试
 * @author  yls
 */
@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
@Slf4j
public class ApolloTests {

    private  final static String PORTALURL = ""; // apollo portal url
    private  final static String TOKEN = "81294dd33877fe7589808efd2a73d7ccfc1094c4"; // 申请的token

    private String appId = "yls-config-test";    //appId
    private String namespaceName="application";  //namespaceName

    //third apollo open api
    private  static ApolloOpenApiClient client=null;

    static{
        client = ApolloOpenApiClient.newBuilder()
                .withPortalUrl(PORTALURL)
                .withToken(TOKEN)
                .build();
    }


    @Test
    public void contextLoad(){

    }



    //新增配置
    @Test
    public void addParam() {
        OpenItemDTO openItemDTO = new OpenItemDTO();
        openItemDTO.setKey("timeout");
        openItemDTO.setValue("100");
        openItemDTO.setComment("超时时间");
        openItemDTO.setDataChangeCreatedBy("apollo");
        OpenItemDTO item = client.createItem(appId, "DEV", "default", namespaceName, openItemDTO);
        log.info("--------------新增配置------------"+JSON.toJSONString(item));
        releaseParam();

    }

    //修改配置
    @Test
    public void updateParam() {
        OpenItemDTO openItemDTO = new OpenItemDTO();
        openItemDTO.setKey("timeout");
        openItemDTO.setValue("200");
        openItemDTO.setComment("超时时间");
        openItemDTO.setDataChangeCreatedBy("apollo");
        client.createOrUpdateItem(appId, "DEV", "default", namespaceName, openItemDTO);
        log.info("--------------修改配置------------"+JSON.toJSONString(openItemDTO));
        releaseParam();
    }



    //删除配置
    @Test
    public void deleteConfig() {
        //operator 要存在，否则会报错
        client.removeItem(appId, "DEV", "default", namespaceName, "timeout","apollo");
        log.info("--------------删除配置------------");
        releaseParam();
    }



    //发布配置
    @Test
    public void releaseParam() {
        DateTime dt = new DateTime();
        String currentTimeStr=dt.toString("yyyy年MM月dd日 HH:mm:ss EE", Locale.CHINESE);
        NamespaceGrayDelReleaseDTO namespaceGrayDelReleaseDTO = new NamespaceGrayDelReleaseDTO();
        namespaceGrayDelReleaseDTO.setReleaseTitle(currentTimeStr+"release");
        namespaceGrayDelReleaseDTO.setReleaseComment("自动化发布测试");
        namespaceGrayDelReleaseDTO.setReleasedBy("apollo");
        OpenReleaseDTO openReleaseDTO = client.publishNamespace(appId, "DEV", "default", namespaceName, namespaceGrayDelReleaseDTO);
        log.info("--------------发布配置------------"+JSON.toJSONString(openReleaseDTO));
    }


    /**
     * 获取名称空间下所有的配置
     * -----------获取名称空间下所有的配置-------{"appId":"yls-config-test","clusterName":"default","comment":"default app namespace","dataChangeCreatedBy":"apollo","dataChangeCreatedTime":1607930678000,"dataChangeLastModifiedBy":"apollo","dataChangeLastModifiedTime":1607930678000,"format":"properties","items":[{"comment":"test","dataChangeCreatedBy":"apollo","dataChangeCreatedTime":1607930774000,"dataChangeLastModifiedBy":"apollo","dataChangeLastModifiedTime":1607930774000,"key":"name","value":"yls"}],"namespaceName":"application","public":false}
     */
    @Test
    public void getAllNameSpace() {
        log.info("-----------获取名称空间下所有的配置-------"+JSON.toJSONString(client.getNamespace(appId, "DEV", "default",namespaceName))); ;
    }
}
