package com.csx.ops;


import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import com.csx.ops.web.Application;
import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 通知、告警测试
 *
 * 告警：采用飞书机器人，通过 webhook 的形式获取来自外部系统的消息，然后将消息即时推送到群聊中与群成员共享
 *
 * @author  yls
 */
@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
@Slf4j
public class NotifyTests {

    String url="";   //机器人对应的webhook 地址


    /**
     * https://www.feishu.cn/hc/zh-cn/articles/360024984973
     * 飞书群，配置中新建机器人有对应的使用说明
     * 测试简单消息
     */
    @Test
    public void testFeishuRobotSimpleMsg() throws IOException {
        Map<String,Object> result=Maps.newHashMap();
        Map<String,String> cotent=Maps.newHashMap();
        cotent.put("text","你好，我是标题");
        result.put("msg_type","text");
        result.put("content",cotent);
        String jsonStr=JSONObject.toJSONString(result);
        log.info("jsonStr:"+jsonStr);
        String postResult = HttpRequest
                .post(url)
                .header("Content-Type","application/json")
                .body(jsonStr)
                .execute()
                .body();
        log.info("postResult:"+postResult);
    }




    /**
     * https://www.feishu.cn/hc/zh-cn/articles/360024984973
     * 飞书群，配置中新建机器人有对应的使用说明
     * 发送富文本消息
     */
    @Test
    public void testFeishuRobot() throws IOException {
        FeiShuRichMessage  feiShuRichMessage=new FeiShuRichMessage();
        feiShuRichMessage.setMsg_type("post");

        List<List<FeiShuRichMessage.ZhCnContent>> allContents=Lists.newArrayList();
        //构造zhcnConent
        List<FeiShuRichMessage.ZhCnContent> contents= Lists.newArrayList();
        FeiShuRichMessage.ZhCnContent content=feiShuRichMessage.new ZhCnContent();
        content.setTag("text");
        content.setText("异常归属：技术管理小分队");
        contents.add(content);
        allContents.add(contents);


        contents= Lists.newArrayList();
        content=feiShuRichMessage.new ZhCnContent();
        content.setTag("text");
        content.setText("异常产生：测试异常");
        contents.add(content);
        allContents.add(contents);

        contents= Lists.newArrayList();
        content=feiShuRichMessage.new ZhCnContent();
        content.setTag("text");
        content.setText("异常编码：503");
        contents.add(content);
        allContents.add(contents);

        contents= Lists.newArrayList();
        content=feiShuRichMessage.new ZhCnContent();
        content.setTag("text");
        content.setText("异常来源：业务");
        contents.add(content);
        allContents.add(contents);

        contents= Lists.newArrayList();
        content=feiShuRichMessage.new ZhCnContent();
        content.setTag("text");
        content.setText("异常等级：极其严重");
        contents.add(content);
        allContents.add(contents);

        contents= Lists.newArrayList();
        content=feiShuRichMessage.new ZhCnContent();
        content.setTag("text");
        content.setText("[url]:http://www.baidu.com");
        contents.add(content);
        allContents.add(contents);
        //构造ZhCn对象
        FeiShuRichMessage.ZhCn  zhCn=feiShuRichMessage.new ZhCn("异常标题：ops-告警-异常",allContents);
        FeiShuRichMessage.FeiShuRichMessagePost  messagePost=feiShuRichMessage.new FeiShuRichMessagePost(zhCn);
        FeiShuRichMessage.FeiShuRichMessageContent feiShuRichMessageContent=feiShuRichMessage.new FeiShuRichMessageContent(messagePost);
        feiShuRichMessage.setContent(feiShuRichMessageContent);
        String jsonStr=JSONObject.toJSONString(feiShuRichMessage).trim();
        log.info("jsonStr:"+jsonStr);
        String postResult = HttpRequest
                .post(url)
                .header("Content-Type","application/json")
                .body(jsonStr)
                .execute()
                .body();
        log.info("postResult:"+postResult);
    }


    /**
     * 飞书富文本消息体
     */
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class FeiShuRichMessage{
        private String msg_type;
        private FeiShuRichMessageContent content;


        @Data
        @NoArgsConstructor
        public class FeiShuRichMessageContent{
            FeiShuRichMessagePost  post;
            public FeiShuRichMessageContent(FeiShuRichMessagePost post){
                    this.post=post;
            }
        }


        @Data
        @NoArgsConstructor
        class FeiShuRichMessagePost{
            ZhCn zh_cn;
            public FeiShuRichMessagePost(ZhCn zh_cn){
                this.zh_cn=zh_cn;
            }
        }


        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public class ZhCn{
            private String title;
            private List<List<ZhCnContent>> content;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        class ZhCnContent{
            private String tag;
            private String text;
            private String href;
        }
    }

}
