package com.csx.ops;


import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.fastjson.JSON;
import com.csx.ops.dao.dal.auto.t_config_base_dal;
import com.csx.ops.dao.model.auto.t_config_model;
import com.csx.ops.web.Application;
import com.yh.csx.bsf.core.db.DbConn;
import com.yh.csx.bsf.core.db.DbHelper;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * DBHelper工具类测试
 * crud操作
 * @author  yls
 */
@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
@Slf4j
public class DBHelperTests {


    @Autowired
    private DataSource dataSource;


    @Test
    public void contextLoad(){

    }


    /**
     * 测试查询所有
     * @throws Exception
     */
    @Test
    public void testQueryAll()  throws Exception{
        try {
            List<Map<String, Object>> dataList = DbHelper.get(dataSource,(c)->{return c.executeList("select * from t_config",null);});
            log.info("e===="+ JSON.toJSONString(dataList));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 测试查询所有
     * @throws Exception
     */
    @Test
    public void testQueryByPage()  throws Exception{
        try {
            List<Map<String, Object>> dataList = DbHelper.get(dataSource,(c)->{return c.executeList("select * from t_config limit 0,10",null);});
            log.info("e===="+ JSON.toJSONString(dataList));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 测试新增功能
     * @throws Exception
     */
    @Test
    public void testInsert() throws  Exception{
        t_config_model  configModel=new t_config_model();
        configModel.setKey("test");
        configModel.setValue("test");
        configModel.setCreateTime(new Date());
        configModel.setUpdateTime(new Date());
        //新增config配置表
        StringBuilder sqlBuilder=new StringBuilder();
        sqlBuilder.append("INSERT INTO t_config (`key`,`value`,createTime,updateTime) values(?,?,?,?)");
        int res=DbHelper.get(dataSource,(c)->{return c.executeSql(sqlBuilder.toString(),new Object[]{configModel.getKey(),configModel.getValue(),configModel.getCreateTime(),configModel.getUpdateTime()});});
        if(res>0){
            log.info("DBHelperTests.testInsert插入成功====");
        }else{
            log.info("DBHelperTests.testInsert插入失败====");
        }
    }


    /**
     * 测试更新
     */
    @Test
    public void testUpdate(){
        t_config_model  configModel=new t_config_model();
        configModel.setId(4);
        configModel.setKey("cool");
        configModel.setValue("coolTest");
        configModel.setCreateTime(new Date());
        configModel.setUpdateTime(new Date());
        //新增config配置表
        StringBuilder sqlBuilder=new StringBuilder();
        sqlBuilder.append("update t_config set `key`=?,`value`=?,createTime=?,updateTime=? where id=?");
        int res=DbHelper.get(dataSource,(c)->{return c.executeSql(sqlBuilder.toString(),new Object[]{configModel.getKey(),configModel.getValue(),configModel.getCreateTime(),configModel.getUpdateTime(),configModel.getId()});});
        if(res>0){
            log.info("DBHelperTests.testInsert更新成功====");
        }else{
            log.info("DBHelperTests.testInsert更新失败====");
        }
    }



    /**
     * 测试更新
     */
    @Test
    public void delete(){
        t_config_model  configModel=new t_config_model();
        configModel.setId(4);
        //新增config配置表
        StringBuilder sqlBuilder=new StringBuilder();
        sqlBuilder.append("delete from t_config where id=?");
        int res=DbHelper.get(dataSource,(c)->{return c.executeSql(sqlBuilder.toString(),new Object[]{configModel.getId()});});
        if(res>0){
            log.info("DBHelperTests.testInsert删除成功====");
        }else{
            log.info("DBHelperTests.testInsert删除失败====");
        }
    }


}
