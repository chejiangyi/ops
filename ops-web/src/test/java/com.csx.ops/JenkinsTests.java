package com.csx.ops;


import com.csx.ops.web.Application;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.client.JenkinsHttpClient;
import com.offbytwo.jenkins.model.Build;
import com.offbytwo.jenkins.model.BuildWithDetails;
import com.offbytwo.jenkins.model.ConsoleLog;
import com.offbytwo.jenkins.model.JobWithDetails;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.URI;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;


/**
 * Jenkins单元测试
 * 参考：https://blog.csdn.net/qq_32641153/article/details/94230465
 * @author  yls
 */
@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
@Slf4j
public class JenkinsTests {


    // 连接 Jenkins 需要设置的信息
   private  static final String JENKINS_URL = "";   //jenkins url
   private  static final String JENKINS_USERNAME = "";       //username
   private  static final String JENKINS_PASSWORD = "";          //password


   private static JenkinsHttpClient jenkinsHttpClient = null;        //jenkins http client
   private static JenkinsServer jenkinsServer = null;                //jenkins server client


   static {
       try {
           jenkinsHttpClient = new JenkinsHttpClient(new URI(JENKINS_URL), JENKINS_USERNAME, JENKINS_PASSWORD);
           jenkinsServer = new JenkinsServer(new URI(JENKINS_URL), JENKINS_USERNAME, JENKINS_PASSWORD);
       }catch (Exception e){
           e.printStackTrace();
       }
   }



    @Test
    public void contextLoad(){

    }




    //无参构建任务
    @Test
    public void buldJob() {
        try {
                 // 获取 Job 信息
                JobWithDetails job = jenkinsServer.getJob("yls-test");
                log.info("============获取 Job 名称======="+job.getName());
                log.info("============获取 Job URL======="+job.getUrl());
                log.info("============获取 Job 下一个 build 编号======="+job.getNextBuildNumber());
                log.info("============输出 Job 显示的名称======="+job.getDisplayName());
                log.info("============输出 Job 描述信息======="+job.getDescription());
                log.info("============获取 Job 下游任务列表======="+job.getDownstreamProjects());
                log.info("============获取 Job 上游任务列表======="+job.getUpstreamProjects());
                log.info("============获取 Job getLastBuild 编号======="+job.getLastBuild().getNumber());
                job.build();
                // 获取所有编译中最大的编译对象，也就是当前版本
                var max = job.getAllBuilds().stream().max(Comparator.comparing(Build::getNumber));
                Build  numberBuild=max.isPresent()?max.get():null;
                BuildWithDetails buildWithDetails=numberBuild.details();
                // 当前日志
                ConsoleLog currentLog = buildWithDetails.getConsoleOutputText(0);
                log.info("-------------------jenkins 运行日志-----------"+currentLog.getConsoleLog());
                // 检测是否还有更多日志,如果是则继续循环获取
                while (currentLog.getHasMoreData()) {
                    // 获取最新日志信息
                    ConsoleLog newLog = buildWithDetails.getConsoleOutputText(currentLog.getCurrentBufferSize());
                    // 输出最新日志
                    log.info("-------------------jenkins 更多运行日志-----------"+newLog.getConsoleLog());
                    currentLog = newLog;
                    // 睡眠1s
                    Thread.sleep(1000);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

    }


    //无参构建任务
    @Test
    public void buldJobWithParam() {
        try {
            // 获取 Job 信息
            JobWithDetails job = jenkinsServer.getJob("yls-test");
            log.info("============获取 Job 名称======="+job.getName());
            log.info("============获取 Job URL======="+job.getUrl());
            log.info("============获取 Job 下一个 build 编号======="+job.getNextBuildNumber());
            log.info("============输出 Job 显示的名称======="+job.getDisplayName());
            log.info("============输出 Job 描述信息======="+job.getDescription());
            log.info("============获取 Job 下游任务列表======="+job.getDownstreamProjects());
            log.info("============获取 Job 上游任务列表======="+job.getUpstreamProjects());
            // 设置参数值
            Map<String,String> param = new HashMap<>();
            param.put("key","hello world!");
            job.build(param);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
